module Model.PSCM where

-- import Algebra.PartialOrd
import Development.DomainModelling
--import GHC.Generics
import Polysemy.State
--import Prelude
import Model.PSCM.BasicTypes
-- import Model.PSCM.Operator
-- import Data.Function ((&))

data DecideToHaltLoop

data GetEnvironmentalInput

data ProcessEnvironmentalInput

data ProposeOperators

data ComputeNextOperator

data ApplyChosenOperator

data OutputResultsToEnvironment

data CurrentState

data GetEnvironmentalOutputs

data EvaluateOperatorProposals

data SelectNewOperator

data CreateNewSubstate

data GetOperatorProposals

data GetCurrentOperator

data GetCurrentState

data ResolveImpasses

data GatherPSCMCycleStatisticsForCurrentIteration

data ProcessPSCMCycleStatistics

data EmitPSCMCycleStatistics

data OperatorChosen

-- {-# INLINEABLE pscmLoop #-}
-- pscmLoop ::
--   ( Members
--       '[ Tagged GetEnvironmentalInput (Input input),
--          Tagged ProcessEnvironmentalInput (Methodology input ()),
--          Tagged CurrentState (Input currentState),
--          Tagged DecideToHaltLoop (DecisionProcedureOn input),
--          Tagged ProposeOperators (Methodology currentState operatorProposals),
--          Tagged EvaluateOperatorProposals (Methodology operatorProposals (Either impasse chosenOperator)),
--          Tagged CreateNewSubstate (Output impasse),
--          Tagged ApplyChosenOperator (Methodology chosenOperator output),
--          Tagged OutputResultsToEnvironment (Output output),
--          Tagged ResolveImpasses (Methodology chosenOperator ()),
--          Tagged GatherPSCMCycleStatisticsForCurrentIteration (Input statisticsForCurrentIteration),
--          Tagged ProcessPSCMCycleStatistics (Methodology statisticsForCurrentIteration statistics),
--          Tagged EmitPSCMCycleStatistics (Output statistics)
--        ]
--       r
--   ) =>
--   Sem r ()
-- pscmLoop = do
--   environmentalInput <- tag @GetEnvironmentalInput input
--   haltLoop <- tag @DecideToHaltLoop $ decide environmentalInput
--   unless haltLoop do
--     tag @ProcessEnvironmentalInput $ process environmentalInput
--     currentState <- tag @CurrentState input
--     proposals <- tag @ProposeOperators $ process currentState
--     chosen <- tag @EvaluateOperatorProposals $ process proposals
--     case chosen of
--       Right chosenOperator -> do
--         results <- tag @ApplyChosenOperator $ process chosenOperator
--         tag @ResolveImpasses $ process chosenOperator
--         tag @OutputResultsToEnvironment $ output results
--       Left impasse -> do
--         tag @CreateNewSubstate $ output impasse -- This is where we start descending into substates.
--     iterationStatistics <- tag @GatherPSCMCycleStatisticsForCurrentIteration input
--     statistics <- tag @ProcessPSCMCycleStatistics $ process iterationStatistics
--     tag @EmitPSCMCycleStatistics $ output statistics
--     pscmLoop



data AllOperatorProposals

data CurrentOperator

data SetOperatorSelectionInWorkingMemoryAndElaborate

data IsAtQuiescence


{-# INLINEABLE pscmPhase #-}
pscmPhase ::
  ( Members
      '[ Tagged GetEnvironmentalInput (Input input),
         Tagged ProcessEnvironmentalInput (Methodology input ()),
         Tagged CurrentState (Input currentState),
         Tagged DecideToHaltLoop (DecisionProcedureOn input),
         Tagged ProposeOperators (Methodology currentState operatorProposals),
         Tagged AllOperatorProposals (State operatorProposals),
         Tagged EvaluateOperatorProposals (Methodology operatorProposals (Either impasse chosenOperator)),
         Tagged CurrentOperator (State chosenOperator),
         Tagged CreateNewSubstate (Output impasse),
         Tagged SetOperatorSelectionInWorkingMemoryAndElaborate (Methodology chosenOperator ()),
         Tagged ApplyChosenOperator (Methodology chosenOperator ()),
         Tagged GetEnvironmentalOutputs (Input output),
         Tagged IsAtQuiescence (Input Bool),
         Tagged OutputResultsToEnvironment (Output output),
         --         Tagged ResolveImpasses (Methodology chosenOperator ()),
         Tagged GatherPSCMCycleStatisticsForCurrentIteration (Input statisticsForCurrentIteration),
         Tagged ProcessPSCMCycleStatistics (Methodology statisticsForCurrentIteration statistics),
         Tagged EmitPSCMCycleStatistics (Output statistics),
         Tagged PSCMLoopPhase (State PSCMLoopPhase)
       ]
      r
  ) =>
  Sem r () ->
  Sem r ()
pscmPhase elaborate = do
  environmentalInput <- tag @GetEnvironmentalInput input
  haltLoop <- tag @DecideToHaltLoop $ decide environmentalInput
  unless haltLoop do
    currentPhase <- tag @PSCMLoopPhase get
    case currentPhase of
      InputElaboration -> do
        tag @ProcessEnvironmentalInput $ process environmentalInput
        whileNotAtQuiescence elaborate
        tag @PSCMLoopPhase $ put OperatorProposalAndEvaluation
      OperatorProposalAndEvaluation -> do
        whileNotAtQuiescence elaborate
        currentState <- tag @CurrentState input
        proposals <- tag @ProposeOperators $ process currentState
        whenAtQuiescence do
          tag @AllOperatorProposals $ put proposals
          tag @PSCMLoopPhase $ put OperatorDecision
      OperatorDecision -> do
        proposals <- tag @AllOperatorProposals get
        chosen <- tag @EvaluateOperatorProposals $ process proposals
        case chosen of
          Right chosenOperator -> do
            tag @CurrentOperator $ put chosenOperator
            tag @PSCMLoopPhase $ put OperatorElaboration
          Left impasse -> do
            tag @CreateNewSubstate $ output impasse -- This is where we start descending into substates.
            tag @PSCMLoopPhase $ put InputElaboration
      OperatorElaboration -> do
        chosenOperator <- tag @CurrentOperator $ get
        tag @SetOperatorSelectionInWorkingMemoryAndElaborate $ process chosenOperator
        whenAtQuiescence do
          tag @PSCMLoopPhase $ put OperatorApplication
      OperatorApplication -> do
        chosenOperator <- tag @CurrentOperator get
        tag @ApplyChosenOperator $ process chosenOperator
        whenAtQuiescence do
          tag @PSCMLoopPhase $ put OutputToEnvironment
      OutputToEnvironment -> do
        results <- tag @GetEnvironmentalOutputs input
        tag @OutputResultsToEnvironment $ output results
        tag @PSCMLoopPhase $ put InputElaboration
    --        tag @ResolveImpasses $ process chosenOperator
    iterationStatistics <- tag @GatherPSCMCycleStatisticsForCurrentIteration input
    statistics <- tag @ProcessPSCMCycleStatistics $ process iterationStatistics
    tag @EmitPSCMCycleStatistics $ output statistics
  where
    --    pscmPhase
    whenAtQuiescence m = do
      atQuiescence <- tag @IsAtQuiescence input
      when atQuiescence m

    whileNotAtQuiescence m = do
      atQuiescence <- tag @IsAtQuiescence input
      unless atQuiescence do
        _ <- m
        whileNotAtQuiescence m

data OperatorSelection

data OperatorInSuperstate

data GetStateOperatorResolves

data GetAllChildStates

-- evaluateOperatorProposals :: (Each allProposals allProposals proposal proposal,
--                              PartialOrd proposal,
{-# INLINEABLE resolveImpasses #-}
resolveImpasses ::
  ( Members
      '[ Tagged CurrentState (Input currentState),
         Tagged OperatorInSuperstate (DecisionProcedureOn chosenOperator),
         Tagged GetStateOperatorResolves (Methodology chosenOperator resolvedState),
         Tagged GetAllChildStates (Methodology resolveStates childStates)
         --           Tagged RetractInstantiationSupportedWMEs (Methodology )
       ]
      r
  ) =>
  chosenOperator ->
  Sem r ()
resolveImpasses _chosenOperator = pure ()
