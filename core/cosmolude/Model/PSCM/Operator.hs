module Model.PSCM.Operator where

--import Model.DomainModelling

-- data OperatorProposal

-- data PendingProductionActivity

-- data ExaminingIndividualActivity

-- data DetermineOperatorSupport

-- data InstantiationSupported

-- data AnyPendingInstantiationSupportedActivity

-- data ProcessPendingActivity

-- data ApplyActivity

-- proposalPhase ::
--   ( Members
--       '[ Tagged PendingProductionActivity (Input totalPendingActivity),
--          Tagged AnyPendingInstantiationSupportedActivity (Methodology totalPendingActivity (Maybe instantiationSupportedActivity)),
--          Tagged ProcessPendingActivity (Methodology instantiationSupportedActivity changesToMake),
--          Tagged ApplyActivity (Output changesToMake)
--        ]
--       r
--   ) =>
--   Sem r ()
-- proposalPhase = do
--   while @AnyPendingInstantiationSupportedActivity \pending -> do
--     tag $ process pending




-- decisionPhase ::
--   ( Members
--       '[
--          Tagged GetCurrentState (Input currentState),
--          Tagged GetOperatorProposals (Methodology currentState allProposals),
--          Tagged EvaluateOperatorProposals (Methodology allProposals (Either impasse chosenOperator)),
--          Tagged CreateNewSubstate (Output impasse),
--          Tagged SelectNewOperator (Output chosenOperator)
--        ]
--       r
--   ) =>
--   Sem r ()
-- decisionPhase = do
--   currentState <- tag @GetCurrentState input
--   allProposals <- tag @GetOperatorProposals $  process currentState
--   operatorChoice <- tag @EvaluateOperatorProposals $ process allProposals
--   case operatorChoice of
--     Right chosenOperator -> tag @SelectNewOperator $ output chosenOperator
--     Left impasse -> tag @CreateNewSubstate $ output impasse

data AnyPendingActivity

-- processPendingActivity :: (Members '[] r) => Sem (Tagged ProcessPendingActivity (Methodology activityToProcess changesToMake) : r) x -> Sem r x
-- processPendingActivity = undefined

-- applicationPhase ::
--   ( Monoid changesToMake,
--     Members
--       '[ Tagged PendingProductionActivity (Input pendingActivity),
--          Tagged AnyPendingActivity (Methodology pendingActivity (Maybe activityToProcess)),
--          Tagged ProcessPendingActivity (Methodology activityToProcess changesToMake),
--          Tagged ApplyActivity (Output changesToMake)
--        ]
--       r
--   ) =>
--   Sem r ()
-- applicationPhase = do
--   while @AnyPendingActivity \pending -> do
--     tag @ProcessPendingActivity $ do
--       process pending

--  _ $ adaptToMonoidalOutput $ splitOutput (\pendingActivations pendingRetractions -> pure (pendingActivations, pendingRetractions)) $ combineInput pure $ processPendingActivity
--      runInputConst pending (adaptToInOutMonoid id $ splitOutput (\pendingActivations pendingRetractions -> pure (pendingActivations, pendingRetractions)) $ combineInput pure processPendingActivity)
-- (\pendingActivations pendingRetractions -> pure (pendingActivations, pendingRetractions))
