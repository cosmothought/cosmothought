﻿{-# LANGUAGE DuplicateRecordFields #-}
module Model.PSCM.Impasses where
import Data.Functor
import Data.Foldable
import Data.Traversable    
import Data.Kind (Type)
import Prelude (Eq, Show, Read, (.))
import Data.Hashable
import GHC.Generics (Generic)
import Control.Exception    
import Control.Lens

import Core.Identifiers
import qualified Data.Text as T           
import Data.Interned.Text
import Data.Interned
    
class ImpasseType ty where
    type ImpasseOperatorsType ty :: Type
    impasseType :: ty -> T.Text
    impasseType = unintern . impasseTypeSymbol
    impasseTypeSymbol :: ty -> InternedText
    impasseOperators :: Getter ty (ImpasseOperatorsType ty)
    
data TieImpasse operators = TieImpasse {_tieImpasseOperators :: operators}-- (HashSet ObjectIdentifier)
                  deriving stock (Eq, Show, Generic, Functor, Foldable, Traversable)
                  deriving anyclass (Exception, Hashable)

                                      
data ConflictImpasse operators = ConflictImpasse {_conflictImpasseOperators :: operators}
                       deriving stock (Eq, Show, Read, Generic, Functor, Foldable, Traversable)
                       deriving anyclass (Exception, Hashable)

                                           
data ConstraintFailureImpasse operators = ConstraintFailureImpasse {_constraintFailureImpasseOperators :: operators}
                                deriving stock (Eq, Show, Read, Generic, Functor, Foldable, Traversable)
                                deriving anyclass (Exception, Hashable)

                                                    

data NoChangeImpasse operators = StateNoChangeImpasse  { _noChangeImpasseOperators :: operators}
                     | OperatorNoChangeImpasse { _noChangeImpasseOperators :: operators}
  deriving stock (Eq, Show, Read, Generic, Functor, Foldable, Traversable)
  deriving anyclass (Exception, Hashable)


                                                   
makeLenses ''ConflictImpasse
makeLenses ''ConstraintFailureImpasse
makeLenses ''TieImpasse
makePrisms ''NoChangeImpasse
makeLenses ''NoChangeImpasse
instance ImpasseType (TieImpasse o) where
    type ImpasseOperatorsType (TieImpasse o) = o
    impasseTypeSymbol _ = "tie"
    impasseOperators  = tieImpasseOperators
instance ImpasseType (ConflictImpasse o) where
    type ImpasseOperatorsType (ConflictImpasse o) = o
    impasseTypeSymbol _ = "conflict"
    impasseOperators  = conflictImpasseOperators
instance ImpasseType (ConstraintFailureImpasse o) where
    type ImpasseOperatorsType (ConstraintFailureImpasse o) = o
    impasseTypeSymbol _ = "constraint-failure"
    impasseOperators  = constraintFailureImpasseOperators
instance ImpasseType (NoChangeImpasse o) where
    type ImpasseOperatorsType (NoChangeImpasse o) = o
    impasseTypeSymbol _ = "no-change"
    impasseOperators = noChangeImpasseOperators

                                                   
data Impasse' operators = Tie  (TieImpasse operators)
             | Conflict (ConflictImpasse operators)
             | ConstraintFailure (ConstraintFailureImpasse operators)
             | NoChange (NoChangeImpasse operators)
               deriving stock (Eq, Show, Generic, Functor, Foldable, Traversable)
               deriving anyclass (Exception, Hashable)
               
makePrisms ''Impasse'

instance ImpasseType (Impasse' o) where
    type ImpasseOperatorsType (Impasse' o) = o
    impasseTypeSymbol (Tie ti) = impasseTypeSymbol ti
    impasseTypeSymbol (Conflict ci) = impasseTypeSymbol ci
    impasseTypeSymbol (ConstraintFailure cfi) = impasseTypeSymbol cfi
    impasseTypeSymbol (NoChange nci) = impasseTypeSymbol nci
    impasseOperators = to \case
                             Tie ti -> ti^.impasseOperators
                             Conflict ci -> ci^.impasseOperators
                             ConstraintFailure cfi -> cfi^.impasseOperators
                             NoChange nci -> nci ^. impasseOperators
                                  
{-# INLINE operators #-}           
operators :: Getter (Impasse' o) o
operators = impasseOperators


data ImpasseResolutionType operatorProposals chosenOperator  = ImpasseResolved chosenOperator | ImpasseRegenerated (Impasse' operatorProposals) | ImpasseEliminated deriving (Eq, Show, Generic, Hashable)
makePrisms ''ImpasseResolutionType               
data ImpasseResolution' operatorProposals chosenOperator a = ImpasseResolution {_resolvedState :: StateIdentifier, _resolutionType :: ImpasseResolutionType operatorProposals chosenOperator, _resolutionData :: a} deriving stock (Eq, Show, Generic, Functor, Foldable, Traversable)
                                deriving anyclass (Exception, Hashable)
resolveImpasse :: StateIdentifier -> chosenOperator -> a -> ImpasseResolution' operatorProposals chosenOperator a
resolveImpasse s chosen a = ImpasseResolution s (ImpasseResolved chosen) a
regenerateImpasse :: StateIdentifier -> Impasse' operatorProposals -> a  -> ImpasseResolution' operatorProposals chosenOperator a
regenerateImpasse s newImpasse a = ImpasseResolution s (ImpasseRegenerated newImpasse) a
eliminateImpasse :: StateIdentifier -> a -> ImpasseResolution' operatorProposals chosenOperator a
eliminateImpasse s a = ImpasseResolution s ImpasseEliminated a
                                                               
makeLenses ''ImpasseResolution'    
