module Commands.Results where
import Core.Rule.Match
import Core.CoreTypes
import System.FilePath
import Cosmolude hiding (group, Named, Any(..))
import Data.Semigroup



import GHC.Exts hiding (Any)
import Control.Exception (SomeException)
import Data.Either
    

data CommandResult' metadata proof
  = SideEffect
  | Message CoreDoc
  | Firings (HashSet (RuleActivation' metadata proof), HashSet (RuleRetraction' metadata proof))
  | ValueResult Value
  | IdentifierResult Identifier
  | FileWrittenTo FilePath
  | CommandError (Either SomeException CoreDoc)
  | TestSucceeds
  | TestFails CoreDoc
  deriving (Show, Generic)

makeClassyPrisms ''CommandResult'
           
data CommandResults' metadata proof = CommandResults {_commandResults :: Seq (CommandResult' metadata proof)} deriving (Show)

instance IsList (CommandResults' metadata proof) where
  type Item (CommandResults' metadata proof) = CommandResult' metadata proof
  {-# INLINE fromList #-}
  fromList = CommandResults . GHC.Exts.fromList
  {-# INLINE toList #-}
  toList (CommandResults r) = GHC.Exts.toList r

makeLenses ''CommandResults'

{-# INLINE resultingIn #-}
resultingIn :: Review (CommandResults' metadata proof) [CommandResult' metadata proof]
resultingIn = unto GHC.Exts.fromList

instance Semigroup (CommandResults' metadata proof) where
  {-# INLINE CONLIKE (<>) #-}
  (CommandResults a) <> (CommandResults b) = CommandResults (a <> b)

instance Monoid (CommandResults' metadata proof) where
  {-# INLINE CONLIKE mempty #-}
  mempty = CommandResults mempty                
    
