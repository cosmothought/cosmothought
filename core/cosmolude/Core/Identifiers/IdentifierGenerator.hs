module Core.Identifiers.IdentifierGenerator (IDGen, newGlobalIdentifier, newLocalIdentifierInContext, runIDGen, newNodeIdentifier, newWMEIdentifier, runIDGenSTMWith, runIDGenWith, runIDGenGlobal, runIDGenSTMGlobal, globalIDState, newIDGenState, IdentifierGenerationState, seeds, newRandomIdentifier, resetIDGenState, HasIdentifierGenerationState(identifierGenerationState)) where

import Control.Concurrent.STM
import Control.Lens
import Polysemy.Transaction
import Core.Identifiers.IdentifierType
import Core.Identifiers (ObjectIdentifier(ObjectIdentifier))
import Core.Identifiers.IDGen
import Core.Utils.MetaTypes
import qualified Data.ByteString.Lazy as B
import Data.Maybe
import Data.UUID
import Core.Variable

import Focus
import qualified GHC.Conc as UNSAFE (unsafeIOToSTM)
import GHC.Stack
import Polysemy
import Polysemy.Reader
import StmContainers.Map as STMM
import System.Entropy
import qualified System.IO.Unsafe as UNSAFE (unsafePerformIO)
import Prelude
import Data.Text

data IdentifierGenerationState = IdGenState {_seeds :: STMM.Map Identifier Integer, _subVariables :: STMM.Map SymbolName Integer}

makeClassy ''IdentifierGenerationState

newIDGenState :: STM IdentifierGenerationState
newIDGenState = IdGenState <$>  STMM.new <*> STMM.new
resetIDGenState :: IdentifierGenerationState -> STM ()
resetIDGenState (IdGenState a b) = do
  STMM.reset a
  STMM.reset b
      
{-# NOINLINE globalIDState #-}
globalIDState :: IdentifierGenerationState
globalIDState = UNSAFE.unsafePerformIO (IdGenState <$>  STMM.newIO <*> STMM.newIO)

-- SAFETY: I think it's safe; resource exhaustaion would occur in IO anyway?
nextRandom' :: (HasCallStack) => STM UUID
nextRandom' =  do
  b <- fastRandom 16
  return $ fromJust . fromByteString $ B.fromStrict b
  where
    fastRandom nr = maybe (UNSAFE.unsafeIOToSTM $ getEntropy nr) pure =<< (UNSAFE.unsafeIOToSTM $ getHardwareEntropy nr)

-- TODO FIXME: Use a Random effect so we can make it seedable.
newRandomIdentifier :: (HasCallStack) => STM Identifier
newRandomIdentifier = fmap UniqueIdentifier nextRandom'

runIDGen :: forall r a. (HasCallStack, Members '[Transactional] r) => Sem (IDGen : r) a -> Sem r a
runIDGen sem = do
  s <- embedSTM newIDGenState
  runIDGenWith s sem
{-# INLINE runIDGen #-}

runIDGenGlobal :: forall r a. (HasCallStack, Members '[Transactional] r) => Sem (IDGen : r) a -> Sem r a
runIDGenGlobal sem = do
  runIDGenWith globalIDState sem
{-# INLINE runIDGenGlobal #-}

{-# INLINEABLE runIDGenWith #-}
runIDGenWith :: forall agent r a. (HasIdentifierGenerationState agent, HasCallStack, Members '[Transactional] r) => agent -> Sem (IDGen : r) a -> Sem r a
runIDGenWith agent sem | genState <- agent^.identifierGenerationState =  do
  runReader genState $ reinterpret handleTheEffect sem
  where
    handleTheEffect :: IDGen (Sem r') x -> Sem (Reader IdentifierGenerationState : r) x
    handleTheEffect NewGlobalIdentifier = {-# SCC newGlobalIdentifier #-}  embedSTM newRandomIdentifier
    handleTheEffect (NewLocalIdentifierInContext c) = newContextualIdent c
    handleTheEffect (NewObjectIdentifierBasedOnVariable var) = do
      s <- ask @IdentifierGenerationState
      ident <- embedSTM newRandomIdentifier
      i <- bumpSeed s (var^.variableName) subVariables
      let name = textToSymbolName $ ((symbolNameToText $ var^.variableName) <> "-" <> (pack (show i)))
      pure $ ObjectIdentifier name ident
    handleTheEffect (FreshVariableBasedOn (view variable -> base)) =
      do
        let sym = base^.variableName
        s <- ask @IdentifierGenerationState
        i <- newContextualIdent (base^.identifier)
        sub <- bumpSeed s sym subVariables
        let newName = textToSymbolName $ (flip append) ("___DOT" `append` (pack (show sub))) $ symbolNameToText sym
        pure (AVariable i newName)
    newContextualIdent c = do
      s <- ask @IdentifierGenerationState
      i <- bumpSeed s c seeds
      pure (ContextualIdentifier i c)
    -- Increment seed and return old value
    bumpSeed s c map = focusMap (cases (0, Set 1) (\a -> (a, Set (a + 1)))) c (s ^. map) 
      

{-# INLINE runIDGenSTMWith #-}
runIDGenSTMWith :: HasCallStack => IdentifierGenerationState -> Sem '[IDGen] a -> STM a
runIDGenSTMWith s = runFinal . embedToFinal . interpretTransactional' . runIDGenWith s . raiseUnder3

{-# INLINE runIDGenSTMGlobal #-}
runIDGenSTMGlobal :: HasCallStack => Sem '[IDGen] a -> STM a
runIDGenSTMGlobal = runIDGenSTMWith globalIDState
