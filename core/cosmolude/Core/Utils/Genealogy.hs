module Core.Utils.Genealogy where

import Control.Lens
import Control.Lens.Action
import Control.Lens.Mutable

class OwnsChildren ty container where
  ownedChildren :: Lens' ty (container ty)

class KnowsAncestors ty where
  ancestors :: Traversal' ty ty
