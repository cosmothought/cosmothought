{-# LANGUAGE Strict #-}
{-# OPTIONS_GHC -Wwarn=orphans #-}
module Core.Utils.Parsing.Errors where
import Core.Utils.MetaTypes hiding (errorCode)
import qualified Core.Utils.MetaTypes as QQ (errorCode)
import Data.Hashable
import GHC.Exts (SPEC(..))
import Polysemy.Transaction
import Control.Applicative
import Control.Category
import Control.Lens
import Control.Monad
import Core.Identifiers.IDGen
import Core.Utils.Prettyprinting
import Data.Bool
import Data.Foldable
import Data.Function (($))
import Data.Maybe
import qualified Data.Set as Set
import Data.String as DS (IsString (..))
import Data.Text (Text)
import Error.Diagnose hiding (defaultStyle)

import Control.Exception (Exception)
import GHC.Generics (Generic)
import Polysemy
import Prettyprinter
  ( Pretty,
    (<+>),
  )
import qualified Prettyprinter as PP
import Prettyprinter.Render.TerminalModified
import Text.Megaparsec hiding (State, many, match, noneOf, parse, parseMaybe, some, tokens)
import qualified Text.Megaparsec as MP
import Prelude (Either (..), Eq (..), Int,  Ord, Read, Show (..), String, const, flip, fromIntegral, lines, undefined, (++))

import Core.Error

type CosmicDiagnostic = Diagnostic DiagnosticMessageType

-- renderErrorCode ::    (ToErrorCode err => err -> DiagnosticMessageType 
-- renderErrorCode e = ( (getErrorCode e))))
                    
data ParsingAnnotations
  = CoreAnnotation CoreAnnotations
  | ParseAnnotation Annotation
  deriving (Generic)

data WhenParsingRuleLHS
  = ParsingStateTestPattern
  | ParsingOrdinaryPattern
  deriving (Eq, Ord, Generic, Show, Read, Hashable)
           
instance Renderable WhenParsingRuleLHS where
    render ParsingStateTestPattern = "rule state test pattern"
    render ParsingOrdinaryPattern = "rule condition"
data WhenParsingRule
  = ParsingCompleteRule
  | ParsingRuleLHS (Maybe WhenParsingRuleLHS)  (Maybe WMEField)
  | ParsingRuleRHS (Maybe WMEField)
  | RuleSemanticAnalysis
  deriving (Eq, Ord, Generic, Show, Read, Hashable)

makeClassyPrisms ''WhenParsingRule

data WhenParsing
  = ParsingRule WhenParsingRule
  | ParsingWME
  | ParsingExpression
  | ParsingUnspecified
  | ParsingDotPath
  | CompileTime WhenParsing
  deriving (Eq, Ord, Generic, Show, Read, Hashable)

alsoAtCompileTime :: (WhenParsing -> a) -> WhenParsing -> a
alsoAtCompileTime = alsoAtCompileTime' SPEC
alsoAtCompileTime' :: SPEC -> (WhenParsing -> a) -> WhenParsing -> a
alsoAtCompileTime' !sPEC f (CompileTime t) = alsoAtCompileTime' sPEC f t
alsoAtCompileTime' !_sPEC f t = f t
           
makeClassyPrisms ''WhenParsing

instance AsWhenParsingRule WhenParsing where
  {-# INLINE _WhenParsingRule #-}
  _WhenParsingRule = _ParsingRule

instance Renderable WhenParsing where
  render ParsingWME = "wme"
  render ParsingExpression = "expression"
  render (ParsingRule ParsingCompleteRule) = "rule"
  render (ParsingRule (RuleSemanticAnalysis)) = "and analysing rule" -- HACK HACK TODO FIXME: Convert "WhenParsing" to a more general ingestion pipeline description, starting with parsing, then analysis, then adding, etc...
  render (ParsingRule (ParsingRuleLHS (Just typeOfCond) (Just field))) = render field <+> "field in" <+> render typeOfCond
  render (ParsingRule (ParsingRuleLHS Nothing (Just field))) = render field <+> "field in rule left-hand side"
  render (ParsingRule (ParsingRuleLHS (Just typeOfCond) Nothing)) = render typeOfCond
  render (ParsingRule (ParsingRuleLHS Nothing Nothing)) = "rule left-hand side"
  render (ParsingRule (ParsingRuleRHS (Just field))) = render field <+> "field in rule RHS condition"
  render (ParsingRule (ParsingRuleRHS Nothing)) = "rule right-hand side"
  render (CompileTime when) = render when <+> "at compile time"
  render (ParsingDotPath) = "dot path"
  render ParsingUnspecified = "unspecified"


--------------------------------------------------------------------------------
-- WME Metadata parse errors
--------------------------------------------------------------------------------

data WMEMetadataParseError = WMEMetadataParseError' deriving (Eq, Ord, Generic, Show, Read, Hashable)
instance ToErrorCode WMEMetadataParseError where
    getErrorCode WMEMetadataParseError' = [QQ.errorCode|parsing/wme/metadata|]
makeClassyPrisms ''WMEMetadataParseError

instance Renderable WMEMetadataParseError where
  render = PP.viaShow

instance HasHintsWhen ctx WMEMetadataParseError msg where
  hintsWhenWithDefault _ _ = def

--------------------------------------------------------------------------------
-- WME parse errors
--------------------------------------------------------------------------------

data WMEParseError
  = VariableInWME
  | NumberOfFields Int Int
  | WMEMetadataParsingError WMEMetadataParseError
  deriving (Eq, Ord, Generic, Show, Read, Hashable)
instance ToErrorCode WMEParseError where
    getErrorCode VariableInWME = [QQ.errorCode|parsing/wme/variable_in_wme|]
    getErrorCode (NumberOfFields _a _b) = [QQ.errorCode|parsing/wme/number_of_fields|]
    getErrorCode (WMEMetadataParsingError e) = getErrorCode e
makeClassyPrisms ''WMEParseError

instance AsWMEMetadataParseError WMEParseError where
  {-# INLINE _WMEMetadataParseError #-}
  _WMEMetadataParseError = _WMEMetadataParsingError

instance Renderable WMEParseError where
  render VariableInWME = "variable in concrete wme"
  render (NumberOfFields expected actual) = "unexpected number of fields; expected:" <+> render expected <+> "actual:" <+> render actual
  render (WMEMetadataParsingError e) = render e

instance (IsString msg) => HasHintsWhen WhenParsing WMEParseError msg where
  hintsWhenWithDefault def w (WMEMetadataParsingError e) = hintsWhenWithDefault def w e
  hintsWhenWithDefault _ (ParsingWME) (VariableInWME) = [Note "Variables cannot appear in concrete WMEs; perhaps an object identifier was intended?"]
  hintsWhenWithDefault _ _ (NumberOfFields a b) = [Note . DS.fromString $ "This WME must have " ++ show b ++ "fields, but was given " ++ show a]
  hintsWhenWithDefault def _ VariableInWME = def

--------------------------------------------------------------------------------
-- WME Pattern parse errors
--------------------------------------------------------------------------------

data WMEPatternParseError = WMEPatternParseError' deriving (Eq, Ord, Show, Generic, Read, Hashable)

instance ToErrorCode WMEPatternParseError where
    getErrorCode WMEPatternParseError' = [QQ.errorCode|parsing/wme_pattern|]
                          
makeClassyPrisms ''WMEPatternParseError

instance Renderable WMEPatternParseError where
  render = PP.viaShow

instance HasHintsWhen ctx WMEPatternParseError msg where
  hintsWhenWithDefault def _ _ = def

--------------------------------------------------------------------------------
-- WME Preference parse errors
--------------------------------------------------------------------------------

data WMEPreferenceParseError = WMEPreferenceParseError' deriving (Eq, Ord, Show, Generic, Read, Hashable)

instance ToErrorCode WMEPreferenceParseError where
    getErrorCode WMEPreferenceParseError' = [QQ.errorCode|parsing/wme_preference|]
                             
makeClassyPrisms ''WMEPreferenceParseError

instance Renderable WMEPreferenceParseError where
  render = PP.viaShow

instance HasHintsWhen ctx WMEPreferenceParseError msg where
  hintsWhenWithDefault def _ _ = def


--------------------------------------------------------------------------------
-- Action parse errors
--------------------------------------------------------------------------------

data ActionParseError = NotACreationPreference deriving (Eq, Ord, Show, Generic, Read, Hashable)

makeClassyPrisms ''ActionParseError

instance Renderable ActionParseError where
  render NotACreationPreference = "working memory element pattern in an action with a non-creation preference"

instance IsString msg => HasHintsWhen ctx ActionParseError msg where
  hintsWhenWithDefault _ _ NotACreationPreference = [Note "A WME pattern can only occur with a (possibly implicit) creation (+) preference, or a rejection preference (-).", Note "Numeric preferences are not yet supported.", Hint "Other symbolic preferences don't make sense."]


instance ToErrorCode ActionParseError where
    getErrorCode NotACreationPreference = [QQ.errorCode|action/not_a_creation_preference|]

----------------------------------------
--- Rule semantic errors
----------------------------------------

-- $ruleSemanticErrors
--
-- When analysis discovers a rule contains invalid semantics, an error will be raised and prevent rule simplification.
    
data RuleSemanticError =
    -- | The rule has incompatible types; for example, a variable that matches a 'WME' object field cannot
    -- also have a numerical constraint.
    RuleHasIncompatibleTypes
    | IncompatibleVariableBindings
    | UnknownRuleSemanticsError
      deriving (Eq, Show, Ord, Exception, Generic, Read, Hashable)
instance ToErrorCode RuleSemanticError where
    getErrorCode RuleHasIncompatibleTypes = [QQ.errorCode|rule/incompatible_types|]
    getErrorCode IncompatibleVariableBindings = [QQ.errorCode|rule/incompatible_variable_bindings|]
    getErrorCode UnknownRuleSemanticsError = [QQ.errorCode|rule/unknown_rule_semantics|]                                                
instance Renderable RuleSemanticError where
    render RuleHasIncompatibleTypes = "Rule has incompatible types"
    render IncompatibleVariableBindings = "Rule has incompatible variable bindings"
    render UnknownRuleSemanticsError = "Unknown semantic error when analysing rule"
instance IsString msg => HasHintsWhen ctx RuleSemanticError msg where
  hintsWhenWithDefault _ _ (RuleHasIncompatibleTypes ) = [Note "Variables must have compatible types in rules."]
  hintsWhenWithDefault _ _ (IncompatibleVariableBindings) = [Note "Variable bindings must be able to be merged."]
  hintsWhenWithDefault def _ UnknownRuleSemanticsError = def
--------------------------------------------------------------------------------
-- Rule parse errors
--------------------------------------------------------------------------------
data RuleParseError
  = RuleLHSEmpty
  | RuleLHSStateTestPatternIsntPositive
  | RuleLHSStateTestPatternHasConstraints
  | RuleLHSNoStateTestPattern
  | RuleLHSStateTestPatternDoesntTestStateVariable
  | RuleRHSEmpty
  | InvalidAction ActionParseError
  | InvalidRuleSemantics RuleSemanticError
  deriving (Eq, Ord, Show, Generic, Read, Hashable)

makeClassyPrisms ''RuleParseError

instance ToErrorCode RuleParseError where
    getErrorCode RuleLHSEmpty = [QQ.errorCode|parsing/rule/lhs_empty|]
    getErrorCode RuleRHSEmpty = [QQ.errorCode|parsing/rule/rhs_empty|]
    getErrorCode RuleLHSStateTestPatternIsntPositive = [QQ.errorCode|parsing/rule/state_test_pattern/isnt_positive|]
    getErrorCode RuleLHSStateTestPatternHasConstraints = [QQ.errorCode|parsing/rule/state_test_pattern/has_constraints|]                                                       
    getErrorCode RuleLHSNoStateTestPattern = [QQ.errorCode|parsing/rule/state_test_pattern/missing|]
    getErrorCode RuleLHSStateTestPatternDoesntTestStateVariable = [QQ.errorCode|parsing/rule/state_test_pattern/doesnt_test_state_object_variable|]
    getErrorCode (InvalidAction e) = getErrorCode e
    getErrorCode (InvalidRuleSemantics e) = getErrorCode e                                     
                 
instance Renderable RuleParseError where
  render RuleLHSEmpty = "rule left-hand side empty"
  render RuleRHSEmpty = "rule right-hand side empty"
  render RuleLHSNoStateTestPattern = "no state test pattern"
  render RuleLHSStateTestPatternHasConstraints = "state test pattern has constraints"
  render RuleLHSStateTestPatternIsntPositive = "state test pattern isn't positive"
  render RuleLHSStateTestPatternDoesntTestStateVariable = "state test pattern missing object variable"
  render (InvalidAction e) = render e
  render (InvalidRuleSemantics e) = render e

instance IsString msg => HasHintsWhen ctx RuleParseError msg where
  hintsWhenWithDefault _ _ RuleLHSEmpty = [Note "A rule must have at least one condition.", Hint "If a rule didn't have any conditions, it would always match, no matter what."]
  hintsWhenWithDefault _ _ RuleRHSEmpty = [Note "A rule must have at least one action.", Hint "If a rule didn't have any actions, it would never have any effect, apart from taking up resources."]
  hintsWhenWithDefault _ _ RuleLHSNoStateTestPattern = [Note "The rule doesn't start with a valid state test pattern.", Hint "A state test pattern is a positive condition which has a state variable for the object."]
  hintsWhenWithDefault _ _ RuleLHSStateTestPatternIsntPositive = [Note "The rule doesn't start with a valid state test pattern.", Hint "A state test pattern is a positive condition which has a state variable for the object."]
  hintsWhenWithDefault _ _ RuleLHSStateTestPatternDoesntTestStateVariable = [Note "The rule doesn't start with a valid state test pattern.", Hint "A state test pattern is a positive condition which has a state variable for the object."]
  hintsWhenWithDefault _ _ RuleLHSStateTestPatternHasConstraints = [Note "This is not a fundamental limitation of the logic, but rather a technical implementation detail", Hint "Add an equivalent constraint elsewhere."]
  hintsWhenWithDefault def w (InvalidAction e) = hintsWhenWithDefault def w e
  hintsWhenWithDefault def w (InvalidRuleSemantics e) = hintsWhenWithDefault def w e
--------------------------------------------------------------------------------
-- Top-level parsing error codes
--------------------------------------------------------------------------------

data ParsingErrorCode
  = WMEParsingError WMEParseError
  | WMEPatternParsingError WMEPatternParseError
  | WMEPreferenceParsingError WMEPreferenceParseError
  | RuleParsingError RuleParseError
  | UnknownError
  deriving (Eq, Ord, Show, Generic, Read, Hashable)
instance ToErrorCode ParsingErrorCode where
    getErrorCode (WMEParsingError e) = getErrorCode e
    getErrorCode (WMEPatternParsingError e) = getErrorCode e
    getErrorCode (WMEPreferenceParsingError e) = getErrorCode e
    getErrorCode (RuleParsingError e) = getErrorCode e
    getErrorCode UnknownError = [QQ.errorCode|unknown_error|]
makeClassyPrisms ''ParsingErrorCode

instance AsWMEParseError ParsingErrorCode where
  {-# INLINE _WMEParseError #-}
  _WMEParseError = _WMEParsingError

instance AsWMEPatternParseError ParsingErrorCode where
  {-# INLINE _WMEPatternParseError #-}
  _WMEPatternParseError = _WMEPatternParsingError

instance AsRuleParseError ParsingErrorCode where
  {-# INLINE _RuleParseError #-}
  _RuleParseError = _RuleParsingError

instance (IsString msg) => HasHintsWhen WhenParsing ParsingErrorCode msg where
  hintsWhenWithDefault def  w (WMEParsingError e) = hintsWhenWithDefault def w e
  hintsWhenWithDefault def w (WMEPatternParsingError e) = hintsWhenWithDefault def w e
  hintsWhenWithDefault def w (RuleParsingError e) = hintsWhenWithDefault def w e
  hintsWhenWithDefault def w (WMEPreferenceParsingError e) = hintsWhenWithDefault def w e
  hintsWhenWithDefault _ _ UnknownError = [Note "Note to self: Figure out what caused this, and make it an explicit error!"]
instance Renderable ParsingErrorCode where
  render (WMEParsingError e) = render e
  render (WMEPatternParsingError e) = render e
  render (RuleParsingError e) = render e
  render (WMEPreferenceParsingError e) = render e
  render (UnknownError) = "<<Unknown error>>"


--------------------------------------------------------------------------------
-- Top-level parsing error
--------------------------------------------------------------------------------

data ParsingError = ParsingError { _whenParsing :: WhenParsing, _errorSpan :: (Maybe (SourcePos, SourcePos)), _structuredErrorCode :: ParsingErrorCode} deriving (Eq, Ord, Generic, Read)

makeClassy ''ParsingError

instance GetErrorContext ParsingError WhenParsing where
    hintContext = view whenParsing
    defaultContext = ParsingUnspecified
           
instance ToErrorCode ParsingError where 
    getErrorCode e =  getErrorCode (e^.structuredErrorCode)


                 
getErrorType :: IsString msg => ParseErrorBundle s ParsingError -> msg
getErrorType _ = "Parse error"

instance  Renderable ParsingError where
  render (ParsingError whenParsing _pos errorCode) =  -- PP.vsep (
                                                               ("when parsing" <+> render whenParsing <> ":" <+> render errorCode) -- :
                                                              --(renderNote @Text <$> (hintsWhen @ParsingErrorCode @Text whenParsing errorCode)))

instance Show ParsingError where
    show = show . render
renderNote :: Renderable msg => Note msg -> CoreDoc
renderNote (Note n)  = "Note:" <+> render n
renderNote (Hint h) = "Hint:" <+> render h

instance ShowErrorComponent ParsingError where
    showErrorComponent = show . render
    
instance DS.IsString msg => HasMarker ParsingError msg where
    errorMarker = Where . DS.fromString . showErrorComponent

instance (IsString msg) => HasHintsWhen WhenParsing ParsingError msg where
  hintsWhenWithDefault def _ (ParsingError w _errSpan e) = hintsWhenWithDefault def w e

type Parser r a = (Members '[IDGen, Transactional] r) => ParsecT ParsingError Text (Sem r) a




-- | Transforms a megaparsec 'MP.ParseErrorBundle' into a well-formated 'Diagnostic' ready to be shown.
diagnosticFromBundle' ::
  forall s err msg.
  (Stream s, VisualStream s, TraversableStream s,  Renderable err, ToErrorCode err, HasParsingError err, ShowErrorComponent err, HasMarker err msg, ErrorCodeRendering msg, IsString msg, GetErrorContext err WhenParsing, HasHintsWhen WhenParsing err msg) =>
  -- | How to decide whether this is an error or a warning diagnostic
  (MP.ParseError s err -> Bool) ->
  Maybe msg ->
  msg ->
  -- | Default hints when trivial errors are reported
  Maybe [Note msg] ->
  -- | The bundle to create a diagnostic from
  MP.ParseErrorBundle s err ->
  Diagnostic msg
diagnosticFromBundle' isError defaultCode defaultMessage (fromMaybe [] -> trivialHints) MP.ParseErrorBundle {..} =
  foldl addReport def (toLabeledPosition <$> bundleErrors)
  where
    msg = "Parse error"
    
    toLabeledPosition :: MP.ParseError s err -> Report msg
    toLabeledPosition error@(FancyError _ errs')
      | errs <-
          [ (pos, e) | (ErrorCustom e) <- Set.toList errs', let pos = (e ^. errorSpan)
          ] =
          let locatedMessages =
                errs <&> \(pos, err) ->
                  ( ( case pos of
                        Just (start, stop) ->
                          Position
                            ((fromIntegral . MP.unPos . sourceLine) start, (fromIntegral . MP.unPos . sourceColumn) start)
                            ((fromIntegral . MP.unPos . sourceLine) stop, (fromIntegral . MP.unPos . sourceColumn) stop)
                            (sourceName start)
                        Nothing ->
                          ( let (_, pos) = MP.reachOffset (MP.errorOffset error) bundlePosState
                             in errorPosition (MP.pstateSourcePos pos)
                          )
                    ),
                    errorMarker err
                  )
              locatedWithThis = locatedMessages & _head %~ (\case
                                                               (p, Where e) -> (p, This e)
                                                               a -> a)
              code = errs^? folded . _2 . to getErrorCode . to renderErrorCode
           in flip
                (if isError error then Err code  msg else Warn code msg)
                (errs ^.. traversed . _2 . to hintsInContext . traversed)
                locatedWithThis
    toLabeledPosition error =
      let (_, pos) = MP.reachOffset (MP.errorOffset error) bundlePosState
          source = errorPosition (MP.pstateSourcePos pos)
          msgs = DS.fromString  <$> lines (MP.parseErrorTextPretty error)
       in flip
            (if isError error then Err defaultCode defaultMessage else Warn defaultCode defaultMessage)
            (hintsInContextWithDefault trivialHints error)
            case msgs of
                 [] -> [(source, This "<<Unknown error>>")]
                 x:xs -> (source, This x) : fmap (\m -> (source, Where m)) xs

instance GetErrorContext e WhenParsing => GetErrorContext (MP.ParseError s e) WhenParsing where
  defaultContext = ParsingUnspecified
  
-- | Creates an error diagnostic from a megaparsec 'MP.ParseErrorBundle'.
errorDiagnosticFromBundle' ::
  forall msg s.
  (msg ~ DiagnosticMessageType, IsString msg, MP.Stream s,  MP.ShowErrorComponent ParsingError, MP.VisualStream s, MP.TraversableStream s) =>
  -- | An optional error code
  Maybe msg ->
  -- | The error message of the diagnostic
  msg ->
  -- | Default hints when trivial errors are reported
  Maybe [Note msg] ->
  -- | The bundle to create a diagnostic from
  MP.ParseErrorBundle s ParsingError ->
  Diagnostic msg
errorDiagnosticFromBundle' = diagnosticFromBundle'  (const True)

parsingErrorToReport :: ParsingError -> Report msg
parsingErrorToReport = undefined

diagnose ::
    forall ctx msg.
  (DiagnosticMessageType ~ msg, IsString msg, HasHintsWhen ctx ParsingError msg) =>
  ParseErrorBundle DiagnosticMessageType ParsingError ->
  Diagnostic msg
diagnose bundle = errorDiagnosticFromBundle' (Just "Parse error" ) (getErrorType bundle) Nothing bundle

maybeDiagnose :: forall ctx msg a. 
  (DiagnosticMessageType ~ msg, IsString msg, HasHintsWhen ctx ParsingError msg ) =>
  Either (ParseErrorBundle DiagnosticMessageType ParsingError) a ->
  Either (Diagnostic msg) a
maybeDiagnose (Right res) = (Right res)
maybeDiagnose (Left bundle) = Left (diagnose @ctx bundle)



                              
defaultStyle' :: PP.Doc Annotation -> CoreDoc
defaultStyle' = reAnnotate DiagnosticAnnotation

diagnosticToPrintableMessage :: Pretty msg => Diagnostic msg -> DiagnosticMessageType
diagnosticToPrintableMessage msg  = renderStrict $ simplifyCDS (PP.layoutPretty PP.defaultLayoutOptions (PP.unAnnotate (PP.fuse PP.Deep $ prettyDiagnostic True 4 msg)))


diagnosticToPrettyCoreDoc :: Pretty msg => Diagnostic msg -> CoreDoc
diagnosticToPrettyCoreDoc msg =   defaultStyle' (PP.fuse PP.Deep $ prettyDiagnostic True 4 msg)
                                    
diagnosticToPrettyMessage :: Pretty msg => Diagnostic msg -> DiagnosticMessageType
diagnosticToPrettyMessage msg = renderStrict $ simplifyCDS  (PP.layoutPretty PP.defaultLayoutOptions (defaultStyle $ diagnosticToPrettyCoreDoc msg))

diagnosticToPrettyString :: (Pretty msg, Renderable msg) => Diagnostic msg -> String
diagnosticToPrettyString = show . render . diagnosticToPrettyMessage
    
errorWhenParses :: WhenParsing -> Parser r a -> (WhenParsing -> Maybe ParsingError) -> Parser r b
errorWhenParses w parser errorGen =
  hidden $ case (errorGen w) of
    Just err -> do
      start <- getSourcePos
      end <- lookAhead . try $ (parser *> getSourcePos)
      _ <- parser
      fancyFailure (Set.singleton . ErrorCustom $ err & errorSpan .~ Just (start, end))
    Nothing -> mzero
