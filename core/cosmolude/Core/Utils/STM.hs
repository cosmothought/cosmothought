{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE LinearTypes #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Core.Utils.STM where
import Polysemy.Transaction
--import Data.Traversable
import Prelude(Ord, Eq)
import qualified Data.Map.Strict as DM
-- (foldlM')
import qualified Data.Map.Merge.Strict as DM
import Control.Applicative
import Control.Comonad
import Control.Concurrent.STM 
import Control.Lens
import Control.Lens.Action
import Control.Lens.Action.Internal
import Control.Monad (Monad)
import Control.Monad.Catch
import qualified Control.Monad.STM.Class as STMC
import Data.Bool
import Data.Foldable as DF (traverse_)
import Data.Function
import qualified Data.HashSet as HS

import Data.Hashable
import Data.Monoid
--import Data.Profunctor
--import Data.Profunctor.Rep
--import Data.Profunctor.Sieve
import Data.Sequence
import DeferredFolds.UnfoldlM as UnF
import Focus (Change (..), Focus)
import qualified Focus
import ListT
import Polysemy

--import Polysemy.Embed
--import Polysemy.Error
import Polysemy.NonDet
import StmContainers.Map as STMM
import qualified StmContainers.Map as STM
import StmContainers.Multimap as STMMM
import StmContainers.Set as STMS
--import qualified Unsafe.Linear as Unsafe
import Data.Refined
import Theory.Named
--import qualified Control.Concurrent.Classy.STM.TMVar as STMC
import GHC.Base (($!))


-- {-# INLINE modifyTMVar #-}
-- modifyTMVar :: STMC.MonadSTM stm =>  STMC.TMVar stm a -> (a -> a) -> stm ()
-- modifyTMVar var f = do
--   a <- STMC.takeTMVar var
--   STMC.putTMVar var $! f a
{-# INLINE modifyTMVar #-}
modifyTMVar :: TMVar a -> (a -> a) -> STM ()
modifyTMVar var f = do
  a <- takeTMVar var
  putTMVar var $! f a
stateTMVar :: TMVar s -> (s -> (a, s)) -> STM a
stateTMVar var f = do
   s <- takeTMVar var
   let (a, s') = f s -- since we destructure this, we are strict in f
   putTMVar var s'
   pure a
{-# INLINE stateTMVar #-}

{-# INLINE addRefinedToSet #-}
addRefinedToSet ::  (Members '[Transactional] r , Hashable (a ? prop)) =>  (a ~~ name) ::: prop name -> STMS.Set (a ? prop) -> Sem r ()
addRefinedToSet item set = insertInSet (unname item) set




{- EW EW EW orphan instance -}
instance (MonadCatch (Sem r), Members '[Embed STM, NonDet] r) => STMC.MonadSTM (Sem r) where
  type TVar (Sem r) = TVar
  {-# INLINE newTVar #-}
  newTVar = embed . newTVar

  {-# INLINE readTVar #-}
  readTVar = embed . readTVar

  {-# INLINE writeTVar #-}
  writeTVar var val = embed $ writeTVar var val

-- filteredByM :: forall i m a r p f. (Effective m r f, Indexable i p, Applicative f) => MonadicFold m a i -> IndexedAction i m a a -- p a (f a) -> a -> f a
-- filteredByM p f val = do
--   i <- val ^!? p
--   case (i :: Maybe i) of
--     Nothing -> ineffective @m @r @f $ pure val
--     Just witness -> ineffective @m @r $ indexed f witness val

{-# INLINE extractSet #-}
{-# SCC extractSet #-}

-- | Convert a stm-containers Set to a HashSet
extractSet :: (Hashable a,  Members '[Transactional] r) => Action (Sem r) (STMS.Set a) (HS.HashSet a)
extractSet = extractAndActOnSet id

{-# INLINEABLE extractAndActOnSet #-}
{-# SCC extractAndActOnSet #-}

-- | Convert an stm-containers Set to a HashSet, performing an Action.
extractAndActOnSet :: ( Members '[Transactional] r , Hashable a) => Action (STM) s a -> Action (Sem r) (STMS.Set s) (HS.HashSet a)
extractAndActOnSet f =
  act \s ->
    Polysemy.Transaction.foldlM'
      ( \current input ->
          do
            res <- input ^! f
            pure (current <> HS.singleton res)
      )
      HS.empty
      (STMS.unfoldlM s)

{-# INLINE mapSetM_ #-}
mapSetM_ :: (Members '[Transactional] r) => (Action (STM) a ()) -> Action (Sem r) (STMS.Set a) ()
mapSetM_ f = act $ \s -> embedSTM $ UnF.mapM_ (perform f) (STMS.unfoldlM s)

{-# INLINE actAndDelete #-}
actAndDelete :: (Hashable a, Members '[Transactional] r) => (Action (Sem r) a ()) -> Action (Sem r) (STMS.Set a) ()
actAndDelete a = act \s -> do
  hs <- embedSTM . toList . STMS.listT $ s
  hs ^! folded . a
  embedSTM (STMS.reset s)

{-# INLINE elems #-}
{-# SCC elems #-}
elems :: Members '[Transactional] r =>  MonadicFold (Sem r) (STMS.Set item) item -- :: (Effective STM item f, Applicative f) => (item -> f item) -> STMS.Set item -> f (STMS.Set item)
elems inj s =   effective  do
  xs <- embedSTM $ toList $  STMS.listT s
  ineffective $ DF.traverse_ inj xs

{-# INLINE elemsM #-}
{-# SCC elemsM #-}
elemsM ::  Members '[Transactional] r => MonadicFold (Sem r) (STM.Map k v) (k, v) -- :: (Effective STM item f, Applicative f) => (item -> f item) -> STMS.Set item -> f (STMS.Set item)
elemsM inj s = effective  do
  xs <- embedSTM $ toList  $ STM.listT s
  ineffective $ DF.traverse_ inj xs

{-# INLINE elemsMM #-}
{-# SCC elemsMM #-}
elemsMM ::  Members '[Transactional] r => MonadicFold (Sem r) (STMMM.Multimap k v) (k, v) -- :: (Effective STM item f, Applicative f) => (item -> f item) -> STMS.Set item -> f (STMS.Set item)
elemsMM inj s = effective  do
  xs <- embedSTM $ toList  $ STMMM.listT s
  ineffective $ DF.traverse_ inj xs


{-# INLINE toSeq #-}
{-# SCC toSeq #-}
toSeq ::  Members '[Transactional] r => Action (Sem r) (STMS.Set a) (Seq a)
toSeq = act \s ->  do
  fmap fromList $ embedSTM $ toList  $  STMS.listT $ s

{-# INLINE foldSet #-}
{-# SCC foldSet #-}
foldSet :: (Hashable a, Monoid b,  Members '[Transactional] r ) => (Action (Sem r) a b) -> Action (Sem r) (STMS.Set a) b
foldSet a = act \s -> do
  hs <- embedSTM $ toList $ STMS.listT $ s
  hs ^! folded . a

{-# INLINE inserting #-}
{-# SCC inserting #-}
inserting ::  (Members '[Transactional] r,  Hashable a) => a -> Action (Sem r) (STMS.Set a) ()
inserting item = act (insertInSet item)

                 
{-# INLINE insertingTested #-}
{-# SCC insertingTested #-}
insertingTested :: ( Members '[Transactional] r ,Hashable a) => a -> Action (Sem r) (STMS.Set a) Bool
insertingTested item = (act (focusSet (Focus.testingIfInserts (Focus.insert ())) item)) . _2
{-# INLINE insertingMTested #-}
{-# SCC insertingMTested #-}
insertingMTested ::  (Members '[Transactional] r, Hashable k) => k -> v -> Action (Sem r) (STMM.Map k v) Bool
insertingMTested k v = (act (focusMap (Focus.testingIfInserts (Focus.insert v)) k)) . _2

{-# INLINE deleting #-}
{-# SCC deleting #-}
deleting ::( Members '[Transactional] r , Hashable a) => a -> Action (Sem r) (STMS.Set a) ()
deleting item = act (deleteFromSet item)
{-# INLINE deletingKVM #-}
deletingKVM :: (Hashable key,  Members '[Transactional] r ) => key  -> Action (Sem r) (STMM.Map key value) ()
deletingKVM k  = act (\s -> deleteFromMap k s)

{-# INLINE deletingKVMM #-}
deletingKVMM :: (Hashable key, Hashable value,  Members '[Transactional] r ) => key -> value -> Action (Sem r) (STMMM.Multimap key value) ()
deletingKVMM k v = act (\s -> deleteFromMultimap v k s)

{-# INLINE insertingKVMM #-}
insertingKVMM :: (Hashable key, Hashable value,  Members '[Transactional] r ) => key -> value -> Action (Sem r) (STMMM.Multimap key value) ()
insertingKVMM k v = act (\s -> insertInMultimap v k s)

{-# INLINE insertingKVM #-}
insertingKVM :: (Hashable key, Hashable value,  Members '[Transactional] r ) => key -> value -> Action (Sem r) (STMM.Map key value) ()
insertingKVM k v = act (\s -> insertInMap v k s)

{-# INLINEABLE getOrCreateFocus #-}
getOrCreateFocus :: Monad m => m a -> Focus a m a
getOrCreateFocus create =
  Focus.casesM
    ( do
        val <- create
        pure (val, Set val)
    )
    (\a -> pure (a, Leave))

{-# INLINE multimapAsMap #-}    
multimapAsMap :: (Members '[Transactional] r) => Action (Sem r) (STMMM.Multimap k v) (STM.Map k (STMS.Set v))
multimapAsMap = act multimapToMapOfSets
{-# INLINE multimapKeys #-}
multimapKeys :: (Members '[Transactional] r) => MonadicFold (Sem r) (STMMM.Multimap k v) k
multimapKeys = multimapAsMap.elemsM . _1
{-# INLINE getOrCreate #-}
getOrCreate :: (Hashable key,  Members '[Transactional] r ) => (Action (STM) key value) -> key -> Action (Sem r) (STM.Map key value) value
getOrCreate action key = act $ \map -> focusMap (getOrCreateFocus (key ^! action)) key map



{-# INLINE mapMergeOnA_ #-}
mapMergeOnA_ :: ( Hashable k, Ord k, Hashable c ,Members '[Transactional] r) =>
             (k -> c -> Sem r ()) ->
             DM.WhenMissing (Sem r) k a c ->
             DM.WhenMissing (Sem r) k b c ->
             DM.WhenMatched (Sem r) k a b c ->
             STMM.Map k a ->
             STMM.Map k b ->
             (Sem r) ()
mapMergeOnA_ action missingA missingB matched as bs =  do
      aContents <- DM.fromList <$> as^!!elemsM
      bContents <- DM.fromList <$> bs^!!elemsM
      merged <- DM.mergeA missingA missingB matched aContents bContents
      (flip DM.traverseWithKey) merged \key value -> do
        action key value
      pure ()



{-# INLINE mapMergeA #-}
mapMergeA :: ( Hashable k, Ord k, Hashable c,  Members '[Transactional] r) =>
             DM.WhenMissing (Sem r) k a c ->
             DM.WhenMissing (Sem r) k b c ->
             DM.WhenMatched (Sem r) k a b c ->
             STMM.Map k a ->
             STMM.Map k b ->
             Sem r (STMM.Map k c)
mapMergeA missingA missingB matched as bs =  do
      finalMap <- newMap
      mapMergeOnA_ (\key value -> finalMap^!insertingKVM key value) missingA missingB matched as bs
      pure finalMap

{-# INLINE differenceWithKVM #-}
differenceWithKVM :: (Hashable key, Ord key, Hashable a,  Members '[Transactional] r ) => (key -> a -> b -> a) -> STMM.Map key a -> STMM.Map key b -> Sem r (STMM.Map key a)
differenceWithKVM f = mapMergeA DM.preserveMissing DM.dropMissing (DM.zipWithMatched f)

{-# INLINE differenceKVM #-}
differenceKVM :: (Hashable key, Ord key, Hashable value,  Members '[Transactional] r ) => STMM.Map key value -> STMM.Map key otherValue -> Sem r (STMM.Map key value)
differenceKVM = differenceWithKVM (\ _key a _b -> a)

{-# INLINE subtractingWithKVM #-}
subtractingWithKVM :: (Hashable key, Ord key, Hashable a,  Members '[Transactional] r ) => (key -> a -> b -> a) -> STMM.Map key a -> STMM.Map key b -> Sem r ()
subtractingWithKVM f a b = mapMergeOnA_ (\key _value -> a^!deletingKVM key) DM.preserveMissing DM.dropMissing (DM.zipWithMatched f) a b

{-# INLINE subtractingKVM #-}
subtractingKVM :: (Hashable key, Ord key, Hashable value,  Members '[Transactional] r) => STMM.Map key value -> STMM.Map key otherValue -> Sem r ()
subtractingKVM = subtractingWithKVM (\ _key a _b -> a)

{-# INLINE adjust #-}
adjust :: (Eq key, Hashable key,  Members '[Transactional] r) => key -> (value -> value) -> STMM.Map key value -> Sem r ()
adjust key f map =  focusMap (Focus.adjust f) key map
