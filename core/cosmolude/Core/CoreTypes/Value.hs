﻿{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE NoFieldSelectors #-}
{-# OPTIONS_GHC -funbox-strict-fields #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
module Core.CoreTypes.Value
  ( 
    parseValue,
    parseConstantValue,
    parseSymbolicValue,
    symbolicValue,
    oid,
    AsIdentifier (..),
    Value (..),
    AsValue(..),
    ConstantValue (..),
    asRational,
    AsSymbolicValue (..),
    SymbolicValue (..),
    AsConstantValue (..),
    SymbolName,
    symbol,
    identity,
    constantValue,
    operatorSymbol,
    operatorValue
  )
where


-- import Algebra.PartialOrd
-- TODO: Implement a partial order  


import Core.Identifiers

import Core.Utils.MetaTypes
import Core.Identifiers.IdentifierType
import Core.Utils.Rendering.CoreDoc
import GHC.Generics
import Control.Lens
import Polysemy
import Core.Utils.Parsing
import Prelude hiding ((.))


import Data.String
import Prettyprinter

import Control.Category ((.))

import Data.Hashable

import Data.Ratio ((%))



import Text.Megaparsec.Char.Lexer hiding (lexeme, symbol)
-- import Text.Megaparsec.Debug

import GHC.Float (Floating(..), log)

import Language.Haskell.TH.Syntax
--------------------------------------------------------------------------------
-- Values
--------------------------------------------------------------------------------
data SymbolicValue = SymbolicIdentity {_identity :: Identifier} | Symbol {_symbol :: SymbolName} | ObjectIdentity {_oid :: ObjectIdentifier} deriving (Eq, Ord, Generic, Hashable, Show, Renderable, Lift)
makeLenses ''SymbolicValue
makeClassyPrisms ''SymbolicValue
parseSymbolicValue :: Members '[ObjectIdentityMap x] r =>  WhenParsing -> Parser r SymbolicValue
parseSymbolicValue w = ((try $ SymbolicIdentity <$> parseIdentifier) <?> "Symbolic identity") <|> ((try $ Symbol <$> (lexeme parseSymbolName)) <?> "Symbol") <|> ((ObjectIdentity <$> parseObjectIdentifierLookup w) <?> "OID")

instance IsString SymbolicValue where
  {-# INLINE fromString #-}
  fromString s = Symbol . textToSymbolName . fromString $ s

instance AsObjectIdentifier SymbolicValue where
    {-# INLINE _ObjectIdentifier #-}
    _ObjectIdentifier = _ObjectIdentity


data ConstantValue = IntValue Integer | RationalValue Rational | FloatValue Double deriving (Read, Show, Generic, Hashable, Renderable, Lift)

{-# INLINE CONLIKE asRational #-}
asRational :: ConstantValue -> Rational
asRational (RationalValue r) = r
asRational (IntValue v) = toRational v
asRational (FloatValue v) = toRational v

instance Eq ConstantValue where
  {-# INLINE (==) #-}
  a == b = asRational a == asRational b
makeClassyPrisms ''ConstantValue
instance Ord ConstantValue where
  {-# INLINE (<=) #-}
  a <= b = asRational a <= asRational b
instance Num ConstantValue where
  {-# INLINE CONLIKE (+) #-}
  (FloatValue a) + (FloatValue b) = FloatValue (a + b)
  (IntValue a) + (IntValue b) = IntValue (a + b)
  (RationalValue a) + (RationalValue b) = RationalValue (a + b)
  a + b = RationalValue ((asRational a) + (asRational b))

  {-# INLINE CONLIKE (-) #-}
  (FloatValue a) - (FloatValue b) = FloatValue (a - b)
  (IntValue a) - (IntValue b) = IntValue (a - b)
  (RationalValue a) - (RationalValue b) = RationalValue (a - b)
  a - b = RationalValue ((asRational a) - (asRational b))

  {-# INLINE CONLIKE (*) #-}
  (FloatValue a) * (FloatValue b) = FloatValue (a * b)
  (IntValue a) * (IntValue b) = IntValue (a * b)
  (RationalValue a) * (RationalValue b) = RationalValue (a * b)
  a * b = RationalValue ((asRational a) * (asRational b))

  {-# INLINE CONLIKE fromInteger #-}
  fromInteger = IntValue

  {-# INLINE CONLIKE signum #-}
  signum (FloatValue a) = FloatValue (signum a)
  signum (IntValue a) = IntValue (signum a)
  signum (RationalValue a) = RationalValue (signum a)

  {-# INLINE CONLIKE abs #-}
  abs (FloatValue a) = FloatValue (abs a)
  abs (IntValue a) = IntValue (abs a)
  abs (RationalValue a) = RationalValue (abs a)

instance Fractional ConstantValue where
  {-# INLINE CONLIKE fromRational #-}
  fromRational = RationalValue

  {-# INLINE CONLIKE (/) #-}
  (FloatValue a) / (FloatValue b) = FloatValue (a / b)
  a / b = RationalValue ((asRational a)  / (asRational b))

instance Real ConstantValue where
  {-# INLINE CONLIKE toRational #-}
  toRational = asRational
instance RealFrac ConstantValue where
  {-# INLINE CONLIKE properFraction #-}
  properFraction a = RationalValue <$> properFraction (asRational a)

{-# INLINE CONLIKE doFloatOp #-}
doFloatOp :: (Double -> Double) -> ConstantValue -> ConstantValue
doFloatOp f (FloatValue a) = FloatValue (f a)
doFloatOp f a = FloatValue . f . fromRational . asRational $ a

instance Floating ConstantValue where
  {-# INLINE CONLIKE pi #-}
  pi = FloatValue pi
  
  {-# INLINE CONLIKE exp #-}
  exp = doFloatOp exp

  {-# INLINE CONLIKE log #-}
  log = doFloatOp GHC.Float.log

  {-# INLINE CONLIKE sin #-}
  sin = doFloatOp sin

  {-# INLINE CONLIKE cos #-}
  cos = doFloatOp cos
  {-# INLINE CONLIKE tan #-}
  tan = doFloatOp tan

  {-# INLINE CONLIKE sqrt #-}
  sqrt = doFloatOp sqrt  
  
  {-# INLINE CONLIKE asin #-}
  asin = doFloatOp asin

  {-# INLINE CONLIKE acos #-}
  acos = doFloatOp acos

  {-# INLINE CONLIKE atan #-}
  atan = doFloatOp atan

  {-# INLINE CONLIKE sinh #-}
  sinh = doFloatOp sinh
  {-# INLINE CONLIKE cosh #-}
  cosh = doFloatOp cosh
  {-# INLINE CONLIKE tanh #-}
  tanh = doFloatOp tanh
  
  {-# INLINE CONLIKE asinh #-}
  asinh = doFloatOp asinh

  {-# INLINE CONLIKE acosh #-}
  acosh = doFloatOp acosh

  {-# INLINE CONLIKE atanh #-}
  atanh = doFloatOp atanh

  {-# INLINE CONLIKE log1p #-}
  log1p = doFloatOp log1p
  {-# INLINE CONLIKE expm1 #-}
  expm1 = doFloatOp expm1
  {-# INLINE CONLIKE log1pexp #-}
  log1pexp = doFloatOp log1pexp
  {-# INLINE CONLIKE log1mexp #-}
  log1mexp = doFloatOp log1mexp


    
  

parseConstantValue :: Parser r ConstantValue
parseConstantValue =
  label "Constant value" $
    ( try $
        parseMaybeParens
          ( label "Rational number" do
              numerator <- lexeme (parseMaybeParens (signed sc decimal)) <?> "numerator"
              verbatim "%" <?> "obelus"
              denominator <- lexeme (parseMaybeParens (signed sc decimal)) <?> "denominator"
              pure $ RationalValue (numerator % denominator)
          )
    )
      <|> (try $ parseMaybeParens (FloatValue <$> (signed sc float <?> "Floating point number")))
      <|> (parseMaybeParens ((IntValue <$> (signed sc decimal <?> "Integer"))))

instance Pretty ConstantValue where
  pretty (IntValue i) = pretty i
  pretty (RationalValue r) = viaShow r
  pretty (FloatValue f) = pretty f

data Value = Symbolic {_symbolicValue :: SymbolicValue} | Constant {_constantValue :: ConstantValue} deriving (Eq, Ord, Generic, Hashable, Renderable, Lift)
makeLenses ''Value
makeClassyPrisms ''Value

instance AsObjectIdentifier Value where
    {-# INLINE _ObjectIdentifier #-}
    _ObjectIdentifier = _SymbolicValue . _ObjectIdentifier
                 

           
operatorSymbol :: SymbolName
operatorSymbol = textToSymbolName "operator"
operatorValue :: Value
operatorValue = let o = operatorSymbol ^.re (_Symbolic . _Symbol) in o
parseValue :: Members '[ObjectIdentityMap x] r => WhenParsing -> Parser r Value
parseValue w = (try (Constant <$> parseConstantValue <?> "Constant value")) <|> (Symbolic <$> parseSymbolicValue w <?> "Symbolic value")

instance IsString Value where
  {-# INLINE fromString #-}
  fromString = Symbolic . fromString



instance Show Value where
  show = show . render


instance AsIdentifier SymbolicValue where
  {-# INLINE _Identifier #-}
  _Identifier = _SymbolicIdentity

instance AsSymbolicValue Value where
  {-# INLINE _SymbolicValue #-}
  _SymbolicValue = _Symbolic

instance AsConstantValue Value where
  {-# INLINE _ConstantValue #-}
  _ConstantValue = _Constant

instance AsIdentifier Value where
  {-# INLINE _Identifier #-}
  _Identifier = _Symbolic . _Identifier
