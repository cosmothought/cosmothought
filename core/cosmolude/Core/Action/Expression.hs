module Core.Action.Expression where

import Core.Variable
import GHC.Generics (Generic)
import Prettyprinter    
import Data.Hashable
import Core.Utils.Rendering.CoreDoc
import Control.Lens
import Core.CoreTypes
import Prelude 



data ArithExpr a where
  ArithVar :: a -> ArithExpr a
  Num :: ConstantValue -> ArithExpr a
  (:+:) :: ArithExpr a -> ArithExpr a -> ArithExpr a
  (:*:) :: ArithExpr a -> ArithExpr a -> ArithExpr a
  (:-:) :: ArithExpr a -> ArithExpr a -> ArithExpr a
  (:/:) :: ArithExpr a -> ArithExpr a -> ArithExpr a
  Neg :: ArithExpr a -> ArithExpr a
  Sin :: ArithExpr a -> ArithExpr a
  Cos :: ArithExpr a -> ArithExpr a
  Tan :: ArithExpr a -> ArithExpr a
  Asin :: ArithExpr a -> ArithExpr a
  Acos :: ArithExpr a -> ArithExpr a
  Atan :: ArithExpr a -> ArithExpr a
  Sinh :: ArithExpr a -> ArithExpr a
  Cosh :: ArithExpr a -> ArithExpr a
  Tanh :: ArithExpr a -> ArithExpr a
  Asinh :: ArithExpr a -> ArithExpr a
  Acosh :: ArithExpr a -> ArithExpr a
  Atanh :: ArithExpr a -> ArithExpr a
  Abs :: ArithExpr a -> ArithExpr a
  Sign :: ArithExpr a -> ArithExpr a
  Exp :: ArithExpr a -> ArithExpr a
  Log :: ArithExpr a -> ArithExpr a
  
 deriving (Eq, Show, Generic, Hashable, Functor, Foldable, Traversable, Renderable)

infixl 6 :+:,  :-:
infixl 7 :*:, :/:

instance HasVariables a => HasVariables (ArithExpr a) where
  {-# INLINE variables #-}
  variables = traversed . variables
  
  
instance Plated (ArithExpr a) where
  {-# INLINEABLE plate #-}
  plate f (a :+: b) = ((:+:) <$> (f a)) <*> (f b)
  plate f (a :-: b) = ((:-:) <$> (f a)) <*> (f b)
  plate f (a :*: b) = ((:*:) <$> (f a)) <*> (f b)
  plate f (a :/: b) = ((:/:) <$> (f a)) <*> (f b)
  plate f (Neg a) = Neg <$> f a
  plate f (Sin a) = Sin <$> f a
  plate f (Cos a) = Cos <$> f a
  plate f (Tan a) = Tan <$> f a
  plate f (Asin a) = Asin <$> f a
  plate f (Acos a) = Acos <$> f a
  plate f (Atan a) = Atan <$> f a
  plate f (Sinh a) = Sinh <$> f a
  plate f (Cosh a) = Cosh <$> f a
  plate f (Tanh a) = Tanh <$> f a
  plate f (Asinh a) = Asinh <$> f a
  plate f (Acosh a) = Acosh <$> f a
  plate f (Atanh a) = Atanh <$> f a 
  plate f (Abs a) = Abs <$> f a
  plate f (Sign a) = Sign <$> f a
  plate f (Exp a) = Exp <$> f a
  plate f (Log a) = Log <$> f a
  plate _ a@(Num _)  = pure a
  plate _ a@(ArithVar _) = pure a




data SymExpr a where
  SymVar :: a -> SymExpr a
  SymText :: SymbolName -> SymExpr a
  (:<>:) :: SymExpr a -> SymExpr a -> SymExpr a
  deriving (Eq, Show, Generic, Hashable, Functor, Foldable, Traversable, Renderable)

instance Plated (SymExpr a) where
  {-# INLINE plate #-}
  plate _ a@(SymVar _) = pure a
  plate _ a@(SymText _) = pure a
  plate f (a :<>: b) = (:<>:) <$> f a <*> f b

data RuleExpr a where
  SymbolicExpression :: SymExpr a -> RuleExpr a
  ArithmeticExpression :: ArithExpr a -> RuleExpr a
  deriving (Eq, Show, Generic, Hashable, Functor, Foldable, Traversable, Renderable)
  deriving Pretty via ByRenderable (RuleExpr a)




