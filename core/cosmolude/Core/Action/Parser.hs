module Core.Action.Parser where

import Core.CoreTypes
-- OsPath
import Core.Action
import Core.Patterns.PatternSyntax
import Cosmolude
import Data.Either.Combinators
import Data.Text
import qualified Df1
import System.FilePath
import System.IO
import Text.Megaparsec.Char.Lexer (decimal)
import Core.Patterns.Parser


parseMessageDestination :: Parser r MessageDestination
parseMessageDestination =
  try ((File "/dev/stdout" (Just stdout)) <$ keyword "stdout")
    <|> try ((File "/dev/stderr" (Just stderr)) <$ keyword "stderr")
    <|> do
      path <- parseFilePath
      pure $ File (path) Nothing -- encodeUtf path
      



parseFilePath :: Parser r FilePath
parseFilePath = fmap unpack (lexeme stringLiteral) <?> "file path"

parseProcessArgument :: Parser r ProcessArgument
parseProcessArgument = fmap unpack (lexeme stringLiteral) <?> "process argument"



parseExecution :: Parser r WhenToExecute
parseExecution =
  keyword "on"
    *> ( try (OnActivation <$ keyword "activation")
           <|> (OnRetraction <$ keyword "retraction")
       )

    


parseRuleAction :: Members '[ObjectIdentityMap (WME' metadata)] r => Df1.Level -> Parser r RuleAction
parseRuleAction defaultLevel =
  try
    ( do
        pat <- lexeme (parseWMEPattern (ParsingRule (ParsingRuleRHS Nothing)))
        case pat ^. patternPreference  of
          (Just Acceptable) -> pure (NewWME pat)
          Nothing -> pure (NewWME pat)
          (Just Reject) -> pure (Core.Action.RemoveWME pat)
          _ | isOperatorPattern pat -> pure (NewWME pat)
          _ -> customFailure (ParsingError (ParsingRule (ParsingRuleRHS Nothing)) Nothing (NotACreationPreference ^. re (_RuleParsingError . _InvalidAction)))
    )
    <|> try
      ( do
          keyword "set"
          keyword "fuel"
          keyword "to"
          (SetQuiescenceFuelForCurrentState . rightToMaybe) <$> eitherP (Nothing <$ keyword "unlimited") (decimal)
      )
    <|> try
      ( do
          keyword "log"
          lvl <- optional $ do
            optional (keyword "at")
            keyword "level"
            parseDf1Level
          let level = fromMaybe defaultLevel lvl
          constructor <- Core.Action.Log <$> parseExecution <*> pure level
          msg <- stringLiteral <?> "Log message"
          _ <- optional (lexeme (keyword ","))
          pure $ constructor (layoutSmart defaultLayoutOptions (render msg))
      )
    <|> try
      ( do
          keyword "print"
          f <- optional $ do
            _ <- keyword "to"
            parseMessageDestination
          let file = fromMaybe stdOutDestination f
          constructor <- Output <$> parseExecution <*> pure file
          msg <- stringLiteral <?> "Output message"
          _ <- optional (lexeme (keyword ","))
          pure $ constructor (layoutSmart defaultLayoutOptions (render msg))
      )
    <|> try
      ( do
          _ <- keyword "success"
          Core.Action.TestSuccess <$> parseExecution          
      )
    <|> try
      ( do
          keyword "failure"
          constructor <- Core.Action.TestFailure <$> parseExecution
          msg <- stringLiteral <?> "Failure message"
          _ <- optional (lexeme (keyword ","))
          pure $ constructor (layoutSmart defaultLayoutOptions (render msg))
      )
    <|> ( do
            keyword "exec"
            constructor <- RunProcess <$> parseExecution
            path <- parseFilePath <?> "executable"
            args <- many parseProcessArgument
            _ <- optional (lexeme (keyword ","))
            pure $ constructor path args
        )
    
