{-# LANGUAGE PatternSynonyms #-}
module Core.Rule.Match (RuleMatch'(RuleMatch), activation, retraction, matchingToken, wmes, matchingSubstitution, instantiationID, newObjects, RuleActivation'(..), RuleRetraction'(..), RuleResult'(..)) where
import Core.Rule
import Cosmolude hiding (Last (..))


import Core.CoreTypes
--import Data.Semigroup (Last (..))




data RuleMatch' wmeMetadata matchProof = RuleMatch {_rule :: !Rule, _matchingToken :: !matchProof ,
                            _wmes :: !(Seq (WME' wmeMetadata)), _matchingSubstitution :: (Substitution Value), _newObjects :: Substitution ObjectIdentifier,
                            _instantiationID :: InstantiationIdentifier} deriving (Show,Generic)




instance (Renderable metadata, Renderable matchProof) =>  Renderable (RuleMatch' metadata matchProof) where
  render (RuleMatch rule _token wmes' sub objs ident) = vsep ["Matched rule:" <+> render rule,
                                                        --"Matching token:" <+> pretty token,
                                                        "New objects:" <+> render objs,
                                                        "Matched WMEs:" <+> render (wmes' ^.. folded), "Substitution:"
                                                         <+> render sub,
                                                        "Instantiation ID:" <+> render ident]


instance HasVariables (RuleMatch' metadata proof) where
  {-# INLINE variables #-}
  variables f (RuleMatch r t w s o i) = RuleMatch <$> (variables f r) <*> pure t <*>
    pure w <*> (traverseSubstitutionKeysWith const) f s <*> (traverseSubstitutionKeysWith const) f o <*> pure i

instance HasIdentifier (RuleMatch' metadata proof) where
  {-# INLINE identifier #-}
  identifier f (RuleMatch rule token wmes sub objs ident) = RuleMatch rule token wmes sub objs <$> identifier f ident

deriving via ByIdentifier (RuleMatch' metadata proof) instance Eq (RuleMatch' metadata proof)

deriving via ByIdentifier (RuleMatch' metadata proof) instance Ord (RuleMatch' metadata proof)

deriving via ByIdentifier (RuleMatch' metadata proof) instance Hashable (RuleMatch' metadata proof)

makeLenses ''RuleMatch'
instance HasSubstitution (RuleMatch' metadata proof) Value where
    {-# INLINE substitution #-}
    substitution = matchingSubstitution
instance HasRule (RuleMatch' metadata proof) where
    {-# INLINE rule #-}
    rule = Core.Rule.Match.rule
data RuleActivation' metadata proof = RuleActivation !(RuleMatch' metadata proof) deriving (Eq, Generic, Hashable)
deriving via ByRenderable (RuleActivation' metadata proof) instance (Renderable metadata, Renderable proof) => Pretty (RuleActivation' metadata proof)

instance (Renderable metadata, Renderable proof) => Renderable (RuleActivation' metadata proof) where
  render (RuleActivation match) = render match

instance (Renderable metadata, Renderable proof) => Show (RuleActivation' metadata proof) where
  show = show . render

data RuleRetraction' metadata proof = RuleRetraction !(RuleMatch' metadata proof) deriving (Eq, Generic, Hashable)
deriving via ByRenderable (RuleRetraction' metadata proof) instance (Renderable metadata, Renderable proof) => Pretty (RuleRetraction' metadata proof)

instance (Renderable metadata, Renderable proof) => Renderable (RuleRetraction' metadata proof) where
  render (RuleRetraction match) = render match

instance (Renderable metadata, Renderable proof) => Show (RuleRetraction' metadata proof) where
  show = show . render


data RuleResult' metadata proof = RuleWasActivated {_activation :: !(RuleActivation' metadata proof)}
                  | RuleWasRetracted {_retraction :: !(RuleRetraction' metadata proof)}
                  deriving (Eq,  Generic, Hashable)
                           

instance (Renderable metadata, Renderable proof) => Renderable (RuleResult' metadata proof) where
    render (RuleWasActivated a) = namedBracket "Activation" (render a)
    render (RuleWasRetracted a) = namedBracket "Retraction" (render a)                                   
                           
instance (Renderable metadata, Renderable proof) => Show (RuleResult' metadata proof) where
  show = show . render






makeLenses ''RuleResult'


