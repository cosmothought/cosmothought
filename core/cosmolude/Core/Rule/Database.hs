{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module Core.Rule.Database (HasRuleDB(..), RuleDatabase, addRule, lookUpRule, deleteRule, getAllRulesInDatabase, RuleNotInRuleDatabase(..), resetRuleDatabase, RuleDB(..), newRuleDatabase, runRuleDatabase, runFakeRuleDatabase, FakeRuleLookup(..)) where

import Core.Rule
import Polysemy
import Cosmolude
import qualified StmContainers.Map as STMM
import qualified System.IO.Unsafe as UNSAFE (unsafePerformIO)
import Control.Exception (Exception)
import Polysemy.Error
import GHC.Stack

-- TODO use a justified container for this.
data RuleDatabase m a where
  AddRule :: Rule -> RuleDatabase m ()
  LookUpRule :: HasCallStack =>  RuleIdentifier -> RuleDatabase m Rule
  DeleteRule :: RuleIdentifier -> RuleDatabase m ()
  GetAllRulesInDatabase :: RuleDatabase m [Rule]

makeSem_ ''RuleDatabase
lookUpRule :: (HasCallStack, Member RuleDatabase r) => RuleIdentifier -> Sem r Rule
addRule :: (HasCallStack, Member RuleDatabase r) => Rule -> Sem r ()
deleteRule :: (HasCallStack, Member RuleDatabase r) => RuleIdentifier -> Sem r ()
getAllRulesInDatabase :: (HasCallStack, Member RuleDatabase r) => Sem r [Rule]

data RuleNotInRuleDatabase = RuleNotInRuleDatabase RuleIdentifier CallStack [(RuleIdentifier,Rule)] deriving (Exception)
                           
instance Show RuleNotInRuleDatabase where
    show (RuleNotInRuleDatabase ident s db) = "Rule not in rule database exception:\n  " ++ show ident ++ "\n\n" ++ show db ++ "\n\n" ++ prettyCallStack s

data RuleDB = RuleDB (STMM.Map RuleIdentifier Rule)
class HasRuleDB ty where
    ruleDB :: Lens' ty RuleDB
instance HasRuleDB RuleDB where
    {-# INLINE ruleDB #-}
    ruleDB = id
instance Show RuleDB where
  show (RuleDB db) = show (UNSAFE.unsafePerformIO . doTransaction $ db^!!elemsM)

{-# INLINE CONLIKE newRuleDatabase #-}
newRuleDatabase :: STM RuleDB
newRuleDatabase = RuleDB <$> STMM.new

{-# INLINE resetRuleDatabase #-}
resetRuleDatabase :: (HasRuleDB ruledb, HasCallStack, Member Transactional r) => ruledb -> Sem r ()
resetRuleDatabase ruledb | (RuleDB db) <- ruledb^.ruleDB = resetMap db
{-# INLINE runRuleDatabase #-}
runRuleDatabase :: (HasRuleDB ruledb, HasCallStack, Members '[Error RuleNotInRuleDatabase, Transactional ] r) => ruledb -> InterpreterFor RuleDatabase r
runRuleDatabase ruledb | (RuleDB db) <- ruledb^.ruleDB = interpret \case
  AddRule rule -> db ^! insertingKVM (rule ^. ruleIdentifier) rule
  LookUpRule ident -> do
    res <- lookupMap ident db
    case res of
      Just r -> pure r
      Nothing -> do
               contents <- db^!!elemsM
               throw (RuleNotInRuleDatabase ident callStack contents)
  DeleteRule ident -> deleteFromMap ident db
  GetAllRulesInDatabase ->  db^!!elemsM . folded

data FakeRuleLookup = FakeRuleLookup deriving (Eq, Show, Exception)                            
runFakeRuleDatabase :: (Members '[Error FakeRuleLookup] r) => InterpreterFor RuleDatabase r
runFakeRuleDatabase = interpret \_ -> throw FakeRuleLookup

                            
