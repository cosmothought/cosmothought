{-# LANGUAGE GeneralisedNewtypeDeriving #-}

module PSCM.States where

import Core.CoreTypes
import Core.Rule.Match (RuleActivation', RuleRetraction')
-- import Data.Bits
import Cosmolude
import qualified Data.HashMap.Strict as HM
import Data.HashPSQ as PSQ
import qualified Data.Text as T
import Data.Tuple (swap)
import Model.PSCM.BasicTypes
import Model.PSCM.Impasses
import Numeric.Natural
import PSCM.Operators.OperatorProposals
import PSCM.Effects.WorkingMemory    
import Polysemy.Conc.Effect.Mask
import Polysemy.Conc.Sync
import Polysemy.Reader
-- import Polysemy.Resource
import qualified StmContainers.Map as STMM
import qualified StmContainers.Multimap as STMMM
import qualified StmContainers.Set as STMS
import Prelude hiding ((.))

newtype StatePriority = Priority Natural
  deriving stock (Eq, Generic, Show)
  deriving newtype (Ord, Num, Enum, Real, Integral)
  deriving anyclass (Hashable)

maximumPriority :: StatePriority
maximumPriority = Priority 0

type Impasse metadata = Impasse' (OperatorProposals metadata)  -- (HS.HashSet ObjectIdentifier)


    
data ImpasseResolutionData metadata = ImpasseResolutionData {_learnings :: [Rule], _additionsToWorkingMemory :: HashMap (WME' metadata) WMETruth, _removalsFromWorkingMemory :: HashMap (WME' metadata) WMETruth}
  deriving stock (Eq, Generic)
  deriving anyclass (Hashable)

makeLenses ''ImpasseResolutionData

type ImpasseResolution metadata = ImpasseResolution' (OperatorProposals metadata) (ChosenOperator metadata) (ImpasseResolutionData metadata)
makeImpasseResolutionData :: [Rule] -> HashMap (WME' metadata) WMETruth -> HashMap (WME' metadata) WMETruth -> ImpasseResolutionData metadata
makeImpasseResolutionData = ImpasseResolutionData
type StateDepth = Natural

data GoalDependencySet  = GoalDependencySet {_supporting :: STMS.Set WMEIdentifier} deriving (Generic)

makeLenses ''GoalDependencySet

newGoalDependencySet :: STM GoalDependencySet 
newGoalDependencySet = GoalDependencySet <$> STMS.new

-- TODO OPTIMISE                       
createGDSFromImpasse :: (Members '[Transactional] r) => Impasse metadata -> Sem r GoalDependencySet
createGDSFromImpasse reason =
  do
    supportingWMEs <- newSet
    reason ^!! operators . allProposals . folded . relevantWME . _Just . wMEIdentifier . act (\a -> insertInSet a supportingWMEs) 
    pure $ GoalDependencySet supportingWMEs
    

data StateOrdering = StateOrdering StateDepth StatePriority deriving (Eq, Ord, Generic, Show, Hashable)

instance ToPrimitiveAttribute StateOrdering where
  {-# INLINE toPrimitiveAttribute #-}
  toPrimitiveAttribute = toPrimitiveAttribute . T.pack . show

instance ToAttribute StateOrdering

-- instance Semigroup StateOrdering where

type OrderedActivity a = HashPSQ StateIdentifier StateOrdering (HashMap a WMETruth)

{-# INLINEABLE settingPriority #-}
settingPriority :: (Member Transactional r) => StateIdentifier -> StatePriority -> Action (Sem r) (TMVar (OrderedActivity a)) ()
settingPriority key priority =
  act
    ( \psq ->
        embedSTM $
          modifyTMVar
            psq
            ( \psq' ->
                snd $
                  alter
                    ( \case
                        Nothing -> ((), Nothing)
                        Just ((StateOrdering depth _), existing) -> ((), Just ((StateOrdering depth priority), existing))
                    )
                    key
                    psq'
            )
    )

{-# INLINEABLE setPriority #-}
setPriority :: (Members '[ScopedSync (OrderedActivity a), Mask, Resource] r) => StateIdentifier -> StatePriority -> Sem r ()
setPriority key priority =
  withSync $
    modify_
      ( \psq' ->
          pure $
            snd $
              alter
                ( \case
                    Nothing -> ((), Nothing)
                    Just ((StateOrdering depth _), existing) -> ((), Just ((StateOrdering depth priority), existing))
                )
                key
                psq'
      )

{-# INLINEABLE insertingAt #-}
insertingAt :: (Eq a, Hashable a, Member Transactional r) => StateIdentifier -> StateOrdering -> a -> Action (Sem r) (TMVar (OrderedActivity a)) ()
insertingAt key priority value =
  act
    ( \psq ->
        embedSTM $
          modifyTMVar
            psq
            ( \psq' ->
                snd $
                  alter
                    ( \case
                        Nothing -> ((), Just (priority, HM.singleton value top))
                        Just (p, existing) -> ((), Just (p, HM.insertWith (/\) value top existing))
                    )
                    key
                    psq'
            )
    )

{-# INLINEABLE insertingAtWith #-}
insertingAtWith :: (Eq a, Hashable a, Member Transactional r) => WMETruth -> (WMETruth -> WMETruth -> WMETruth) -> StateIdentifier -> StateOrdering -> a -> Action (Sem r) (TMVar (OrderedActivity a)) ()
insertingAtWith support mergeFunc key priority value =
  act
    ( \psq -> do
        --        truthVal <- readT (value^.truthValue)
        embedSTM $
          modifyTMVar
            psq
            ( \psq' ->
                snd $
                  alter
                    ( \case
                        Nothing -> ((), Just (priority, HM.singleton value support))
                        Just (p, existing) -> ((), Just (p, HM.insertWith mergeFunc value support existing))
                    )
                    key
                    psq'
            )
    )

-- {-# INLINE insertAtScoped #-}
-- insertAtScoped :: (Eq a, Hashable a,Members '[ScopedSync (OrderedActivity a), Mask, Resource] r ) => StateIdentifier -> StateOrdering -> a -> Sem r ()
-- insertAtScoped key priority value =
--   withSync $
--         modify_
--           ( \psq' ->

--                 pure $ snd $ alter
--                   ( \case
--                       Nothing -> ((), Just (priority, HS.singleton value))
--                       Just (p, existing) -> ((), Just (p, HS.insert value existing))
--                   )
--                   key
--                   psq'
--           )

type instance Index (OrderedActivity a) = StateIdentifier

type instance IxValue (OrderedActivity a) = (StateOrdering, HashMap a WMETruth)

instance Ixed (OrderedActivity a) where
  {-# INLINEABLE ix #-}
  ix key f psq = case PSQ.lookup key psq of
    Nothing -> pure psq
    Just res -> f res <&> \(prio, a) -> PSQ.insert key prio a psq

instance At (OrderedActivity a) where
  at key f psq = do
    let res = PSQ.lookup key psq
    f res <&> \case
      Nothing -> PSQ.delete key psq
      Just (prio, a) -> PSQ.insert key prio a psq

-- {-# INLINE insertAt #-}
-- insertAt :: (Eq a, Hashable a ) => StateIdentifier ->  -> Setter' (OrderedActivity a) (StateOrdering,a)
-- insertAt key  =
--   setting
--           ( \f psq' ->

--                 snd $ alter
--                   ( \case
--                       Nothing -> ((), Just (priority, HS.singleton value))
--                       Just (p, existing) -> ((), Just (p, HS.insert value existing))
--                   )
--                   key
--                   psq'
--           )
{-# INLINEABLE gettingMinAndReset #-}
gettingMinAndReset :: (Eq a, Hashable a) => StateIdentifier -> Action STM (TMVar (OrderedActivity a)) (StateIdentifier, HashMap a WMETruth)
gettingMinAndReset topLevelState =
  act
    ( \psq ->
        stateTMVar
          psq
          ( \psq' ->
              alterMin
                ( \case
                    Nothing -> ((topLevelState, HM.empty), Nothing)
                    Just (state, _depth, existing) -> ((state, existing), Nothing)
                )
                psq'
          )
    )

{-# INLINEABLE getMinAndReset #-}
getMinAndReset :: (Eq a, Hashable a, Members '[ScopedSync (OrderedActivity a), Mask, Resource] r) => StateIdentifier -> Sem r (StateIdentifier, HashMap a WMETruth)
getMinAndReset topLevelState =
  withSync $
    modify
      ( \psq' ->
          pure . swap $
            alterMin
              ( \case
                  Nothing -> ((topLevelState, HM.empty), Nothing)
                  Just (state, _depth, existing) -> ((state, existing), Nothing)
              )
              psq'
      )

data StateData metadata = StateData
  { _stateIdentifier :: StateIdentifier,
    _priority :: TVar StatePriority,
    _impasseReason :: Maybe (Impasse metadata),
    _currentOperatorWME :: TVar (Maybe (WME' metadata)),
    _quiescenceFuelLimit :: TVar (Maybe QuiescenceFuel), -- ,
    _goalDependencySet :: GoalDependencySet
    --    _referencedObjects :: STMS.Set ObjectIdentifier
  }
  deriving (Generic)

makeLenses ''StateData

instance Hashable (StateData metadata) where
  {-# INLINE hashWithSalt #-}
  hashWithSalt salt (StateData ident _prio _imp _op _fuel _gds) = hashWithSalt salt ident

-- TODO make StateData renderable
instance HasIdentifier (StateData metadata) where
  {-# INLINE identifier #-}
  identifier = stateIdentifier . identifier

deriving via ByIdentifier (StateData metadata) instance Eq (StateData metadata)

topLevelState :: STM (StateData metadata)
topLevelState = StateData <$> pure topLevelStateIdentifier <*> newTVar (Priority 0) <*> pure Nothing <*> newTVar Nothing <*> newTVar Nothing <*> (newGoalDependencySet)

data StateMap metadata = StateMap
  { _statesByObject :: STMM.Map ObjectIdentifier StateIdentifier,
    _stateChildren :: STMMM.Multimap StateIdentifier (StateData metadata),
    _orderingsByState :: STMM.Map StateIdentifier StateOrdering,
    _allStates :: STMM.Map StateIdentifier (StateData metadata)
  }

makeClassy ''StateMap

{-# INLINE blankStateMap #-}
blankStateMap :: STM (StateMap metadata)
blankStateMap = StateMap <$> STMM.new <*> STMMM.new <*> STMM.new <*> STMM.new

{-# INLINE resetStateMap #-}
resetStateMap :: (HasStateMap stateMap metadata, Member Transactional r) => stateMap -> Sem r ()
resetStateMap st | s@(StateMap a b c d) <- st ^. stateMap = do
  embedSTM $ do
    STMM.reset a
    STMMM.reset b
    STMM.reset c
    STMM.reset d
  s ^! statesByObject . insertingKVM (topLevelStateIdentifier ^. stateObject) topLevelStateIdentifier
  let ordering = StateOrdering 0 0
  s ^! orderingsByState . insertingKVM topLevelStateIdentifier ordering

{-# INLINE newStateMap #-}
newStateMap :: (Member Transactional r) => Sem r (StateMap metadata)
newStateMap = do
  s <- embedSTM blankStateMap
  resetStateMap s
  pure s
{-# INLINEABLE removeStateFromStateMap #-}       
removeStateFromStateMap ::(HasStateMap stmp metadata, Members '[Reader stmp, Transactional] r) => StateIdentifier -> Sem r ()
removeStateFromStateMap ident = do
  stateMap <- ask <&> view stateMap
  let stateOID = ident^.stateObject
  stateMap ^! statesByObject . deletingKVM stateOID
  stateMap ^! stateChildren . act (deleteFromMultimapByKey ident)
  stateMap ^! orderingsByState . deletingKVM ident
  stateMap ^! allStates . deletingKVM ident
           
{-# INLINEABLE indexStateAtDepth #-}
indexStateAtDepth :: (HasStateMap stmp metadata, Members '[Reader stmp, Transactional] r) => StateIdentifier -> StateData metadata -> StateDepth -> Sem r ()
indexStateAtDepth parent s depth = do
  stateMap <- ask <&> view stateMap
  stateMap ^! statesByObject . insertingKVM (s ^. stateIdentifier . stateObject) (s ^. stateIdentifier)
  stateMap ^! stateChildren . insertingKVMM parent s
  prio <- s ^! priority . act readT
  stateMap ^! orderingsByState . insertingKVM (s ^. stateIdentifier) (StateOrdering depth prio)
  stateMap ^! allStates . insertingKVM (s ^. stateIdentifier) s

--  fuel <- readT (s^.quiescenceFuel)
--  stateMap^!fuelRemaining . insertingKVM (s^.stateIdentifier) fuel



{-# INLINEABLE getStateFromSID #-}
getStateFromSID :: (HasTracer agent, HasStateMap agent metadata, Members '[Reader agent, Resource, Log, OpenTelemetryContext, OpenTelemetrySpan, Transactional] r) => StateIdentifier -> Sem r (Maybe (StateData metadata))
getStateFromSID i = do
  stateMap <- ask <&> view stateMap
  lookupMap i (stateMap ^. allStates)

{-# INLINE state #-}
state :: (HasTracer agent, HasStateMap agent metadata, Members '[Reader agent, Resource, Log, OpenTelemetryContext, OpenTelemetrySpan, Transactional] r) => MonadicFold (Sem r) StateIdentifier (StateData metadata)
state = (act getStateFromSID) . _Just

        
{-# INLINEABLE getStateFromOID #-}
getStateFromOID :: (HasTracer agent, HasStateMap agent metadata, Members '[Reader agent, Resource, Log, OpenTelemetryContext, OpenTelemetrySpan, Transactional] r) => ObjectIdentifier -> Sem r (Maybe (StateData metadata))
getStateFromOID oid = push "getStateFromOID" do
  statemap <- ask <&> view stateMap
  ident <- lookupMap oid (statemap ^. statesByObject)
  case ident of
    Nothing -> do
      Cosmolude.error "Unknown state identifier" (makeAttributesWithRaw "pscm.state.identifier" oid)
      pure Nothing
    Just i -> do
      i ^!? state

{-# INLINEABLE makeStateBaseWMEMetadata #-}
makeStateBaseWMEMetadata :: (CreateBlankWMEMetadata metadata, HasCreatingState metadata, HasWMESupport metadata, Members '[Transactional] r) => StateIdentifier -> Sem r metadata
makeStateBaseWMEMetadata stateIdent = do
  metadata <- embedSTM createBlankWMEMetadata
  metadata ^! wmeSupport . act (writeT Architectural)
  pure (metadata & creatingState .~ stateIdent)       

       
makeStateAugmentation :: (CreateBlankWMEMetadata metadata, HasCreatingState metadata, HasWMESupport metadata, Members '[WorkingMemory' metadata, Transactional] r) => StateIdentifier -> SymbolicValue -> Value -> Sem r (WME' metadata)
makeStateAugmentation stateIdent a v = do
  let stateObjectField = stateIdent ^. stateObject
  meta <- (makeStateBaseWMEMetadata stateIdent)
  ident <- contentAddressableIdentifier (WMEContentKey stateObjectField a v)
  pure (WME stateObjectField a v ident meta)

       
data PendingActivity metadata proof = PendingActivity
  { _pendingActivationsBySubstate :: TMVar (OrderedActivity (RuleActivation' metadata proof)),
    _pendingRetractionsBySubstate :: TMVar (OrderedActivity (RuleRetraction' metadata proof)),
    _fuelUsed :: STMM.Map StateIdentifier (QuiescenceFuel),
    _pendingWMEInsertionsBySubstate :: TMVar (OrderedActivity (WME' metadata)),
    _pendingWMEDeletionsBySubstate :: TMVar (OrderedActivity (WME' metadata))
  }

makeClassy ''PendingActivity

{-# INLINE newPendingActivity #-}
newPendingActivity :: STM (PendingActivity metadata proof)
newPendingActivity = PendingActivity <$> newTMVar PSQ.empty <*> newTMVar PSQ.empty <*> STMM.new <*> newTMVar PSQ.empty <*> newTMVar PSQ.empty

resetPendingActivity :: (PendingActivity metadata proof) -> STM ()
resetPendingActivity (PendingActivity a r f i d) = do
  putTMVar a PSQ.empty
  putTMVar r PSQ.empty
  STMM.reset f
  putTMVar i PSQ.empty
  putTMVar d PSQ.empty
           
removeStateFromPendingActivity :: PendingActivity metadata proof -> StateIdentifier -> STM ()
removeStateFromPendingActivity activity ident = do
  activity ^! pendingActivationsBySubstate . act (\tmv -> modifyTMVar tmv (PSQ.delete ident))
  activity ^! pendingRetractionsBySubstate . act (\tmv -> modifyTMVar tmv (PSQ.delete ident))
  activity ^! pendingWMEInsertionsBySubstate . act (\tmv -> modifyTMVar tmv (PSQ.delete ident))
  activity ^! pendingWMEDeletionsBySubstate . act (\tmv -> modifyTMVar tmv (PSQ.delete ident))
  STMM.delete ident (activity ^. fuelUsed)

