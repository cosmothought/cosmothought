module PSCM.Impasses where
import Polysemy.Reader (Reader)
-- import Core.WME

-- import Model.PSCM

-- import Data.Text
import PSCM.Operators.OperatorProposals
import Core.CoreTypes
import Core.WorkingMemory (WorkingMemory')
import Cosmolude
import qualified Data.HashSet as HS
import Data.HashSet.Lens
import GHC.Real
import Model.PSCM.Impasses
import PSCM.Effects.ProductionSystem as ProdSys
import PSCM.Effects.ProblemSubstates
import PSCM.States


-- import qualified Data.Text    as T
createSubstateFromImpasse :: (HasTracer agent, HasWMPreference (OperatorProposal metadata) ObjectIdentifier, CreateBlankWMEMetadata metadata, HasCreatingState metadata, HasWMESupport metadata, WMETruthValue (WME' metadata), Members '[ProductionSystem' metadata, IDGen, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, WorkingMemory' metadata, Resource, Log, ProblemSubstates metadata, Transactional] r) => Impasse metadata -> Sem r ()
createSubstateFromImpasse impasse = push "createSubstateFromImpasse" do
  addAttributes [("pscm.impasse.type", toAttribute (impasseType impasse))]
  newIdent <- newGlobalIdentifier
  let newStateOid = ObjectIdentifier (textToSymbolName $ fromString $ show $ "substate-" <> (render newIdent)) newIdent
  basicStateWMEs <- initiateSubstate impasse newStateOid maximumPriority
  newParentStateIdent <- currentState
  let makeAug = makeStateAugmentation (SubStateIdentifier newParentStateIdent newStateOid)
      impasseItems conflictSet = do
        itemWMEs <- do
          let oids = conflictSet ^..  allProposals . folded . proposedOperator . re (_Symbolic . _ObjectIdentity)
          forM oids (makeAug "item")
        itemCountWME <- makeAug "item-count" (Constant . fromInteger . toInteger $ lengthOf (allProposals.folded) conflictSet )
        pure $ (itemCountWME : itemWMEs)
      impasseNonNumericItems conflictSet = do
        let oids = conflictSet^.. allProposals . folded . filtered (hasn't (wMPreference . _Indifferent . _Just)) . proposedOperator . re (_Symbolic . _ObjectIdentity)           
        nonNumericPrefs <- HS.fromList <$> forM oids (makeAug "non-numeric")
        nonNumericItemCount <- makeAug "non-numeric-count" (Constant . fromInteger . toInteger $ (HS.size nonNumericPrefs))
        pure (nonNumericPrefs  <> HS.singleton nonNumericItemCount)

  impasseWME <- makeAug "impasse" ((impasseTypeSymbol impasse) ^. re (_Symbolic . _Symbol ))
  impasseSpecificWMEs <- case impasse of
    NoChange noChangeType -> push "noChangeImpasse" do
      choicesWME <- makeAug "choices" "none"
      attributeWME <-
        makeAug
          "attribute"
          ( case noChangeType of
              StateNoChangeImpasse _props -> "state"
              OperatorNoChangeImpasse _props -> operatorValue
          )
      quiescenceWME <- makeAug "quiescence" "t"
      pure $ HS.fromList [choicesWME, attributeWME, quiescenceWME]
    ConstraintFailure (ConstraintFailureImpasse (conflictSet :: OperatorProposals metadata)) -> push "constraintFailureImpasse" do
      attributeWME <- makeAug "attribute" operatorValue
      choicesWME <- makeAug "choices" "constraint-failure"
      itemWMEs <- impasseItems conflictSet
      pure . HS.fromList $  choicesWME : attributeWME : itemWMEs
    Tie (TieImpasse (conflictSet :: OperatorProposals metadata)) -> push "tieImpasse" do
      choicesWME <- makeAug "choices" "multiple"                                        
      itemWMEs <- impasseItems conflictSet
      attributeWME <- makeAug "attribute" operatorValue
      nonNumericWMEs <- impasseNonNumericItems conflictSet
      pure $ (HS.fromList $ choicesWME : attributeWME : itemWMEs) <> nonNumericWMEs
    Conflict (ConflictImpasse conflictSet) -> push "conflictImpasse" do
      choicesWME <- makeAug "choices" "multiple"                                        
      itemWMEs <- impasseItems conflictSet
      attributeWME <- makeAug "attribute" operatorValue
      pure $ (HS.fromList $ choicesWME : attributeWME : itemWMEs)
                                        
    
  let wmes = (setOf folded basicStateWMEs) <> impasseSpecificWMEs <> HS.singleton impasseWME
  ProdSys.addElements wmes
