module PSCM.ProductionSystem.Exceptions where
import Cosmolude
import Core.Rule.Match
import Control.Exception (Exception)    
data SubstateIdentificationExceptionType
  = MissingStateVariableInRule
  | MissingStateVariableInSubstitution
  | MissingMatchedObject
  | StateIdentityNotFound
  deriving (Eq, Ord)

instance Show SubstateIdentificationExceptionType where
  show MissingStateVariableInRule = "Missing state variable in rule"
  show MissingStateVariableInSubstitution = "Missing state variable in unification substitution"
  show MissingMatchedObject = "Missing matched object"
  show StateIdentityNotFound = "State identity not found"

data SubstateIdentificationException metadata proof = SubstateIdentificationException SubstateIdentificationExceptionType (RuleMatch' metadata proof) deriving (Show, Exception)    
