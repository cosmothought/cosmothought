* Features [0/7]
:PROPERTIES:
:ID:       25cd0ce8-4f2c-4106-b4d5-18c7f0277394
:END:
- [-] Infrastructure
  - [-] Unified identifier and message UX system
    - [X] Several identifier subtypes
	  - [X] UUIDs
	    Relatively cheap to generate and compare, while being effectively globally unique; useful for grounding semantics
	  - [X] URIs
    	Useful for special identifiers, long-term identifiers, and identifiers with semantic content
	  - [X] de Bruijn-style contextual identifiers
	    Quick to generate and compare; useful for, for example, tokens in the rete network, and WMEs that
        aren't expected to be long-term	
    - [-] Messages and UX text annotated with linked identifiers
  - [X] Pervasive use of software transactional memory
	This greatly decreases the chance of inconsistent views of memory during processing by presenting atomic
    bulk changes to the state; all while in the presence of mass concurrency
  - [-] Logging
	Logging in the `df1` format
	- [X] Log levels
	- [X] Log output filtering
	- [ ] Log to arbitrary handles
  - [ ] Server
	- [ ] Remote connections
	  - [ ] String-based REPL
	  - [ ] Local
	  - [ ] Remote
  - [ ] Sockets
  - [ ] HTTPS
- [-] UI/UX
  - [-] Scripting
    - [-] Low-level command language
	  - [X] Add and remove WMES
	  - [X] Add and remove rules
	  - [X] Print environmental data
	    - [X]  WMEs
		- [X] Rules
		- [X] Rete nodes
	  - [X] WME existence checking
	  - [X] Update configuration at runtime
	  - [X] Version information
	  - [ ] Run commands by linking to special objects
    - [-] Scripts
	  - [-] Loading script files
	    - [-] Parsing
		  - [X] Command parsing
		  - [-] Script parsing
		- [-] Execution
		  - [X] Command execution
		  - [ ] Script execution
		- [-] Dumping production systems
		- [-] Dumping WMEs
- [-] Production system
  - [X] Working memory
	- [X] WME triples
	  - [X] Value types
	    - [X] Symbolic values
	    - [X] Numeric values
	  - [X] WME support
	    - [X] Operator support
		- [X] Truth/belief strength
  - [-] Production rules
	- [-] Pattern matching and substitution
	  - [X] Unification of variables
	  - [ ] Compilation of a URI to a series of WMEPatterns
	  - [-] "Where" expressions for complex binding values
		- [-] Expression language
		  - [X] Basic arithmetic
		  - [-] Symbolic manipulation
			- [X] Symbol concatenation
			- [X] Fresh symbol generation
			- [ ] Regular expressions
    	- [ ] Adds extra variable bindings
		  - [ ] For matching
		  - [ ] For actions  		
	- [-] Actions
	  - [-] WME processing
	    - [X] Creation and removal
		- [-] WME preference setting
		- [ ] Deep copying of an object
		- [ ] WME statistics
		  - [ ] Number of WMEs present
		  - [ ] Number of references to a WME
		  - [ ] Number of references to an object
	  - [X] Message output and logging
	  - [X] Explicit separation between rule activation and retraction
		This enables, for example, different log messages to be emitted when a rule fires and when the rule retracts.
	  - [-] Configuration changes
		- [X] Quiescence fuel
		- [ ] Minimum activation strengths
	  - [X] Halting and interruption
	  - [ ] Asking for user input
	  - [ ] Reification of processing statistics
		- [ ] Processing time
		- [ ] Processing cycles
	  - [ ] Storage and retrieval of long-term memory
	  - [ ] Random number generation
	  - [-] External process execution
		- [X] One-shot
		- [ ] Daemon
	  - [ ] Script execution
	- [ ] Production rules reified to working memory
  - [-] Uses a rete network
	- [X] Rules are compiled to a rete network
	- [X] Rules and WMEs can be inserted and deleted in any order
	- [ ] Unlinking
	  - [ ] Left unlinking
	  - [ ] Right unlinking
	- [X] Node types
	  - [X] Alpha nodes
	  - [X] Join nodes
		- [X] K-ary joins
	  - [X] Memory nodes
	  - [X] Unary negations
	  - [X] Conjunctive negations
	  - [X] Production nodes
  - [-] Quiescence limit
	- [ ] Per-state limit
	- [X] Decrements per iteration
- [ ] High-level programming language
  - [ ] Compiles to the low-level command language
- [-] PSCM
  - [-] State-based
	- [X] Hierarchical
	  - [X] Sibling states
    	States form a tree, allowing parallel problem decomposition
	- [-] Prioritised
	  Implemented using a priority search queue.
	- [X] Impasses
	  Currently generated, but not entered into
  - [X] Operators
	- [X] Operator-vs-instantiation WME support
	  - [X] Distinction made
	  - [X] Detection
	  - [X] Rule retractions don't remove operator-supported WMEs
  - [ ] Chunking
- [ ] Long-term memory
  - [ ] Stored
	- [ ] PostgreSQL
	- [ ] Local XML files
  - [ ] Reinforcement learning
	- [ ] Activation strength
  - [ ] Semantic memory
  - [ ] Episodic memory
  - [ ] Emotional memory
- [ ] Emotional framework
  - [ ] Emotions
    - [ ] Curiosity
  - [ ] Based on OpenPSI?
