module PSCM.ProductionSystemTests where
import qualified OpenTelemetry.Trace.Core as OT
import Polysemy.Error
import Hedgehog.Internal.Property (forAllT)    
import Control.Monad.IO.Class    
import OpenTelemetry.Trace.Core hiding (inSpan)
import Polysemy.State.Keyed
import Core.Configuration
import Agent
import Core.RuleTests    
import qualified PSCM.Effects.RuleMatcher as Rete
import Core.Rule.Match
import Core.Rule.Construction
import Data.Foldable
import Core.Utils.Logging
import Polysemy.OpenTelemetry as POT
import Util
import Core.Utils.Rendering.CoreDoc
import PSCM.ProductionSystem
import PSCM.ProductionSystem.ActivityIdentification
import Test.Tasty.Hedgehog
import Core.Rule
import Control.Monad
import Data.List.NonEmpty    
import Test.Tasty    
import qualified Gen
import qualified Hedgehog.Gen as Gen

--import 
-- addAndQueryWMEs :: TestTree
-- addAndQueryWMEs = testProperty "Add and query for WMEs" $ property do
--   wmes <- forAll $ Gen.set (Range.linearFrom 1 10) (Gen.mkWMEAndCompatiblePattern Gen.wmeMetadata)
--   with
--   forAll Gen.objectIdentifier
--   others <- forAll acceptableAndLowers
--   let proposals = proposeOperator (required' oid) <> others
--   annotateShow proposals
--   (Right (ChosenOperator oid proposals topLevelStateIdentifier)) === (run (runInputConst topLevelStateIdentifier $ evaluateOperators proposals))    

productionSystemTests :: TestTree
productionSystemTests = testGroup "Production system" [substateIdentification, simpleProductionSystemTest]



generateWMEsAndRulesTwice :: (MonadIO m, MonadGen m) => m (([WME], NonEmpty Rule), ([WME], NonEmpty Rule))
generateWMEsAndRulesTwice = do
  (wmes1, rules1) <- generateWMEsAndRules Gen.metadataGen
  (wmes2, rules2) <- generateWMEsAndRules Gen.metadataGen
  when (nub (rules1 <> rules2) /= (rules1 <> rules2)) do
                         Gen.discard
  pure ((wmes1,rules1),(wmes2,rules2))

substateIdentification :: TestTree
substateIdentification = testProperty "Substate identification" $ withTests 1000 $ substateIdentificationProp
       
substateIdentificationProp :: Property
substateIdentificationProp =   withShrinks 10000 $ withRetries 3 $ property do
  (wmes, rules) <- forAllT (generateWMEsAndRules Gen.metadataGen)
  Util.annotate . show $ "WMES:" <+> render wmes
  Util.annotate . show $ "Rules:" <+> render rules
  evalIO $! {-#SCC substateIdentification #-} withFreshAgentIO  \agent ->  OT.inSpan (agent^.tracerL) "Test:PSCM:substateIdentification" OT.defaultSpanArguments do
    runPSCM' False agent do
      putAt MaximumIterationsForQuiescence (Just 1)
      addElements wmes
      addAttributeWithRaw "core.pscm.rule_addition.given_rules" (rules ^.. folded)
      _ <- runError  do -- figure out what to do with the error when I get more error types I guess?
               rules' <- traverse (runError . simplifyRule) rules
               case sequence rules' of
                 Left e -> do
                           POT.recordException e []
                           
                 Right rules'' -> do
                           addAttributeWithRaw "core.pscm.rule_addition.simplified_rules" rules''
                           (_, activations, _) <- Rete.addRules rules''
                           forM_ activations \activation@(RuleActivation match) -> do
                                (_ident, _depth) <- identifySubstateMatchAppliesTo match
                                pure ()
      pure ()
                        
simpleProductionSystemTest :: TestTree
simpleProductionSystemTest = testGroup "Simple production system commands" [addingJustWMEs, addingJustRules, addingJustRulesInMultipleBatches, addingWMEsThenRules, addingRulesThenWMEs]

addingJustRulesInMultipleBatches :: TestTree
addingJustRulesInMultipleBatches =  testProperty "Adding just rules in multiple batches" $ withShrinks 10000 $ withTests 1000 $ withRetries 3 $ property do
  ((_, rules1), (_,rules2)) <- forAllT generateWMEsAndRulesTwice

  Util.annotate . show $ "First set of rules:" <+> render rules1                 
  Util.annotate . show $ "Second set of rules:" <+> render rules2

  evalIO $ {-#SCC withFreshAgentJustRulesMultipleBatches #-} withFreshAgentIO \agent ->  OT.inSpan (agent^.tracerL) "Test:PSCM:addingJustRulesInMultipleBatches" OT.defaultSpanArguments do
    runPSCM' False agent do                                                                                                     
      putAt MaximumIterationsForQuiescence (Just 2)
      !_ <- addRules rules1
      addRules rules2
                             
addingJustRules :: TestTree
addingJustRules =  testProperty "Adding just rules"    $ withShrinks 10000 $ withTests 1000 $ withRetries 3 $ property do
  (_, rules) <- forAllT (generateWMEsAndRules Gen.metadataGen)
  Util.annotate . show $ "Rules:" <+> render rules
  evalIO $ {-#SCC withFreshAgentJustRules #-} withFreshAgentIO \agent ->  OT.inSpan (agent^.tracerL) "Test:PSCM:addingJustRules" OT.defaultSpanArguments do
    runPSCM' False agent do                                                                                                     
      putAt MaximumIterationsForQuiescence (Just 5)
      addRules rules
                             
addingJustWMEs :: TestTree
addingJustWMEs =  testProperty "Adding just WMEs" $  withShrinks 10000 $ withTests 200 $ withRetries 3 $ property do
  (wmes, _) <- forAllT (generateWMEsAndRules Gen.metadataGen)
  Util.annotate . show $ "WMES:" <+> render wmes
  evalIO $ {-#SCC withFreshAgentJustWMEs #-} withFreshAgentIO \agent -> OT.inSpan (agent^.tracerL) "Test:PSCM:addingJustWMEs" OT.defaultSpanArguments do
    runPSCM' False agent do
      putAt MaximumIterationsForQuiescence (Just 20)
      addElements wmes
  


addingWMEsThenRules :: TestTree
addingWMEsThenRules =  testProperty "Adding WMEs then rules" $ withShrinks 10000 $ withTests 1500 $ withRetries 3 $ property do
  (wmes, rules) <- forAllT (generateWMEsAndRules   Gen.metadataGen)
  Util.annotate . show $ "WMES:" <+> render wmes
  Util.annotate . show $ "Rules:" <+> render rules
  evalIO $! {-#SCC withFreshAgentWMEsThenRules #-} withFreshAgentIO  \agent ->  OT.inSpan (agent^.tracerL) "Test:PSCM:addingWMEsThenRules" OT.defaultSpanArguments do
    runPSCM' False agent do
      putAt MaximumIterationsForQuiescence (Just 10)
      !() <- addElements wmes  
      addRules rules


addingRulesThenWMEs :: TestTree
addingRulesThenWMEs =  testProperty "Adding rules then WMEs"    $ withShrinks 10000 $ withTests 1500 $ withRetries 3 $ property $  do
  (wmes, rules) <- forAllT (generateWMEsAndRules Gen.metadataGen)
  Util.annotate . show $ "WMES:" <+> render wmes
  Util.annotate . show $ "Rules:" <+> render rules
  evalIO $! {-#SCC withFreshAgentRulesThenWMEs #-} withFreshAgentIO \agent -> OT.inSpan (agent^.tracerL) "Test:PSCM:addingRulesThenWMEs" OT.defaultSpanArguments do
    runPSCM' False agent do
      putAt MaximumIterationsForQuiescence (Just 10)
      !_ <- addRules rules
      addElements wmes

                 
