module CosmoreScriptTest where

import Cosmolude
import ForTesting

makeParsingTests :: (HasCallStack, Eq info, Show info, Read info) => GlobPattern -> TestName -> ((info, object) -> IO [TestTree]) -> Parser (MinimalParserEffects Agent) (info, object) -> IO ([CUBE], TestTree)
makeParsingTests rn groupName extraTests parser = do
  succeedDir <- getDataFileName "test/tests/golden/succeed"
  failDir <- getDataFileName "test/tests/golden/fail"
  let succeedCube = succeedSugarCube [succeedDir]
      failCube = failSugarCube [failDir]
  succeedSweets <- findSugar succeedCube
  failSweets <- findSugar failCube
  succeedTests <- withSugarGroups succeedSweets testGroup (mkTest True)
  failTests <- withSugarGroups failSweets testGroup (mkTest False)
  pure $ ([succeedCube, failCube], testGroup groupName [testGroup "Golden tests" [testGroup "Succeed tests" succeedTests, testGroup "Fail tests" failTests]])
  where
    mkTest :: Bool -> Sweets -> Natural -> Expectation -> IO [TestTree]
    mkTest itShouldParse s n e = do
      -- let Just filename = lookup "inputs" $ associated e
      input <- readFile' $ rootFile s

      let params = HMS.fromList (expParamsMatch e)
          -- itShouldParse = True -- (traceShowId $ params ^. at "parses") == (Just NotSpecified)
          whenParsing = theParsing (rootMatchName s) (fromString input)
      if itShouldParse
        then do
          let shouldParseTo = shouldParseTo' (rootFile s) input
          exp <- fmap read $ readFile' $ expectedFile e
          extras <- case actualParse (rootMatchName s) (fromString input) of
            Right a -> extraTests a
            Left _ -> pure []
          pure
            ([testProperty "Should parse" (withTests 1 $ property $ 
                                             liftTest (whenParsing `shouldParseTo` exp)
                                             
                                          )] ++ extras)
        else do
          exp <- readFile' $ expectedFile e
          pure [testProperty "Should fail to parse" (withTests 1 $ property $ liftTest (whenParsing `shouldFailToParseWith` (rootMatchName s, input, exp)))]

    actualParse name input =  do
        Util.parseMinimal
          parser
          name
          input
    theParsing name input =
      fmap fst (actualParse name input)
    succeedSugarCube :: [FilePath] -> CUBE
    succeedSugarCube dataDirs=
      mkCUBE
        { inputDirs = dataDirs,
          rootName = rn,
          associatedNames = [("inputs", "")],
          expectedSuffix = "expected"
        }
    failSugarCube :: [FilePath] -> CUBE
    failSugarCube dataDirs=
      mkCUBE
        { inputDirs = dataDirs,
          rootName = rn,
          associatedNames = [("inputs", "")],
          expectedSuffix = "expected"
        }    
