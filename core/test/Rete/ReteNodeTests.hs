{-#OPTIONS_GHC -Wwarn=unused-top-binds -Wwarn=x-partial #-}
module Rete.ReteNodeTests (reteNodeCommands, allReteNodeCommands, HasReteGraph (..)) where
import Core.Utils.Prettyprinting
import Core.Variable.Substitution

import Data.The
import Agent    
import qualified Gen 
import qualified Gen.Patterns as Gen


import Core.Utils.MetaTypes
import Util
import Control.Applicative


import Control.Monad
import Control.Monad.IO.Class


import Core.Identifiers

import Core.Patterns.PatternSyntax

import Rete.ReteBuilder
import Rete.ReteGraphChecks

import Core.Identifiers.IdentifierGenerator

import Data.Foldable







import Data.Set as Set (empty, insert,  null, size, member)

import Data.Typeable

import GHC.Generics (Generic, M1 (M1))
import Hedgehog
import Hedgehog.Gen as Gen
import Hedgehog.Range as Range
import Meta.Model


import Core.Identifiers.KnownIdentifiers


import System.IO.Unsafe (unsafePerformIO)
import Prelude hiding (Ordering (..))

-- TODO Use actual node identifiers
mkJoinCondition :: MonadGen m => m JoinCondition
mkJoinCondition = do
  f1 <- Gen.enumBounded
  let ident = NodeIdentifier (topLevelStateIdentifier^.identifier)
  f2 <- Gen.enumBounded
  pred <- Gen.frequency [(6, pure EQ), (1, pure LT), (1, pure LTE), (1, pure GTE), (1, pure GT)]
  pure $ JoinCondition (JoinNodeArgument f1 ident) (NodeArgument (JoinNodeArgument  f2 ident)) pred

data CreateNewReteGraph  a v = CreateNewReteGraph deriving (Show, Generic, FunctorB, TraversableB)

data CreateNewAlphaNodesFromWMEPattern  a v = CreateNewAlphaNodesFromWMEPattern (WME' a) WMEPattern deriving (Show, Generic, FunctorB, TraversableB)

data CreateNewMemoryNode  a v = CreateNewMemoryNode [Var (ReteNode  a) v] deriving (Show, Generic, FunctorB, TraversableB)

data CreateNewJoinNode  a v = CreateNewJoinNode [Var (ReteNode  a) v] (Var (ReteNode  a) v) [(JoinCondition, Var (ReteNode  a) v)] deriving (Show, Generic, FunctorB, TraversableB)

data CreateNewProductionNode  a v = CreateNewProductionNode [Var (ReteNode  a) v]  deriving (Generic, FunctorB, TraversableB)

data CreateNewNegativeNode a v = CreateNewNegativeNode [Var (ReteNode a) v]  (Var (ReteNode a) v) [(JoinCondition, Var (ReteNode  a) v)] deriving (Show, Generic, FunctorB, TraversableB)

data CreateNCCNodes  a v = CreateNCCNodes [Var (ReteNode  a) v] (Var (ReteNode  a) v) [(JoinCondition, Var (ReteNode  a) v)] deriving (Show, Generic, FunctorB, TraversableB)
                                                                                          
                                                                                          
                                  
instance (Show1 v, Renderable a) => Show (CreateNewProductionNode  a v) where
  show (CreateNewProductionNode parents ) = "CreateNewProductionNode " ++ show parents

allReteNodeCommands :: forall  a gen m state. (a ~ WMEMetadata , Show a, WMEIsState a,  Show (state Concrete), Typeable a, MonadIO m, MonadTest m, HasReteGraph  a state, MonadGen gen) => Gens state gen  a -> Agent -> [Command gen m state]
allReteNodeCommands gens agent = --(  ($ agent) <$> [createNewReteGraphCommand  @a]) ++
                                 reteNodeCommands gens agent

reteNodeCommands :: forall  a gen m state. (a ~ WMEMetadata , Show a, WMEIsState a,  Show (state Concrete), Typeable a, MonadIO m, MonadTest m, HasReteGraph  a state, MonadGen gen) => Gens state gen  a -> Agent -> [Command gen m state]
reteNodeCommands gens agent = ($ agent) <$> [createNewAlphaNodesCommand  @a gens ,
                                             createNewMemoryNodeCommand  @a,
                                             createNewJoinNodeCommand  @a,
                                             createNewProductionNodeCommand  @a gens,
                                             createNewNegativeNodeCommand @a
                                            ]

createNewReteGraphCommand :: forall  a gen m state. ( a ~ WMEMetadata, Show (state Concrete), Typeable a, MonadIO m, MonadTest m, HasReteGraph  a state, MonadGen gen) => Agent -> Command gen m state
createNewReteGraphCommand agent' =
  let genInput :: state (Hedgehog.Symbolic) -> Maybe (gen (CreateNewReteGraph  a Hedgehog.Symbolic))
      genInput state = Just $ pure CreateNewReteGraph

      execute :: (CreateNewReteGraph  a Hedgehog.Concrete) -> m ()
      execute (CreateNewReteGraph) = do
        label "Create new Rete graph"
        evalIO @m .  runPrePSCM' False agent' $ newEmptyReteNetwork       

      cbs =
        [ Update $ \state (CreateNewReteGraph) _ ->
            state
              & reteNodes  @a
                .~ Set.empty,
          ( Ensure $ \_ state _ _ -> do
              annotateShow state
              Hedgehog.assert (unsafePerformIO $ doTransaction $ reteEnvironmentIsEmpty (state ^. agent . reteEnvironment  ))
          ),
          consistentReteGraph False
        ]
   in Command genInput execute cbs

createNewAlphaNodesCommand :: forall  a gen m state. (a ~ WMEMetadata , Show a, WMEIsState a,  Show (state Concrete), Typeable a, MonadIO m, MonadTest m, HasReteGraph  a state, MonadGen gen) => Gens state gen  a -> Agent -> Command gen m state
createNewAlphaNodesCommand (Gens genMetadata) agent' =
  let genInput :: state (Hedgehog.Symbolic) -> Maybe (gen (CreateNewAlphaNodesFromWMEPattern  a Hedgehog.Symbolic))
      genInput state = Just $ do
        (wme, _, pat) <- Gen.mkWMEAndCompatiblePattern (genMetadata state)
        pure $ CreateNewAlphaNodesFromWMEPattern wme pat
      execute :: (CreateNewAlphaNodesFromWMEPattern  a Hedgehog.Concrete) -> m (ReteNode  a )
      execute (CreateNewAlphaNodesFromWMEPattern wme pats) = do
        label "Create new alpha node from pattern"
        annotateShow pats
        newNodes <- evalIO @m . runPSCM' False agent' $ execute' pats
        annotateShow newNodes
        pure newNodes
        where
--          execute' :: forall r. (Members '[Log, IDGen, Embed STM, Embed IO, Final IO] r) => WMEPattern -> Sem r ((ReteNode  a))
          execute' pats =
            do
              
                nodes <-
                   createOrShareAlphaNodes -- @_  @a
                    (Just pats)
                pure (nodes^?!_Just. _1 . to the)
      addWMEToState = Update $ \s (CreateNewAlphaNodesFromWMEPattern wme _) _ -> s & wmesYetToAdd  @a %~ insert wme
      addAmem = Update $ \state _ node -> state & modelAlphaNodes  @a @state %~ (Set.insert ( node))
   in Command genInput execute (addAmem : addWMEToState : addsNodeCallbacks False)

createNewMemoryNodeCommand :: forall  a gen m state. (a ~ WMEMetadata , Show (state Concrete), Typeable a, MonadIO m, MonadTest m, HasReteGraph  a state, MonadGen gen) => Agent -> Command gen m state
createNewMemoryNodeCommand agent' =
  let genInput :: state (Hedgehog.Symbolic) -> Maybe (gen (CreateNewMemoryNode  a Hedgehog.Symbolic))
      genInput state =
        if Set.null (state ^. reteNodes  @a)
          then Nothing
          else Just $ do
            nodes <- mkParentNodes state
            pure $ CreateNewMemoryNode nodes
      execute :: (CreateNewMemoryNode  a Hedgehog.Concrete) -> m (ReteNode  a)
      execute (CreateNewMemoryNode nodes) = do
        label "Create new memory node from parent nodes"
        annotateShow nodes
        newNode <- evalIO @m . runPSCM' True agent' $ execute' nodes
        annotateShow newNode
        pure newNode
        where
       --   execute' :: forall r. (Members '[Log,IDGen,Embed STM, Embed IO, Final IO] r) => [Var (ReteNode  a) Concrete] -> Sem r (ReteNode  a)
          execute' nodes' =
            do
                nodes <-
                     createOrShareMemoryNodes -- @_  @a
                    ([fmap concrete nodes'])
                pure (the $ head nodes)
   in Command genInput execute (addsNodeCallbacks False)


createNewNegativeNodeCommand :: forall  a gen m state. (a ~ WMEMetadata , Show (state Concrete), Typeable a, MonadIO m, MonadTest m, HasReteGraph  a state, MonadGen gen) => Agent -> Command gen m state
createNewNegativeNodeCommand agent' =
  let --genInput :: state (Hedgehog.Symbolic) -> Maybe (gen (CreateNewNegativeNode a Hedgehog.Symbolic))
      genInput state =
        if Set.null (state ^. reteNodes  @a)
          then Nothing
          else Just $ do
            nodes <- mkParentNodes state
            alpha <- Gen.element (state^..modelAlphaNodes.folded)
            conditions <- genJoinConditions  nodes alpha
            pure $ CreateNewNegativeNode nodes alpha conditions
      execute :: (CreateNewNegativeNode  a Hedgehog.Concrete) -> m (ReteNode  a)
      execute (CreateNewNegativeNode nodes alpha conditions') = do
        label "Create new negative node from parents"
        annotateShow (alpha : nodes)
        newNode <- evalIO @m .  runPSCM' False agent' $ execute
        annotateShow newNode
        pure newNode
        where
       --   execute' :: forall r. (Members '[Log,IDGen,Embed STM, Embed IO, Final IO] r) => [Var (ReteNode  a) Concrete] -> Sem r (ReteNode  a)
          execute =
            do
                node <-
                     createOrShareNegativeNode
                                     (fmap concrete nodes)
                                     ((concrete alpha)^?!_CorrectSubType)
                                     (constructConditions conditions')

                pure (the node)
   in Command genInput execute (addsNodeCallbacks False)

 -- TODO: Improve generation of join condition reference nodes (do a transitive closure of the parents? How can do that for symbolic nodes tho?)
genJoinConditions :: (Eq1 v, Eq (ReteNode a), MonadGen m) => [Var (ReteNode a) v] -> Var (ReteNode a) v -> m [(JoinCondition, Var (ReteNode  a) v)]
genJoinConditions nodes amem = Gen.addExtra [[]] do
            joinConditions <- Gen.list (Range.exponential 0 (min 5 (length nodes))) $ do
              priorNode <- Gen.element (amem : nodes)
              cond <- mkJoinCondition
              pure (cond, priorNode)
            pure joinConditions

constructConditions ::[(JoinCondition, Var (ReteNode  a) Concrete)] ->  [JoinCondition]
constructConditions conditions' = conditions' & traversed %~ (\(c, n) -> c & priorTokenNode .~ ((concrete n) ^. nodeIdentifier))
                 
{-# INLINE mkParentNodes #-}
mkParentNodes :: forall  a state gen. (HasReteGraph  a state, MonadGen gen) => state (Hedgehog.Symbolic) -> gen [(Var (ReteNode  a) Symbolic)]
mkParentNodes state = fmap toList $ Gen.set (Range.exponential 1 (min 7 (Set.size (state ^. reteNodes  @a)))) (Gen.element (state ^.. reteNodes  @a . folded))

createNewJoinNodeCommand :: forall  a gen m state. (a ~ WMEMetadata , Show (state Concrete), Typeable a, MonadIO m, MonadTest m, HasReteGraph  a state, MonadGen gen) => Agent -> Command gen m state
createNewJoinNodeCommand agent' =
  let genInput :: state (Hedgehog.Symbolic) -> Maybe (gen (CreateNewJoinNode  a Hedgehog.Symbolic))

      genInput state =
        if Set.null (state ^. reteNodes  @a) || Set.null (state ^. modelAlphaNodes  @a)
          then Nothing
          else Just $ do
            nodes <- mkParentNodes state
            amem <- Gen.element (state ^.. modelAlphaNodes  @a . folded)
            joinConditions <- genJoinConditions nodes amem
            pure $ CreateNewJoinNode nodes amem joinConditions
      execute :: (CreateNewJoinNode  a Hedgehog.Concrete) -> m (ReteNode  a)
      execute (CreateNewJoinNode nodes amem conditions') = do
        label "Create new join node from parent nodes"
        annotateShow nodes
        newNode <- evalIO @m .  runPSCM' False agent'  $ execute' nodes
        annotateShow newNode
        pure newNode
        where
--          execute' :: forall r. (Members '[Log, IDGen,Embed STM, Embed IO, Final IO] r) => [Var (ReteNode  a) Concrete] -> Sem r (ReteNode  a)
          execute' nodes' =
            do
                node <-
                   createOrShareJoinNode -- @_  @a
                    (fmap concrete nodes')
                    ((concrete amem)^?!_CorrectSubType)
                    (constructConditions conditions')
                pure (the node)
      addJNode = Update $ \state _ node -> state & modelJoinNodes  @a @state %~ (Set.insert ( node))
      requiresParents = Require \state (CreateNewJoinNode parents amem _)  -> (not (Set.null (state ^. reteNodes  @a) || Set.null (state ^. modelAlphaNodes  @a))) && all (\parent -> Set.member parent (state^.reteNodes)) (amem : parents) 
   in Command genInput execute (requiresParents : addJNode : addsNodeCallbacks False)

createNewProductionNodeCommand :: forall  a gen m state. (a ~ WMEMetadata ,Show (state Concrete), Typeable a, MonadIO m, MonadTest m, HasReteGraph  a state, MonadGen gen) => Gens state gen  a -> Agent -> Command gen m state
createNewProductionNodeCommand gens agent' =
  let genInput :: state (Hedgehog.Symbolic) -> Maybe (gen (CreateNewProductionNode  a Hedgehog.Symbolic))
      genInput state =
        if Set.null (state ^. reteNodes  @a)
          then Nothing
          else Just $ do
            nodes <- mkParentNodes state
            pure $ CreateNewProductionNode nodes
      execute :: (CreateNewProductionNode  a Hedgehog.Concrete) -> m (ReteNode  a)
      execute (CreateNewProductionNode nodes ) = do
        label "Create new production node from parent nodes"
        annotateShow nodes
        newNode <- evalIO @m .  runPSCM' False agent'  $ execute' nodes
        annotateShow newNode
        pure newNode
        where
         -- execute' :: forall r. (Members '[Log,IDGen,Embed STM, Embed IO, Final IO] r) => [Var (ReteNode  a) Concrete] -> Sem r (ReteNode  a)
          execute' nodes' =
            do
              
                ruleID <- RuleIdentifier <$> newGlobalIdentifier
                nodes <-
                  createProductionNode -- @_  @a
                    (fmap concrete nodes')
                    ruleID emptySubstitution
                pure (the nodes)
      addPNode = Update $ \state _ node -> state & modelProductionNodes  @a @state %~ (Set.insert (node))
   in Command genInput execute (addPNode : addsNodeCallbacks False)
