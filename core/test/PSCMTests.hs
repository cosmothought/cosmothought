module PSCMTests where
import PSCM

--import Polysemy    
import Cosmolude
--import Control.Concurrent    
import PSCM.ProductionSystemTests
import PSCM.OperatorTests    
import qualified OpenTelemetry.Trace.Core as OT
import OpenTelemetry.Trace.Core hiding (inSpan)
--import Core.Configuration
import Agent
import ForTesting
import Polysemy.Reader    
import Util
import Test.Tasty.Hedgehog
import Test.Tasty
import Model.PSCM.BasicTypes
--import Core.Utils.Logging    (push)
--import PSCM
import Polysemy.State
import Polysemy.Tagged
import Model.PSCM
import Data.Text
import Numeric.Natural
    
pscmTests :: TestTree
pscmTests = testGroup "PSCM"
                    [
                     pscmLoopTests,
                     operatorTests,
                     productionSystemTests
  
                    ]    
    
    
pscmLoopTests :: TestTree
pscmLoopTests = testGroup "PSCM loop" [emptyLoopTest, first10PhasesAfterValidOperatorProposal] --fivePSCMPhasesTest]


first10PhasesAfterValidOperatorProposal :: TestTree
first10PhasesAfterValidOperatorProposal = testGroup "First 10 phases after valid operator proposal" [validOperatorProposal 0 0 InputElaboration, -- Don't run anything. So, 10+1 I guess?
                                                                                         validOperatorProposal 1 1 OperatorProposalAndEvaluation,
                                                                                         validOperatorProposal 2 1 OperatorDecision,
                                                                                         validOperatorProposal 3 2 InputElaboration,
                                                                                         validOperatorProposal 4 2 OperatorProposalAndEvaluation,
                                                                                         validOperatorProposal 5 2 OperatorDecision,
                                                                                         validOperatorProposal 6 2 OperatorElaboration, -- Should have decided on an operator in the superstate
                                                                                         validOperatorProposal 7 1 OperatorApplication, -- Pop to superstate
                                                                                         validOperatorProposal 8 1 OutputToEnvironment,
                                                                                         validOperatorProposal 9 1 InputElaboration,
                                                                                         validOperatorProposal 10 1 OperatorProposalAndEvaluation]
       where
         validOperatorProposal = nPSCMPhasesTest "add rules {(state <s1> type state) (<s2> type state) (<s1> superstate <s2>) (<s2> ^name top) --> (<s2> ^operator <hello> +)}"
                                                                                                       
                
nPSCMPhasesTest :: Text -> PSCMCycleCount -> Natural -> PSCMLoopPhase ->  TestTree
nPSCMPhasesTest command n depth nextPhase  = localOption (mkTimeout 50000000) $ testProperty (fromString $ show n ++ " PSCM Phases with an operator proposal") $ withTests 1 $ property $  do
  stats <- evalIO $  withFreshAgentIO \agent ->  OT.inSpan (agent^.tracerL) (fromString $ "Test:PSCM:PSCMLoop:nPSCMPhasesTest, n=" ++ show n) OT.defaultSpanArguments do
    runPSCM' False agent do
      let stats = newPSCMStatistics
          props = mempty
      execState stats (
                       runNTimes n agent
                       & untag @AllOperatorProposals & evalState props
                      )
                             
  Util.annotate "PSCM cycle count:"                      
  stats^.pscmCycleCount === n
  Util.annotate "Current state depth:"
  stats ^. currentStateDepth === depth
  Util.annotate "Next phase:"
  stats ^. currentPhase === nextPhase

 where
  performN :: Monad m => PSCMCycleCount -> m t -> m [t]
  performN 0 _ = return []
  performN n m =
    do x  <- m
       xs <- performN (n-1) m
       return $ x:xs
   
  runNTimes n agent = do
      !_ <- runReader agent $ parseAndExecute "" command
      void (performN n enterPSCMPhase)

                
fivePSCMPhasesTest :: TestTree
fivePSCMPhasesTest = localOption (mkTimeout 50000000) $ testProperty "Five PSCM Phases with an operator proposal" $ withTests 1 $ property $  do
  stats <- evalIO $  withFreshAgentIO \agent ->  OT.inSpan (agent^.tracerL) "Test:PSCM:PSCMLoop:fivePSCMPhasesTest" OT.defaultSpanArguments do
    runPSCM' False agent do
      let stats = newPSCMStatistics
          props = mempty
      execState stats (
                       runFiveTimes agent
                       & untag @AllOperatorProposals & evalState props
                      )
                             
                      
  stats^.pscmCycleCount === 5
  stats ^. currentPhase === OperatorElaboration
 where
   runFiveTimes agent = do
      !_ <- runReader agent $ parseAndExecute ""{-processCommand' agent-} "add rules {(state <s1> type state) (<s2> type state) (<s1> superstate <s2>) --> (<s2> ^operator <hello> +)}" --"!add rules {(state <s1> type state) (<s1> been hit) --> log at level error on activation \"hello\"} {(state <s1> type state) (<s2> type state) (<s2> superstate <s2>) --> (<s2> ^operator <hello> +)} {(state <s1> type state) (<s1> operator <hello>) --> (<s1> been hit)}"
     
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase

              
emptyLoopTest :: TestTree
emptyLoopTest = localOption (mkTimeout 50000000) $ testProperty "Empty loop" $ withTests 1 $ property $  do
  stats <- evalIO $ {-#SCC emptyLoopTest #-} withFreshAgentIO \agent ->  OT.inSpan (agent^.tracerL) "Test:PSCM:PSCMLoop:emptyLoopTest" OT.defaultSpanArguments do
    runPSCM' False agent do
      let stats = newPSCMStatistics
          props = mempty
      execState stats (
                       runThirteenTimes
                       & untag @AllOperatorProposals & evalState props
                      )
                             
                      
  stats^.pscmCycleCount === 13
  stats ^. currentPhase === OperatorProposalAndEvaluation
 where
   runThirteenTimes = do
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
      enterPSCMPhase
