{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE OverloadedStrings #-}

module Meta.Model (module Meta.Model, module ForTesting) where
import Core.Utils.Rendering.CoreDoc




import Control.Lens
import Core.CoreTypes



import Core.Rule
import Core.WME

import Data.Set



import ForTesting

import GHC.Generics
import OpenTelemetry.Trace.Core
import Hedgehog  (Var,     Symbolic  )

import Prettyprinter
import qualified Data.Text as T
import Data.Version (showVersion)
import Paths_cosmothought_core

import Core.Identifiers.IdentifierType
import Rete.ReteTypes    
import Data.Kind
import Agent

import qualified System.IO.Unsafe (unsafePerformIO)
testTracer :: Tracer
testTracer = let
    instrumentationLib = InstrumentationLibrary
                         {
                           libraryName = "CosmoThought-core-test"
                         , libraryVersion = T.pack $ showVersion version
                         }
    in
      System.IO.Unsafe.unsafePerformIO do
        tp <- getGlobalTracerProvider
        pure (makeTracer tp instrumentationLib (TracerOptions Nothing))
      
    
type MyMetadata = WMEMetadata 

type MyReteNodes = ReteNode MyMetadata

data ModelState  a v = ModelState
  { _knownIdentifiers' :: (Set (Var Identifier v)),
    _identifiersCreated' :: Int,
    _reteNodes' :: Set (Var (ReteNode  a) v),
    _wmesYetToAdd' :: Set (WME' a),
    _anodes :: Set (Var ((ReteNode  a) {-? IsAlphaNode a-}) v),
    _pnodes :: Set (Var ((ReteNode  a) {-? IsProductionNode a-}) v),
    _jnodes :: Set (Var ((ReteNode  a) {-? IsJoinNode a-}) v),
    _telemetrySpanContext' :: SpanContext,
    _agentData' :: Agent
               
  }
  deriving (Generic, Show)
makeLenses ''ModelState
class HasSpanContext c where
    telemetrySpanContext :: Lens' c SpanContext
instance HasSpanContext (ModelState a v) where
    {-# INLINE telemetrySpanContext #-}
    telemetrySpanContext = telemetrySpanContext'
class HasKnownIdentifiers (c :: (Type -> Type) -> Type) where
  knownIdentifiers :: Lens' (c v) (Set (Var Identifier v))
  identifiersCreated :: Lens' (c v) Int

type MyRule = Rule

data Gens state gen metadata = Gens { _metadataGen ::  ((state Hedgehog.Symbolic) -> gen metadata)}
makeLenses ''Gens
class (Renderable metadata, metadata ~ WMEMetadata) => HasReteGraph (metadata :: Type) (c :: (Type -> Type) -> Type) where
  modelAlphaNodes :: Lens' (c v) (Set (Var ((ReteNode  metadata) {-? IsAlphaNode metadata-}) v))
  modelProductionNodes :: Lens' (c v) (Set (Var ((ReteNode  metadata) {-? IsProductionNode metadata-}) v))
  modelJoinNodes :: Lens' (c v) (Set (Var ((ReteNode  metadata) {-? IsJoinNode metadata-}) v))
  reteNodes :: Lens' (c v) (Set (Var (ReteNode  metadata) v))
  wmesYetToAdd :: Lens' (c v) (Set (WME' metadata))
  agent :: Lens' (c v) (Agent)



initialState :: forall v. Pretty MyMetadata => SpanContext -> Agent -> ModelState MyMetadata v
initialState spanContext agent'  = ModelState {} & knownIdentifiers .~ empty & identifiersCreated .~ 0 &  reteNodes' .~ empty & wmesYetToAdd' .~ empty &  anodes .~ empty & pnodes .~ empty & jnodes .~ empty &  telemetrySpanContext .~ spanContext & agentData' .~agent'

-- resetSystem :: HasCallStack =>  Agent -> IO ()
-- resetSystem agent' = do
--   withFreshEnv (\() ->
--    doTransaction $ do                 
--     embedSTM $ resetReteEnvironment reteEnv
--     resetWorkingMemory wm
--     resetRuleDatabase db
--       )
instance (Renderable a, a ~ WMEMetadata) => HasReteGraph a (ModelState a) where
  {-# INLINE reteNodes #-}
  reteNodes = reteNodes'
  {-# INLINE wmesYetToAdd #-}
  wmesYetToAdd = wmesYetToAdd'
  {-# INLINE modelAlphaNodes #-}
  modelAlphaNodes = anodes
  {-# INLINE modelProductionNodes #-}
  modelProductionNodes = pnodes
  {-# INLINE modelJoinNodes #-}
  modelJoinNodes = jnodes
  {-# INLINE agent #-}
  agent = agentData'

-- Gen.frequency [(65, pure False), (35, pure True)]
instance HasKnownIdentifiers (ModelState a) where
  {-# INLINE knownIdentifiers #-}
  knownIdentifiers :: Lens' (ModelState a v) (Set (Var Identifier v))
  knownIdentifiers = knownIdentifiers'
  {-# INLINE identifiersCreated #-}
  identifiersCreated = identifiersCreated'
