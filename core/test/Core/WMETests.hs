module Core.WMETests (wmeTests) where


import Control.Concurrent.STM
import Control.Lens
import Core.CoreTypes

import Core.Utils.MetaTypes



import Core.WME
import Data.Interned

import Data.Ratio
import Data.String (IsString (..))
import qualified Data.Text as T


import Gen
import Hedgehog
import Hedgehog.Classes

import Hedgehog.Gen as Gen
import Hedgehog.Range as Range
import Meta.Model



import Test.Tasty

import Test.Tasty.Hedgehog
import Test.Tasty.Sugar

import Util (WhenParsing (..), makeParsingTests, parseMinimal, pretty, shouldParseTo', shouldn'tParse, testManyLaws)

wmeTests :: Gen MyMetadata -> IO ([CUBE], TestTree)
wmeTests genMetadata = do
  (parsingSugar, wmeParsingTests') <- wmeParsingTests
  pure $ (parsingSugar, testGroup "Basic WME functionality" [wmeParsingTests', trivialWMEParsingTests, valueParsingTests, testManyLaws "Algebraic laws" (Gen.randomWME genMetadata) wmeLaws wmeHigherLaws])

wmeLaws :: (Eq a, Show a) => [Gen a -> Laws]
wmeLaws = [eqLaws]

wmeHigherLaws :: [Laws]
wmeHigherLaws = []

wmeParsingTests :: IO ([CUBE], TestTree)
wmeParsingTests = makeParsingTests "*.wme" "Specific WMEs" (\(_wmeInfo,wme) -> do
                                                              
                                                              (pure [
                                                                    
                                                                   ])) (parseWME)

valueParsingTests :: TestTree
valueParsingTests = testGroup "Value parsing" [symbolParsing, rationalParsing, integerConstantParsingTests, constantRoundTripping]

symbolParsing :: TestTree
symbolParsing =
  testProperty "Symbol parsing" $ property $ do
    str <- forAll $ Gen.text (Range.linear 1 3) alpha
    let shouldParseTo = shouldParseTo' "" (show str)
    liftTest (Util.parseMinimal (parseValue ParsingWME) "" str `shouldParseTo` (Symbolic (Symbol (intern str))))
    success

rationalParsing :: TestTree
rationalParsing =
  testProperty "Rational parsing" $ property do
    numerator <- forAll $ Gen.integral (Range.exponentialFrom 0 (-10 ^ 9) (10 ^ 9))
    denominator <- forAll $ Gen.filter (/= 0) $ Gen.integral (Range.exponentialFrom 1 (-10 ^ 9) (10 ^ 9))
    let val = numerator % denominator
        str = fromString (show val)
    cover 20 "Negative" (numerator % denominator < 0)
    cover 20 "Positive" (numerator % denominator > 0)
    let shouldParseTo = shouldParseTo' "" (show str)
    liftTest (Util.parseMinimal (parseValue ParsingWME) "" str `shouldParseTo` (Constant (RationalValue (val))))
    success

integerConstantParsingTests :: TestTree
integerConstantParsingTests = testGroup "Integral constants" [integerParsing]

integerParsing :: TestTree
integerParsing =
  testProperty "Integer parsing" $ property do
    val <- forAll $ Gen.integral (Range.exponentialFrom 0 (-10 ^ 6) (10 ^ 6))
    cover 20 "Negative" (val < 0)
    cover 20 "Positive" (val > 0)
    let str = fromString (show val)
    annotate ("string: " ++ show str)
    annotate ("raw val: " ++ show val)
    let expected = Constant (IntValue (fromInteger val))
    annotate ("expected: " ++ show expected)
    let shouldParseTo = shouldParseTo' "" (show str)
    liftTest (Util.parseMinimal (parseValue ParsingWME) "" str `shouldParseTo` expected)
    success

constantRoundTripping :: TestTree
constantRoundTripping = testProperty "Constant value round tripping" $ property do
  val <- forAll $ Gen.constantValue
  cover 20 "Integer value" (has _IntValue val)
  cover 20 "Float value" (has _FloatValue val)
  cover 20 "Rational value" (has _RationalValue val)
  annotateShow val
  tripping (val) (T.pack . show . pretty) (Util.parseMinimal parseConstantValue "")

trivialWMEParsingTests :: TestTree
trivialWMEParsingTests = testGroup "Trivial WME parsing" [trivialWMEParseTest]

trivialWMEParseTest :: TestTree
trivialWMEParseTest =
  testProperty "Trivial non-state WME parsing with three symbols" $ withTests 500 $ property do
    afterOpeningParensSpacing <- forAll $ Gen.text (Range.linear 0 3) spaceGen
    isState' <- forAll $ Gen.bool
    cover 20 "WME is state" isState'
    cover 20 "WME isn't state" (not isState')
    let stateText = if isState' then "state" else T.empty
    afterStateSpacing <- forAll $ Gen.text (Range.linearFrom 1 0 4) spaceGen
    o <- forAll $ Gen.text (Range.linear 1 3) alpha
    firstSpacing <- forAll $ Gen.text (Range.linearFrom 1 0 3) spaceGen
    a <- forAll $ Gen.text (Range.linear 1 3) alpha
    secondSpacing <- forAll $ Gen.text (Range.linearFrom 1 0 3) spaceGen
    v <- forAll $ Gen.text (Range.linear 1 3) alpha
    thirdSpacing <- forAll $ Gen.text (Range.linearFrom 1 0 3) spaceGen
    oIdent <- forAll $ objectIdentifierNamed (intern o)
    wmeIdent <- forAll $ WMEIdentifier <$> Gen.identifier
    meta <- evalIO . atomically $ blankWMEMetadata
    let shouldParse = not (T.null firstSpacing || T.null secondSpacing)
        str = "("
              <> afterOpeningParensSpacing
              <> stateText
              <> afterStateSpacing
              <> o
              <> firstSpacing
              <> a
              <> secondSpacing
              <> v
              <> thirdSpacing
              <> ")"
        expectedWME = WME oIdent (Symbol (textToSymbolName (if isState' && T.null afterStateSpacing then stateText <> a else a))) (Symbolic (Symbol (textToSymbolName v))) wmeIdent (meta & isState .~ isState')
    cover 15 "Incorrect syntax" (not shouldParse)
    cover 60 "Correct syntax" shouldParse
    annotate (show str)
    let tried = fmap (\(_, wme) -> wme & wmeIdentity .~ wmeIdent) $ Util.parseMinimal (parseWME) "" str
    if shouldParse
      then do
        let shouldParseTo = shouldParseTo' "" (show str)
        liftTest (tried `shouldParseTo` expectedWME)
      else liftTest ((fmap (\(_, wme) -> wme & wmeIdentity .~ wmeIdent) $ Util.parseMinimal (parseWME) "" str) `shouldn'tParse` ())

    success
  where
    spaceGen = Gen.constant ' '
