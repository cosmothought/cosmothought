{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE TupleSections #-}
{-# OPTIONS_GHC -foptimal-applicative-do  -Wwarn=unused-matches -Wwarn=unused-local-binds -Wwarn=type-defaults #-}

module Gen.Rules where
import Control.Monad.IO.Class
import Polysemy.Error
import Polysemy.Reader (runReader)    
import Polysemy.OpenTelemetry    
import Core.Utils.Logging
import Polysemy.Resource    
import Algebra.Lattice    
import Polysemy.NonDet
import Polysemy
import Control.Lens hiding (Identity (..))
import Core.Rule.Construction
import Core.Rule.Parts
import Core.Action
import Core.Action.Expression
import Core.CoreTypes as CT
import Core.Identifiers
import qualified Core.Identifiers as Ident
import Core.Patterns.PatternSyntax
import Core.Rule

import Core.Utils.Rendering.CoreDoc
import Core.Variable
import Core.Variable.AlphaEquivalence
import Core.Variable.Substitution
import Data.List.NonEmpty
import Data.Sequence as Seq hiding ((:<))
import qualified Di.Df1 as Dif1
import Hedgehog
import Hedgehog.Gen as Gen
import Hedgehog.Range as Range
import Prettyprinter
import qualified Gen
import qualified Gen.Patterns as Gen
import Meta.Model (testTracer)
    
    
makeRuleRHSGiven :: MonadGen m => NonEmpty RuleCondition -> m RuleRHSWithPlaceholders
makeRuleRHSGiven pats = do
  lvl <- df1Level
  let positivePats =
        pats
          ^.. folded
            . conditionPatterns
            . ifiltered
              ( \i p ->
                  i == PositiveAppearance
                    && has (patternObject . _APatternVariable . _1 . _PlainPatternVariable) p
                    && hasn't (patternObject . _APatternVariable . _2 . _Just) p
                    && hasn't (patternAttribute . _DontCareCondition) p
                    && hasn't (patternAttribute . _APatternVariable . _2 . _Just) p
                    && hasn't (patternValue . _DontCareCondition) p
                    && hasn't (patternValue . _APatternVariable . _2 . _Just) p
              )
  let logAction = Core.Action.Log OnActivation lvl $! layoutCompact $! fuse Deep ("Rule activated, pattern:" <+> (render $! (pats ^.. folded)))
      addActions = positivePats ^.. folded . to (\pat -> NewWME $ pat & patternPreference .~ Just Acceptable)
      removeActions = positivePats ^.. folded . to (\pat -> RemoveWME $ pat & patternPreference .~ Just Reject)
  actions <- Gen.seq (Range.linear 1 5) (Gen.element (logAction : addActions ++ removeActions))
  pure $! RuleRHS' actions

df1Level :: MonadGen m => m Dif1.Level
df1Level = Gen.prune Gen.enumBounded

genRuleExprSubstitutions :: MonadGen m => m (Substitution (RuleExpr Variable))
genRuleExprSubstitutions = pure emptySubstitution

genNEPositiveConditions :: (HasMaybeWMPreference a ObjectIdentifier, WMEIsState a, MonadGen m) => m a -> m (Seq (WME' a), SubstitutionLattice Value, NonEmpty RuleCondition)
genNEPositiveConditions genMetadata = do
  !patsAndMatches <- Gen.nonEmpty (Range.linearFrom 1 1 5) (Gen.filterT (\(_, _, pat) -> isLinearPattern (makeVariablesTermLocal pat)) $ (Gen.mkWMEAndCompatiblePattern genMetadata))
  let (wmes,s,cs) = patsAndMatches ^. traversed1 . to (\(!wme, !sub, !pat) -> (Seq.singleton wme, Join sub, (PositiveCondition pat) :| []))
  pure $! (wmes, getJoin s, cs)

genPositiveConditions :: (HasMaybeWMPreference a ObjectIdentifier, WMEIsState a, MonadGen m) => m a -> m (Seq (WME' a), SubstitutionLattice Value, [RuleCondition])
genPositiveConditions genMetadata = do
  -- TODO FIXME: Once non-constant tests are implemented, remove the filter
  !patsAndMatches <- Gen.list (Range.linear 0 6) (Gen.filterT (\(_, _, pat) -> isLinearPattern (makeVariablesTermLocal pat)) $ Gen.mkWMEAndCompatiblePattern genMetadata)
  let (wmes, s, cs) = patsAndMatches ^. traversed . to (\(!wme, !sub, !pat) -> (Seq.singleton wme, Meet sub, [PositiveCondition pat]) )                    
  pure $! (wmes, getMeet s, cs)

genNegatedConjunction :: (MonadGen m) => (Seq (WME' a), SubstitutionLattice Value, [RuleCondition]) -> m (Seq (WME' a), SubstitutionLattice Value, [RuleCondition])
genNegatedConjunction (_, _, []) = Gen.discard
genNegatedConjunction (!wmes, !sub, (!x) : xs) = Gen.shrink
  ( \(!wmes, !sub, !conds) -> case conds of
      (NegatedConjunction ((NegatedConjunction (!x :| !xs)) :| [])) : ys -> [(wmes, sub, [x]), (wmes, sub, x : xs <> ys)]
      _ -> []
  )
  do
    pure $! (wmes, sub, [simplifyConditionClassicalLogic' (NegatedConjunction (x :| xs))])

generateRuleConditions :: (HasMaybeWMPreference a ObjectIdentifier, WMEIsState a, MonadGen m) => m a -> m (Seq (WME' a), SubstitutionLattice Value, [RuleCondition])
generateRuleConditions !genMetadata = do
  res@(!wmes, !sub, !conds) <- Gen.recursive Gen.choice [genPositiveConditions genMetadata] [Gen.subtermM (Gen.filterT (\(_, _, !conds) -> has folded conds) $! generateRuleConditions genMetadata) genNegatedConjunction]
  pure $! res

--   patsAndMatches <- Gen.filterT (/= Seq.empty) $ Gen.seq (Range.linearFrom 1 2 10) (Gen.mkWMEAndCompatiblePattern genMetadata)
--   let (wmes, sub, pats) = patsAndMatches ^?! folded . to (\(wme, sub, pat) -> (Seq.singleton wme, sub, pat :| []))

data RuleGenerationContainer a = RuleGenerationContainer (Seq (WME' a)) (SubstitutionLattice Value) Rule

instance HasVariables (RuleGenerationContainer a) where
  {-# INLINE variables #-}
  variables f (RuleGenerationContainer wmes sub rule) = RuleGenerationContainer <$> pure wmes <*> traverseOf (__Substitution . traverseSubstitutionKeysWith const) f sub <*> variables f rule

generateStateTestPattern :: forall m a. (HasMaybeWMPreference a ObjectIdentifier, WMEIsState a, MonadGen m) => m a -> m (WME' a, SubstitutionLattice Value, WMEPattern)
generateStateTestPattern genMetadata = do
  (wme,sub,pat) <- Gen.filterT (\(_, _, !stp) -> has (patternObject . _APatternVariable . _1 . variable) stp && isLinearPattern (makeVariablesTermLocal stp))  $ Gen.mkWMEAndCompatiblePattern genMetadata
  let sv = pat^?! patternObject . _APatternVariable . _1 . variable
  let pat' = pat  & patternObject .~ APatternVariable (StatePatternVariable sv) Nothing & patternAttribute . predicate .~ Nothing & patternValue . predicate .~ Nothing & patternPreference .~ Nothing
  pure (wme, sub, pat')
  
makeRuleAndNearbyWMEs :: forall m a. (MonadIO m, HasMaybeWMPreference a ObjectIdentifier, WMEIsState a, MonadGen m) => m a -> m (Seq (WME' a), SubstitutionLattice Value, Rule)
makeRuleAndNearbyWMEs genMetadata = do
  (!wme', !sub', !posCond) <- generateStateTestPattern genMetadata
  (!wmes', !sub, !conds') <-
    Gen.justT
      (do
          (!w, !s, !c') <- generateRuleConditions genMetadata
          let s' = mergeSubstitutions [sub', s]
          let c =
                    c'
                      & traversed
                        . conditionPatterns
                        . patternObject
                        . patVariable
                        %~ \case
                          StatePatternVariable v -> PlainPatternVariable v
                          v -> v

          pure $! Just (w, s', c)
      )
  let preConds = (PositiveCondition posCond :| (conds'^..folded))
      lhs = RuleLHS' preConds
      !wmes = wme' Control.Lens.:< wmes'
  rhs <- Gen.Rules.makeRuleRHSGiven preConds
  ident <- Gen.prune $! Ident.RuleIdentifier <$> Gen.identifier
  extraBindings <- genRuleExprSubstitutions
  generatedRule <- liftIO $  runFinal .  embedToFinal  . resourceToIOFinal .  runOpenTelemetrySpan . runOpenTelemetryContext . discardStandardLog . runReader testTracer . runError . runNonDetMaybe  $ elaborateRule ident "Generated rule" lhs rhs (extraBindings^. re __Substitution)
  rule <- case  generatedRule of            
            Right (Just r) -> pure r
            _ ->  Gen.discard
  let (RuleGenerationContainer !finalWMEs !finalSub !finalRule) = makeVariablesTermLocal (RuleGenerationContainer wmes sub rule)
  pure $! (finalWMEs, finalSub, updateStateTestPattern finalRule)
