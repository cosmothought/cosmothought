module Agent.Run.Memory where
import Core.WorkingMemory
import Core.Rule.Database
import Polysemy
import Polysemy.Error
import Core.Identifiers
import Polysemy.Internal
import Core.CoreTypes
import Data.Function
import Core.Identifiers.IdentifierGenerator
import Core.Identifiers.IdentifierType
import Prelude (Show)    
type BasicMemoryEffects metadata = '[ObjectIdentityMap (WME' metadata) , WorkingMemory' metadata, RuleDatabase, IDGen, GlobalContextID]
type BasicMemoryConstraints agent metadata r = (HasIdentifier agent, Member (Error RuleNotInRuleDatabase) r, HasRuleDB agent, WorkingMemoryConstraints agent metadata r, Show metadata)
runBasicMemory :: BasicMemoryConstraints agent metadata r  => agent -> Sem ( Append (BasicMemoryEffects metadata) r) x -> Sem r  x
runBasicMemory agent =
    runGlobalContextWith agent 
    . runIDGenWith agent
    . runRuleDatabase agent
    . processWorkingMemory 
    . runObjectIdentityMapWithWorkingMemoryData
