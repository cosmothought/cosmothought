module Agent.Run.Initialisation where
import Cosmolude
import Data.Either
import Agent.AgentData as AAD
import DiPolysemy
import Polysemy.Error
import Agent.Run.LowLevelInfrastructure
import Core.CoreTypes
--import Core.Identifiers.IdentifierGenerator
getNewAgentData :: (CreateBlankWMEMetadata metadata, HasWMESupport metadata, HasCreatingState metadata, Show metadata, WMEIsState metadata, WMETruthValue (WME' metadata), HasMaybeWMPreference metadata ObjectIdentifier, HasExtraObjectIdentifiers metadata) => IO (Either AgentInitialisationError (AgentData metadata proof))
getNewAgentData  = do
  tracer <- cosmoThoughtCoreTracer
  runLowLevelEffects tracer .  runDiNoop . interpretTransaction . runError $ newAgentData tracer 
  
resetAgentData :: (CreateBlankWMEMetadata metadata, HasWMESupport metadata, HasCreatingState metadata, Show metadata, WMEIsState metadata, WMETruthValue (WME' metadata), HasMaybeWMPreference metadata ObjectIdentifier, HasExtraObjectIdentifiers metadata) => AgentData metadata proof -> IO (Either AgentInitialisationError ())
resetAgentData agentData = do
  tracer <- cosmoThoughtCoreTracer
  runLowLevelEffects tracer . runDiNoop .   runError$ AAD.resetAgentData agentData  
