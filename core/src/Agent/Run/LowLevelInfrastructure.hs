module Agent.Run.LowLevelInfrastructure where
import Paths_cosmothought_core
import OpenTelemetry.Trace.Core
import Cosmolude
import Data.Int (Int)    
import Polysemy
import Polysemy.Internal
import Agent.AgentData
import qualified Data.Text as T    




type LowLevelEffects = Append LowLevelInfrastructureEffects '[Embed IO, Final IO]    


flushTracerProvider :: Members '[Embed IO] r => Maybe Int -> Sem r x -> Sem r x
flushTracerProvider timeout x = do
  embed $ do
    tp <- getGlobalTracerProvider
    void $  forceFlushTracerProvider tp timeout
  x
    
    
{-# INLINE runLowLevelInfrastructureEffects #-}
runLowLevelInfrastructureEffects :: (HasTracer agent, Members '[Embed IO, Final IO] r) => agent -> InterpretersFor LowLevelInfrastructureEffects r
runLowLevelInfrastructureEffects agent = flushTracerProvider Nothing
      . resourceToIOFinal
      . runInputConst (agent^.tracerL)
      . runOpenTelemetrySpan
      . runOpenTelemetryContext
      . interpretTransactional
    
{-# INLINE runLowLevelEffects #-}
runLowLevelEffects :: HasTracer agent => agent ->  Sem LowLevelEffects x -> IO x
runLowLevelEffects tracer = runFinal
                            . embedToFinal
                            . runLowLevelInfrastructureEffects tracer

                              
{-#NOINLINE cosmoThoughtCoreTracer #-}
cosmoThoughtCoreTracer :: IO Tracer
cosmoThoughtCoreTracer = do
  provider <- getGlobalTracerProvider
  
  let instrumentationLib = InstrumentationLibrary "CosmoThought-core" (T.pack $ show version)
      tracer = makeTracer provider instrumentationLib tracerOptions
  pure tracer                              


       
