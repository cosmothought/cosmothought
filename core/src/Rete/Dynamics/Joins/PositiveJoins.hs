module Rete.Dynamics.Joins.PositiveJoins where
import Cosmolude
--import Polysemy.Reader
--import OpenTelemetry.Trace.Core
import Polysemy.Reader    
import qualified Data.Sequence as Seq
import qualified Data.HashMap.Strict as HM
import Rete.Dynamics.Joins.Common

import Rete.ReteTypes
import Core.CoreTypes
import Rete.Types.Tokens

import qualified Data.HashSet as HS
import Data.HashSet.Lens
import Data.Semigroup


{-# INLINEABLE performJoinConditionTest #-}
{-# SCC performJoinConditionTest #-}
performJoinConditionTest ::

  -- | Existing, prior tokens, that have already been searched from prior rete network propagations. For a binary left activation, this will be the amem token.
  PreviouslyConsideredTokenCombo metadata ->
  JoinCondition ->
  -- | The new token to test the rest of the tokens against
  PropagatingToken metadata ->
  JoinConditionSatisfaction metadata
performJoinConditionTest existingTokens joinCond propagatingToken
    | Just currentVal <- propagatingToken ^? propagatingMatch . matchedWME . field (joinCond ^. currentTokenField),
      (Just priorValue, priorToken) <-
          case joinCond ^. priorArgument of
            NodeArgument priorArg
                | Just priorTokenToGetFieldFrom <- existingTokens ^. tailMatches . at (priorArg^.tokenNode) ->
                            (priorTokenToGetFieldFrom ^? match . matchedWME . field (priorArg ^. tokenField), priorTokenToGetFieldFrom)
            ValueArgument v -> (Just v, propagatingToken^.rawToken)
            _ -> (Nothing, propagatingToken^.rawToken),
                                               
      JoinPredicateMet _ _ _ <- performJoinPredicate currentVal (joinCond ^. joinTest) priorValue = 
        JoinConditionMet priorToken joinCond propagatingToken

performJoinConditionTest _ _ _ = JoinConditionFails


--------------------------------------------------------------------------------
-- Join testing
--------------------------------------------------------------------------------

data JoinSatisfaction metadata
  = SomeConditionsFailed -- (WMEToken'  metadata) JoinCondition (WMEToken'  metadata)
  | JoinSucceeded (HashSet (WMEToken' metadata))
  deriving (Show, Generic)

deriving instance (Eq (WMEToken' metadata)) => Eq (JoinSatisfaction metadata)

deriving instance (Hashable (WMEToken' metadata)) => Hashable (JoinSatisfaction metadata)

instance (Hashable (WMEToken' metadata)) => Semigroup (JoinSatisfaction metadata) where
  {-# INLINE (<>) #-}
  SomeConditionsFailed <> _ = SomeConditionsFailed
  _ <> SomeConditionsFailed = SomeConditionsFailed
  (JoinSucceeded lhs) <> (JoinSucceeded rhs) = JoinSucceeded (lhs <> rhs)

instance (Hashable (WMEToken' metadata)) => Monoid (JoinSatisfaction metadata) where
  {-# INLINE mempty #-}
  mempty = JoinSucceeded (HS.empty)

{-# INLINE joinConditionSatisfactionToJoinSatisfaction #-}
joinConditionSatisfactionToJoinSatisfaction :: (Hashable (WMEToken' metadata)) => JoinConditionSatisfaction metadata -> JoinSatisfaction metadata
joinConditionSatisfactionToJoinSatisfaction JoinConditionFails = SomeConditionsFailed
joinConditionSatisfactionToJoinSatisfaction (JoinConditionMet lhs _ rhs) = JoinSucceeded (HS.singleton lhs <> HS.singleton (rhs ^. rawToken))

{-# INLINE evaluateJoinConditions #-}
evaluateJoinConditions :: Hashable (WMEToken' metadata) => HashSet (JoinConditionSatisfaction metadata) -> JoinSatisfaction metadata
evaluateJoinConditions conds = (conds & setmapped %~ joinConditionSatisfactionToJoinSatisfaction) ^. folded

{-# INLINE evaluateJoin #-}
evaluateJoin ::
  Hashable (WMEToken' metadata) =>
  HashSet JoinCondition ->
  -- | Existing, prior tokens. This is the token that ISN'T currently being propagated into the join node. For binary left activations, this will be the amem.
  PreviouslyConsideredTokenCombo metadata ->
  -- | The new, initiating token. For binary left activations, this will be the propagating token.
  PropagatingToken metadata ->
  JoinSatisfaction metadata
evaluateJoin conditions joinTok activatingTok = successfulJoinSatisfaction
  where
    evaluated = evaluateJoinConditions satisfactions
    satisfactions =
      conditions
        & setmapped
          %~ \condition -> 
                  performJoinConditionTest joinTok condition activatingTok
    successfulJoinSatisfaction = case evaluated of
      JoinSucceeded _ -> JoinSucceeded (HS.fromList (activatingTok ^. rawToken : (joinTok ^.. immediateParents . folded)))
      _ -> SomeConditionsFailed


{-# INLINEABLE satisfactions #-}
satisfactions :: (HasCallStack,
    Members '[OpenTelemetryContext, OpenTelemetrySpan,Log, IDGen, Transactional] r, Functor f, Renderable metadata) => PreviouslyConsideredTokenCombo metadata -> HashSet JoinCondition -> f (PropagatingToken metadata) -> Sem r (f (JoinSatisfaction metadata))
satisfactions previouslyConsideredTokens conditions propagatingTokens = do
  let results = propagatingTokens
                & mapped
                      %~ \propagatingToken ->
                          ( evaluateJoin
                            conditions
                            previouslyConsideredTokens
                            propagatingToken
                          )
  pure results
{-# INLINEABLE allTokenSets #-}
allTokenSets ::
  ( Renderable metadata,
    HasMemoryBlock' (WMEToken' metadata) metadata,
    HasCallStack,
    Members '[OpenTelemetryContext, OpenTelemetrySpan,Log,IDGen,  Transactional] r,
        HasJoinConditions (WMEToken' metadata) metadata pred
  ) =>
  (ReteNode metadata) ? pred ->
  ReteNode metadata ->
  PropagatingToken metadata ->
  Sem r (Seq (PreviouslyConsideredTokenCombo metadata))
allTokenSets currentNode nodeTokenCameFrom propagatedToken = do
  tokenSequences <-  do
    tokenSets <- fmap Seq.fromList $ (the currentNode) ^!! parentNodes . folded . ignoringLevel . filtered (\n -> (n ^. nodeIdentifier) /= (nodeTokenCameFrom ^. nodeIdentifier)) . memory . tokens
    traverse (perform toSeq) tokenSets
  tokCartesianProduct currentNode propagatedToken (tokenSequences)


{-# INLINEABLE allSats #-}
allSats ::
  ( Renderable metadata,
--    HasProductionRuleActivity agent metadata,
    HasTracer agent,
    Members '[OpenTelemetryContext, OpenTelemetrySpan,Log, IDGen, Transactional, Reader agent] r,
    HasMemoryBlock' (WMEToken' metadata) metadata,
    HasCallStack,
    HasJoinConditions (WMEToken' metadata) metadata pred
  ) =>
  HashSet JoinCondition ->
  (ReteNode metadata) ? pred  ->
  ReteNode metadata ->
  HashMap (WMEPatternMatch' metadata) (PropagatingToken metadata) ->
  Sem r (HashMap (WMEPatternMatch' metadata) (JoinPseudoToken metadata)) -- JoinSatisfaction  metadata))
allSats conditions currentNode nodeTokensCameFrom propagatingTokens = do
  sets <- mapM (allTokenSets currentNode nodeTokensCameFrom) propagatingTokens
  debug' $ "Sets of tokens: " <+> viaShow sets
  testedJoins <- mapM (\pctc -> satisfactions pctc conditions propagatingTokens) (fold sets)
  debug' $ "Tested join conditions: " <+> viaShow testedJoins
  pure
    ( HM.mapMaybe
        ( ( \sat -> case sat of
              SomeConditionsFailed -> Nothing
              JoinSucceeded successfulTokenCombo -> Just (JoinPseudoToken successfulTokenCombo)
          )
        )
        (fold testedJoins)
    )
