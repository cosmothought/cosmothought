{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE TemplateHaskell #-}



module Core.WME where
import Rete.Types.Tokens
import qualified Data.HashSet as HS
--import qualified Prelude (error)
import Core.Patterns.PatternSyntax
import CalamityCommands.Parser
import qualified Control.Monad.Trans.Class as T
import Core.CoreTypes
import Rete.ReteTypes
import Cosmolude hiding (note, lookupObjectIDByName)
import Polysemy.Reader (Reader)

import PSCM.Effects.WorkingMemory
import Core.Rule.Match
import Core.WorkingMemory.Reifiable

    



import qualified StmContainers.Set as STMS
import qualified System.IO.Unsafe as UNSAFE (unsafePerformIO)
-- cosmoThoughtBaseUUID :: UUID
-- cosmoThoughtBaseUUID = fromJust . UUID.fromString $ "ca25b858-d8a1-42a2-894e-03562c53a69d"

-- cosmoThoughtProductionMemoryUUID :: UUID
-- cosmoThoughtProductionMemoryUUID = fromJust . UUID.fromString $ "5bcd9152-1098-4bd8-ad0e-a43ef47a0f3f"

-- cosmoThoughtProductionMemory :: Identifier
-- cosmoThoughtProductionMemory = UniqueIdentifier cosmoThoughtProductionMemoryUUID


type RuleMatch = RuleMatch' WMEMetadata (WMEToken ? IsProductionToken)
type RuleActivation = RuleActivation' WMEMetadata (WMEToken ? IsProductionToken)
type RuleRetraction = RuleRetraction' WMEMetadata (WMEToken ? IsProductionToken)
type RuleResult = RuleResult' WMEMetadata (WMEToken ? IsProductionToken)



data WMEMetadata = WMEMetadata {
      -- | How the WME is supported
      _support' :: TVar WMESupport ,
      -- | Whether this is a state WME
      _isState' :: !IsState,
      -- | The state that created this WME
      _creatingState' :: !StateIdentifier,
      -- | The rete tokens which depend on this WME
      _tokens' :: STMS.Set (WMEToken),
      -- | The negative join results which belong to this WME
      _negativeJoinResults'' :: STMS.Set (NegativeJoinResult' WMEMetadata ),
      -- | The rete alpha nodes this WME inhabits
      _alphaNodes :: STMS.Set ((ReteNode WMEMetadata) ? IsAlphaNode WMEMetadata),
      -- | If this WME asserts a preference
      _WMPreference' :: Maybe (WMPreference ObjectIdentifier),
      -- | The  strength of this WME
      _truthVal :: TVar WMETruth}
                 deriving Pretty via ByRenderable WMEMetadata

type IsState = Bool

type IsOperatorProposal = Bool

instance Renderable (WMEMetadata) where
  render WMEMetadata {..} =
    align $ vsep ["State:" <+> (viaShow _isState'),
                  "Truth" <+> viaShow (UNSAFE.unsafePerformIO . doTransaction $ readT _truthVal),
                  "Preference:" <+> (viaShow _WMPreference'),
                  "Support:" <+> render ( UNSAFE.unsafePerformIO . doTransaction   $ readT _support'),
                  "Creating state:" <+> render _creatingState']

instance Show (WMEMetadata ) where
  show = show . render

{-# INLINE blankWMEMetadata #-}
blankWMEMetadata :: STM (WMEMetadata)
blankWMEMetadata = do
  toks <- STMS.new
  negJoinResults <- STMS.new
  nodes <- STMS.new
  support <- newTVar (RuleSupported HS.empty HS.empty)
  truthiness <- newTVar top  
  pure (WMEMetadata support False topLevelStateIdentifier toks negJoinResults nodes Nothing truthiness)
instance CreateBlankWMEMetadata WMEMetadata where
    createBlankWMEMetadata = blankWMEMetadata
{-instance Eq WMEMetadata where
  {-# INLINE (==) #-}
  lhs == rhs = _support lhs == _support rhs

instance Show WMEMetadata where
  {-# INLINE show #-}
  show WMEMetadata {..} = show _support

instance Ord WMEMetadata where
  {-# INLINE (<=) #-}
  lhs <= rhs = _support lhs <= _support rhs

instance Hashable WMEMetadata where
  {-# INLINE hashWithSalt #-}
  hashWithSalt salt WMEMetadata {..} = hashWithSalt salt _support
-}
type WME = WME' (WMEMetadata )


type WMEToken = WMEToken' (WMEMetadata)


type IsProductionToken = IsProductionToken' WMEMetadata




makeClassy ''WMEMetadata


instance ReifiableToWMEs WME WMEMetadata where
  {-# INLINE reifyToWMEs #-}
  reifyToWMEs wme = do
    truth <- readT (wme^.truthValue)
    pure [(wme,truth)]
           
instance HasWMESupport WMEMetadata where
    wmeSupport = support'

instance HasCreatingState WMEMetadata where
    {-# INLINE creatingState #-}
    creatingState = creatingState'
instance HasMaybeWMPreference (WMEMetadata ) ObjectIdentifier where
  {-# INLINE preference #-}
  preference = wMPreference'
                    
instance HasExtraObjectIdentifiers WMEMetadata where
    extraObjectIdentifiers :: Traversal' WMEMetadata ObjectIdentifier
    extraObjectIdentifiers = wMPreference' . _Just . extraObjectIdentifiers
instance WMETruthValue WMEMetadata  where
  {-# INLINE truthValue #-}
  truthValue = truthVal
instance WMETruthValue WME where
  {-# INLINE truthValue #-}
  truthValue = metadata.truthVal
instance HasWMEMetadata WME where
  {-# INLINE wMEMetadata #-}
  wMEMetadata = metadata
instance HasWMETokens (WMEMetadata ) (WMEMetadata) where
  {-# INLINE ownedTokens #-}
  ownedTokens :: Lens' (WMEMetadata) (STMS.Set (WMEToken))
  ownedTokens = tokens'
  {-# INLINE negativeJoinResults #-}
  negativeJoinResults = negativeJoinResults''
instance WMEIsState (WMEMetadata ) where
  {-# INLINE isState #-}
  isState = isState'

instance WMEInAlphaNodes (WMEToken ) (WMEMetadata) (WMEMetadata) where
  {-# INLINE inhabitedAlphaNodes #-}
  inhabitedAlphaNodes = alphaNodes





type WorkingMemory = WorkingMemory' WMEMetadata



parseWMEMetadata :: WhenParsing -> WMEParseInfo -> Parser r (WMEMetadata )
parseWMEMetadata w (WMEParseInfo {..}) = do
  md <- T.lift (embedSTM (blankWMEMetadata))
  pref <-  optional ((lexeme $ parseWMPreferenceWith (parseObjectIdentifier w)) <?> "WM preference")
  pure (md & isState .~ wmeIsState & preference .~ (pref^?_Just._Just))

parseWME :: (Members '[ObjectIdentityMap (WME), WorkingMemory] r) => Parser r (WMEParseInfo, WME )
parseWME = do
  do
    let metaParser = parseWMEMetadata ParsingWME
    (parseInfo@WMEParseInfo {}, parsedWME) <- parseWME' metaParser contentAddressableIdentifier 
    T.lift $ do
      pat <- wmeAsConstantPattern parsedWME
      existing <- findWMEsByPattern pat
      case existing^? _head of
        Just existingWME -> pure (parseInfo, existingWME)
        Nothing -> pure (parseInfo, parsedWME)
      

      
-- TODO make the metadata parser take extra args
-- parseWME' :: (Show a, WMEIsState a) => (Bool -> Parser a) -> Identifier -> WMEIdentifier -> Parser ((WMEParseInfo, WME' a))

instance (HasTracer agent, Members '[Reader agent, WorkingMemory, ObjectIdentityMap (WME), Transactional, IDGen] r) => ParameterParser (WME) c r where
  parameterDescription = "working memory element"
  parse =
    adaptToCommand
      ( do
          (_, res) <- parseWME
          pure res
      )

--instance HasWMESupport (WMEMetadata) where
--  {-# INLINE wMESupport #-}
--  wMESupport = support

