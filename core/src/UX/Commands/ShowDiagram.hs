module UX.Commands.ShowDiagram where
import Command
import Core.Rule
import Core.Rule.Database
import Core.WME
import Core.WorkingMemory (HasWorkingMemoryData)
import Cosmolude hiding (Any, Named, group)
import GHC.Exts hiding (Any)
import GHC.Float
import Polysemy
import Polysemy.Reader (Reader)
import Rete.Environment

import System.FilePath
import UX.CosmoScript.Syntax as AST
showDiagramWorkingMemory :: forall agent r. (HasTracer agent, HasWorkingMemoryData agent WMEMetadata,
    HasReteEnvironment agent WMEMetadata, Members '[Log, Reader agent, WorkingMemory, Transactional, OpenTelemetrySpan, OpenTelemetryContext, RuleDatabase, Embed IO] r) => Float -> FilePath -> Sem r CommandResults
showDiagramWorkingMemory size filePath = do
  emitWorkingMemoryGraph (float2Double size) filePath

showDiagramReteNodesForRule ::forall agent metadata r. (HasTracer agent, Renderable metadata, HasReteEnvironment agent metadata, Members '[Log, Transactional, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Embed IO, RuleDatabase, Resource] r) =>  Rule -> Float -> FilePath -> Sem r CommandResults
showDiagramReteNodesForRule rule size filePath = do
  emitReteNodeDiagramForRule rule (float2Double size) filePath

showDiagramReteNodes :: forall agent metadata r. (Renderable metadata, HasReteEnvironment agent metadata, Members [Log, Transactional, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Embed IO, RuleDatabase, Resource] r) => Float -> FilePath -> Sem r CommandResults
showDiagramReteNodes size filePath = do
  emitReteNodeDiagramForAllRules (float2Double size) filePath

processShowDiagramCommand ::          (HasTracer agent, HasWorkingMemoryData agent WMEMetadata, HasReteEnvironment agent WMEMetadata, Members '[Log, Transactional, Reader agent, OpenTelemetrySpan, OpenTelemetryContext, Embed IO, WorkingMemory, RuleDatabase, Resource] r) => AST.ShowCommand                -> Sem r CommandResults
processShowDiagramCommand (AST.ShowDiagramWorkingMemory size filePath) = showDiagramWorkingMemory size filePath
processShowDiagramCommand (AST.ShowDiagramReteNodesForRule rule size filePath) = showDiagramReteNodesForRule rule size filePath
processShowDiagramCommand (AST.ShowDiagramReteNodes size filePath) = showDiagramReteNodes size filePath

                                                                     
