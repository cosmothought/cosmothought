module UX.Commands.Add (processAddCommand) where
import Command
import Core.CoreTypes
import Core.Rule
import Cosmolude hiding (Any, Named, group)
import Data.List.NonEmpty (NonEmpty)
import PSCM
import Core.WorkingMemory.Reifiable 
import UX.CosmoScript.Syntax as AST
import Core.WME    
addRulesCommand :: (Member (ProductionSystem' metadata) r) => NonEmpty Rule -> Sem r CommandResults
addRulesCommand rules = do
  addRules rules
  pure $ resultingIn # [SideEffect] -- TODO: Include additions/removals somehow

addWMEsCommand :: (ReifiableToWMEs (WME' metadata) metadata, Member (ProductionSystem' metadata) r) => NonEmpty (WME' metadata) -> Sem r CommandResults
addWMEsCommand wmes = do
  addElements wmes
    -- firings <- addWMEs wmes
  pure $ resultingIn # [SideEffect] -- , Message (pretty wmes), Firings firings]

processAddCommand :: (Member (ProductionSystem' WMEMetadata) r) => AST.AddCommand -> Sem r CommandResults
processAddCommand (AST.AddRules rules) = addRulesCommand rules
processAddCommand (AST.AddWMEs wmes) = addWMEsCommand wmes
