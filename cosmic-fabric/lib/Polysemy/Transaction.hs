module Polysemy.Transaction where

-- import Polysemy.Bundle
import qualified Unsafe.Coerce  as UNSAFE (unsafeCoerce)
import Control.Applicative (pure)
import Control.Concurrent.STM
import GHC.Stack
import Control.Exception.Base
import Data.Bool
import Data.Hashable
import Data.Maybe
import DeferredFolds.UnfoldlM as DF
import Focus
--import ListT
import Polysemy
import Polysemy.Conc
import Polysemy.Embed

import qualified StmContainers.Map as STMM
import qualified StmContainers.Multimap as STMMM
import qualified StmContainers.Set as STMS
import System.IO (IO)
import Prelude (Eq, ($), (.), Int)

data Transactional' varType m a where
  NewT :: a -> Transactional' varType m (varType a)
  ReadT :: varType a -> Transactional' varType m a
  NewTM :: a -> Transactional' varType m (TMVar a)
  NewEmptyTM :: Transactional' varType m (TMVar a)
  TakeTM :: TMVar a -> Transactional' varType m a
  ReadTM :: TMVar a -> Transactional' varType m a
  TryReadTM :: TMVar a -> Transactional' varType m (Maybe a)
  WriteT :: a -> varType a -> Transactional' varType m ()
  PutTM :: a -> TMVar a -> Transactional' varType m ()
  ModifyT :: (a -> a) -> varType a -> Transactional' varType m ()
  IsNullSet :: STMS.Set x -> Transactional' varType m Bool
  NewSet :: Transactional' varType m (STMS.Set x)
  ResetSet :: STMS.Set x -> Transactional' varType m ()
  InsertInSet :: (Hashable x, Eq x) => x -> STMS.Set x -> Transactional' varType m ()
  LookupSet :: (Eq x, Hashable x) => x -> STMS.Set x -> Transactional' varType m Bool
  DeleteFromSet :: (Eq x, Hashable x) => x -> STMS.Set x -> Transactional' varType m ()
  FocusSet :: (Eq item, Hashable item) => Focus () STM result -> item -> STMS.Set item -> Transactional' varType m result
  --  ListSet :: (Eq item, Hashable item) => STMS.Set item -> Transactional' varType m (ListT STM item)
  --  UnfoldlMSet :: STMS.Set item -> Transactional' varType m (UnfoldlM STM item)
  IsNullMap :: STMM.Map k v -> Transactional' varType m Bool
  NewMap :: Transactional' varType m (STMM.Map k v)
  ResetMap :: STMM.Map k v -> Transactional' varType m ()
  InsertInMap :: (Eq k, Hashable k) => v -> k -> STMM.Map k v -> Transactional' varType m ()
  LookupMap :: (Eq k, Hashable k) => k -> STMM.Map k v -> Transactional' varType m (Maybe v)
  DeleteFromMap :: (Eq k, Hashable k) => k -> STMM.Map k v -> Transactional' varType m ()
  FocusMap :: (Eq key, Hashable key) => Focus value STM result -> key -> STMM.Map key value -> Transactional' varType m result
  NewMultimap :: Transactional' vartype m (STMMM.Multimap k v)
  ResetMultimap :: STMMM.Multimap k v -> Transactional' varType m ()
  InsertInMultimap :: (Eq key, Hashable key, Eq value, Hashable value) => value -> key -> STMMM.Multimap key value -> Transactional' varType m ()
  LookupMultimap :: (Eq k, Hashable k, Eq v, Hashable v) => v -> k -> STMMM.Multimap k v -> Transactional' varType m Bool
  LookupMultimapByKey :: (Eq k, Hashable k) => k -> STMMM.Multimap k v -> Transactional' varType m (Maybe (STMS.Set v))
  DeleteFromMultimap :: (Eq key, Hashable key, Eq value, Hashable value) => value -> key -> STMMM.Multimap key value -> Transactional' varType m ()
  DeleteFromMultimapByKey :: (Eq key, Hashable key) =>  key -> STMMM.Multimap key value -> Transactional' varType m ()
  MultimapToMapOfSets :: STMMM.Multimap k v -> Transactional' varType m (STMM.Map k (STMS.Set v))
  IsNullMultimap :: STMMM.Multimap k v -> Transactional' varType m Bool
  EmbedSTM :: STM a -> Transactional' varType m a
  FoldlM' :: (output -> input -> STM output) -> output -> UnfoldlM STM input -> Transactional' varType m output
  ThrowTransaction :: Exception e => e -> Transactional' varType m a
  RetryTransaction :: Transactional' varType m a
makeSem ''Transactional'

type Transactional = Transactional' TVar

type Transaction = Embed STM

type ScopedTransaction = Scoped_ Transaction

{-# INLINE transaction #-}
transaction :: (HasCallStack, Member ScopedTransaction r) => InterpreterFor Transaction r
transaction = scoped_

{-# INLINE interpretTransaction #-}
interpretTransaction :: (HasCallStack, Members '[Embed IO] r) => InterpreterFor (ScopedTransaction) r
interpretTransaction = runScopedAs pure (\_ -> runEmbedded atomically)

{-# INLINE interpretTransactional #-}
interpretTransactional :: (HasCallStack, Member (Embed IO) r) => InterpreterFor Transactional r
interpretTransactional = interpretTransaction . transaction . raiseUnder . interpretTransactional' . raiseUnder

{-# INLINE interpretTransactional' #-}
interpretTransactional' :: (HasCallStack, Member Transaction r) => InterpreterFor Transactional r
interpretTransactional' = interpret \case
  NewT a -> embed $ newTVar a
  NewTM a -> embed $ newTMVar a
  NewEmptyTM -> embed newEmptyTMVar
  ReadT var -> embed $ readTVar var
  TryReadTM var -> embed $ tryReadTMVar var
  TakeTM var -> embed $ takeTMVar var
  ReadTM var -> embed $ readTMVar var
  WriteT a var -> embed $ writeTVar var a
  PutTM a var -> embed $ putTMVar var a
  ModifyT f var -> embed $ modifyTVar var f
  IsNullSet s -> embed $ STMS.null s
  NewSet -> embed STMS.new
  ResetSet s -> embed $ STMS.reset s
  InsertInSet i s -> embed $ STMS.insert i s
  LookupSet i s -> embed $ STMS.lookup i s
  DeleteFromSet i s -> embed $ STMS.delete i s
  FocusSet f i s -> embed $ STMS.focus f i s
  IsNullMap m -> embed $ STMM.null m
  NewMap -> embed STMM.new
  ResetMap m -> embed $ STMM.reset m
  InsertInMap v k m -> embed $ STMM.insert v k m
  LookupMap k m -> embed $ STMM.lookup k m
  DeleteFromMap k m -> embed $ STMM.delete k m
  FocusMap f k m -> embed $ STMM.focus f k m
  NewMultimap -> embed $ STMMM.new
  ResetMultimap mm -> embed $ STMMM.reset mm
  InsertInMultimap v k mm -> embed $ STMMM.insert v k mm
  LookupMultimap v k mm -> embed $ STMMM.lookup v k mm
  LookupMultimapByKey k mm -> embed $ STMMM.lookupByKey k mm
  DeleteFromMultimap v k mm -> embed $ STMMM.delete v k mm
  DeleteFromMultimapByKey k mm -> embed $ STMMM.deleteByKey k mm                               
  EmbedSTM m -> embed m
  FoldlM' f o u -> embed $ DF.foldlM' f o u
  ThrowTransaction e -> embed $ throwSTM e
  RetryTransaction -> embed retry
  IsNullMultimap m -> embed $ STMMM.null m
  MultimapToMapOfSets m -> pure $ UNSAFE.unsafeCoerce m

{-# INLINE numberOfKeysInMultimap #-}                           
numberOfKeysInMultimap :: Member Transactional r  => STMMM.Multimap k v -> Sem r Int
numberOfKeysInMultimap m = do                                                                                      
                                unwrapped <- multimapToMapOfSets m
                                embedSTM $ STMM.size unwrapped

{-# INLINE doTransaction #-}
doTransaction :: HasCallStack =>  Sem '[Transactional, Embed IO, Final IO] a -> IO a
doTransaction = runFinal . embedToFinal . interpretTransactional -- . raiseUnder2

