{-# LANGUAGE CPP #-}
{-# LANGUAGE ImpredicativeTypes #-}

module Polysemy.Assert where

import Control.Exception (Exception, throwIO)
import Control.Monad
import Polysemy
import Prelude

-- import Polysemy.Error

import Data.String
import Data.Text
import Data.Typeable
import GHC.Stack
import OpenTelemetry.Trace.Core qualified as OT
import Polysemy.OpenTelemetry

data Assert m a where
  RawAssert :: (Show e, Typeable e) => Bool -> CallStack -> String -> e -> a -> Assert m a

makeSem ''Assert

-- type Assert = Asserts (forall e. (Show e, Typeable e) => e)

data AssertionFailedException e = AssertionFailedBlaming {message :: String, blaming :: e, assertionCallStack :: CallStack} deriving (Exception)

instance Show e => Show (AssertionFailedException e) where
  show (AssertionFailedBlaming msg b cs) = "Assertion failed: " ++ msg ++ "\n\nBlame assigned to: " ++ show b ++ "\n\n" ++ (prettyCallStack cs)

--  showsPrec _ (ErrorCallWithLocation err loc) =
--      showString err . showChar '\n' . showString loc
assertsToIOException :: (Members '[Embed IO] r) => OT.Tracer -> InterpreterFor (Assert) r
assertsToIOException tracer = interpret \a -> case a of
  RawAssert True _ _ _ a -> pure a
  RawAssert False stack userMessage exc _ -> do
    embed $ do
      provider <- OT.getGlobalTracerProvider
      _ <- OT.forceFlushTracerProvider provider Nothing
      OT.inSpan tracer (pack userMessage) OT.defaultSpanArguments do
        throwIO (AssertionFailedBlaming userMessage exc stack)

#ifndef debug
{-# RULES "replace asserts" forall a b c d value. send (RawAssert a b c d value) = pure value #-}
#endif
{-# INLINE assert #-}

-- | Nicely assert.
assert ::
  (Show e, Typeable e, HasCallStack, Members '[OpenTelemetrySpan, OpenTelemetryContext, Assert] r) =>
  -- | Condition
  Bool ->
  -- | Message on failure
  String ->
  -- | Value to blame
  e ->
  a ->
  Sem r a
assert cond msg e ~a = withFrozenCallStack $ do
  when (not cond) do
    setSpanStatus (OT.Error "Assertion failed")
    addEvent (fromString msg) [("assertion.blame", OT.toAttribute . pack . show $ e)]
  rawAssert cond GHC.Stack.callStack msg e a

{-# INLINE assertM #-}

-- | Nicely assert, monadically.
assertM ::
  (Show e, Typeable e, HasCallStack, Members '[OpenTelemetrySpan, OpenTelemetryContext, Assert] r) =>
  -- | Condition
  (Sem r Bool) ->
  -- | Message on failure
  String ->
  -- | Value to blame
  e ->
  a ->
  Sem r a
assertM cond msg e ~a =
  withFrozenCallStack $ do
    res <- cond
    assert res msg e a
