module Control.Effects.HypothesisTesting where

import Data.Foldable
import Polysemy
import Polysemy.State
import Polysemy.Trace

data HypothesisProvenance x = Guessed | Deduced x deriving (Eq, Show, Functor)

data HypothesisTesting hypo m a where
  AddHypotheses :: Traversable t => t hypo -> HypothesisTesting hypo m ()
  PopMostRecentHypotheses :: HypothesisTesting hypo m [hypo]
  AllHypotheses :: HypothesisTesting hypo m [[hypo]]

makeSem ''HypothesisTesting

{-# INLINE runHypothesisTestingAsList #-}
runHypothesisTestingAsList ::
  Sem (HypothesisTesting hypo : r) a -> Sem (State [[hypo]] : r) a
runHypothesisTestingAsList = reinterpret $ \case
  AddHypotheses h -> modify' ((toList h) :)
  AllHypotheses -> get
  PopMostRecentHypotheses -> do
    stack <- get
    case stack of
      [] -> return []
      r : rest -> do
        put rest
        return r

{-# INLINE traceHypotheses #-}
traceHypotheses ::
  forall hypo r a.
  (Show hypo, Members '[HypothesisTesting hypo, Trace] r) =>
  Sem r a ->
  Sem r a
traceHypotheses = intercept @(HypothesisTesting hypo) $ \case
  AddHypotheses t -> do
    trace ("Adding hypotheses: " ++ show (toList t))
    addHypotheses t
  PopMostRecentHypotheses -> do
    mostRecent <- popMostRecentHypotheses
    trace ("Popping hypotheses: " ++ show mostRecent)
    return mostRecent
  AllHypotheses -> allHypotheses
