module Control.Effects.Log
    ( Logger(..)
    )
where

import           Polysemy

data Logger logEntry m a where
