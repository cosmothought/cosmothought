module Control.Effects.SAT where
import           Polysemy

data GeneralSAT truth proof m a where
    ProveValidity ::GeneralSAT truth proof m (truth, proof)
    ProveSatisfiability ::GeneralSAT truth proof m (truth, proof)

makeSem ''GeneralSAT

data ValiditySAT truth m a where
    IsValid ::ValiditySAT truth m truth

makeSem ''ValiditySAT

data SATWithVariables vars m a where
    AddVariables ::Traversable t => t vars -> SATWithVariables vars m ()

makeSem ''SATWithVariables

data SimpleSAT truth terms m a where
    AddTerms ::Traversable t => t terms -> SimpleSAT truth terms m ()
    IsSatisfiable ::SimpleSAT truth terms m truth

makeSem ''SimpleSAT
