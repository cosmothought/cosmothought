module Control.Effects.Epoch where
import Polysemy
import Polysemy.Reader

data Epoch e m a where
    CurrentEpoch :: Epoch e m e
    NewEpoch :: (e -> e) -> m a -> Epoch e m a

makeSem ''Epoch