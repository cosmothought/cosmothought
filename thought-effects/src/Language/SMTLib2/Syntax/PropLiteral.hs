module Language.SMTLib2.Syntax.PropLiteral where

import Data.Data
import Data.Hashable
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Symbol
import Prettyprinter
  ( Pretty,
    pretty,
    (<+>),
  )
import qualified Prettyprinter as PP
import Text.Megaparsec

data PropLiteral
  = PositiveProp Symbol
  | NegativeProp Symbol
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty PropLiteral where
  pretty (PositiveProp s) = pretty s
  pretty (NegativeProp s) = PP.parens $ pp "not" <+> pretty s

instance Show PropLiteral where
  showsPrec _ = showsPrecDefault

parsePropLiteral :: Parser PropLiteral
parsePropLiteral =
  NegativeProp
    <$> parens (verbatim "not" *> parseSymbol)
    <|> PositiveProp
      <$> parseSymbol
