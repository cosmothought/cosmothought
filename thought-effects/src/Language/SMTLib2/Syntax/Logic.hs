module Language.SMTLib2.Syntax.Logic where

import Data.Data
import Data.Hashable
import Data.List.NonEmpty as NE
import Data.Text (Text)
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Attribute
import Language.SMTLib2.Syntax.Keyword
import Language.SMTLib2.Syntax.Symbol
import Prettyprinter
  ( Pretty,
    pretty,
    (<+>),
  )
import qualified Prettyprinter as PP
import Text.Megaparsec

data LogicAttribute
  = TheoriesAttribute (NonEmpty Symbol)
  | Language Text
  | Extensions Text
  | LogicValues Text
  | LogicNotes Text
  | CustomLogicAttribute Attribute
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty LogicAttribute where
  pretty (TheoriesAttribute syms) =
    pp ":theories" <+> prettyPrintSList (prettyPrintNonEmpty syms)
  pretty (Language t) = pp ":language" <+> PP.dquotes (pretty t)
  pretty (Extensions t) = pp ":extensions" <+> PP.dquotes (pretty t)
  pretty (LogicValues t) = pp ":values" <+> PP.dquotes (pretty t)
  pretty (LogicNotes t) = pp ":notes" <+> PP.dquotes (pretty t)
  pretty (CustomLogicAttribute attr) = pretty attr

instance Show LogicAttribute where
  showsPrec _ = showsPrecDefault

parseLogicAttribute :: Parser LogicAttribute
parseLogicAttribute =
  ( do
      kw <- parseKeyword
      case kw of
        "theories" ->
          TheoriesAttribute <$> (parens $ parseNonEmpty parseSymbol)
        "language" -> Language <$> parseString
        "extensions" -> Extensions <$> parseString
        "values" -> LogicValues <$> parseString
        "notes" -> LogicNotes <$> parseString
        _ -> empty
  )
    <|> CustomLogicAttribute
      <$> parseAttribute

data Logic
  = Logic Symbol (NonEmpty LogicAttribute)
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty Logic where
  pretty (Logic s attrs) =
    prettyPrintSList' "logic" ((pretty s) : prettyPrintNonEmpty attrs)

instance Show Logic where
  showsPrec _ = showsPrecDefault

parseLogic :: Parser Logic
parseLogic =
  parens $ Logic <$> parseSymbol <*> parseNonEmpty parseLogicAttribute
