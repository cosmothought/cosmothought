module Language.SMTLib2.Syntax.Definition where

import Data.Data
import Data.Hashable
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Sort
import Language.SMTLib2.Syntax.Symbol
import Language.SMTLib2.Syntax.Term
import Prettyprinter
  ( Pretty,
    pretty,
    (<+>),
  )
import Text.Megaparsec

data FunctionDef
  = FunctionDef Symbol [SortedVar] Sort Term
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty FunctionDef where
  pretty (FunctionDef sym vars sort term) =
    pretty sym
      <+> prettyPrintSList (fmap pretty vars)
      <+> pretty sort
      <+> pretty term

instance Show FunctionDef where
  showsPrec _ = showsPrecDefault

parseFunctionDef :: Parser FunctionDef
parseFunctionDef =
  FunctionDef
    <$> parseSymbol
    <*> parens (many parseSortedVar)
    <*> parseSort
    <*> parseTerm
