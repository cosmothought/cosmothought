module Language.SMTLib2.Syntax.InfoFlag where

import Data.Data
import Data.Hashable
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Keyword
import Prettyprinter
  ( Pretty,
    pretty,
  )

data InfoFlag
  = AllStatistics
  | AssertionStackLevels
  | Authors
  | ErrorBehaviour
  | Name
  | ReasonUnknown
  | Version
  | CustomInfoFlag Keyword
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty InfoFlag where
  pretty AllStatistics = pp ":all-statistics"
  pretty AssertionStackLevels = pp ":assertion-stack-levels"
  pretty Authors = pp ":authors"
  pretty ErrorBehaviour = pp ":error-behavior" -- yankee go home
  pretty Name = pp ":name"
  pretty ReasonUnknown = pp ":reason-unknown"
  pretty Version = pp ":version"
  pretty (CustomInfoFlag kw) = pretty kw

instance Show InfoFlag where
  showsPrec _ = showsPrecDefault

parseInfoFlag :: Parser InfoFlag
parseInfoFlag = do
  kw <- parseKeyword
  pure $ case kw of
    "all-statistics" -> AllStatistics
    "assertion-stack-levels" -> AssertionStackLevels
    "authors" -> Authors
    "error-behavior" -> ErrorBehaviour
    "error-behaviour" -> ErrorBehaviour
    "name" -> Name
    "reason-unknown" -> ReasonUnknown
    "version" -> Version
    _ -> CustomInfoFlag kw
