module Language.SMTLib2.Syntax.Identifier where

import Data.Data
import Data.Hashable
import Data.List.NonEmpty (NonEmpty)
import Data.Text (Text)
import Data.Typeable
import GHC.Generics
import Language.SMTLib2.Syntax.ASTUtils
import Language.SMTLib2.Syntax.Constants
import Language.SMTLib2.Syntax.Symbol
import Prettyprinter
  ( Pretty,
    pretty,
  )
import Text.Megaparsec

data Index
  = NumeralIndex Numeral
  | SymbolIndex Symbol
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty Index where
  pretty (NumeralIndex n) = pretty n
  pretty (SymbolIndex s) = pretty s

instance Show Index where
  showsPrec _ = showsPrecDefault

parseIndex :: Parser Index
parseIndex =
  (NumeralIndex <$> parseNumeral <?> "numeral")
    <|> (SymbolIndex <$> parseSymbol <?> "symbol")

data Identifier
  = SymbolIdentifier Symbol
  | IndexedIdentifier Symbol (NonEmpty Index)
  deriving (Eq, Ord, Generic, Hashable, Data, Typeable)

instance Pretty Identifier where
  pretty (SymbolIdentifier s) = pretty s
  pretty (IndexedIdentifier s indices) =
    prettyPrintSList $
      (pretty '_')
        : (pretty s)
        : (prettyPrintNonEmpty indices)

instance Show Identifier where
  showsPrec _ = showsPrecDefault

identifierHead :: Identifier -> Text
identifierHead (SymbolIdentifier s) = symbolText s
identifierHead (IndexedIdentifier s _) = symbolText s

parseIdentifier :: Parser Identifier
parseIdentifier =
  SymbolIdentifier
    <$> parseSymbol
    <|> IndexedIdentifier
      <$> parseSymbol
      <*> parseNonEmpty parseIndex
