module Logic.Symbolic.SAT.Representations.Expression
(
    Expr (..),
    IsAtomic, IsTop, IsBottom, IsAnd, IsOr, IsNot, IsImplication, IsEquivalence, IsLiteral, IsPositiveLiteral, IsNegativeLiteral, IsConjunctOf, IsDisjunctOf, IsTruthValue, literalSenseLemma, conjunctOfPropIsAnd, disjunctOfPropIsOr,bottomIsTruthValue,topIsTruthValue, IsDegenerateTruthValue, distinctOrAnd,
    ExprCase (..),
    pattern Var,
    Prop, Valuation,
    classifyExpression',
    conjuncts, disjuncts,
    negativeLitIsLit,
    positiveLitIsLit,
    toAtom,
    variables,
    negateLiterals,
    mapLiterals,
    varifyExpr,
    isNegativeLiteral,
    isAtomic,
    isLiteral,
    isPositiveLiteral,
    posLitLemma,
    negLitLemma,
    isOr,
    toAtomic,
    isAnd,
    (/\), (\/),
    lookupVar,
    applySubstitution,
    positiveLiteral,
    negativeLiteral,
    partitionLiterals
) where
import Control.Lens hiding ((...))
import Data.Hashable
import Data.Data
import GHC.Generics
import qualified Data.HashSet as HS
import Logic.Symbolic.SAT.Representations.Types
import Polysemy
import Control.Effects.Substitution
import Polysemy.Error hiding (note)
import qualified Data.HashMap.Strict as HM
import Polysemy.Fresh
import GDP
import Data.List (partition)
import Data.Bifunctor
import Data.Discrimination.Grouping
data Sort = Boolean
data Predicate v = Predicate v [Atom v]

{-@autosize Expr@-}
data Expr v = Atomic (Atom v)
    | Top
    | Bottom
    | And [Expr v]
    | Or [Expr v]
    -- | And' (HS.HashSet (Expr v))
    -- | Or' (HS.HashSet (Expr v))
    | (:=>:) (Expr v) (Expr v)
    | Not (Expr v)
    | (:<=>:) (Expr v)  (Expr v)
    deriving stock (Eq, Show, Read, Data, Typeable, Generic, Generic1, Functor, Foldable, Traversable)
instance (Grouping v) => Grouping (Expr v)


newtype IsAtomic prop = MkIsAtomic Defn
type role IsAtomic nominal
instance Injective IsAtomic
newtype IsTop prop = MkIsTop Defn
type role IsTop nominal
instance Injective IsTop
newtype IsBottom prop = MkIsBottom Defn
type role IsBottom nominal
instance Injective IsBottom
newtype IsAnd prop = MkIsAnd Defn
type role IsAnd nominal
instance Injective IsAnd
newtype IsOr prop = MkIsOr Defn
type role IsOr nominal
instance Injective IsOr
newtype IsImplication prop = MkIsImplication Defn
type role IsImplication nominal
instance Injective IsImplication
newtype IsNot prop = MkIsNot Defn
type role IsNot nominal
instance Injective IsNot
newtype IsEquivalence prop = MkIsEquiv Defn
type role IsEquivalence nominal
instance Injective IsEquivalence
newtype IsLiteral prop = MkIsLiteral Defn
type role IsLiteral nominal
newtype IsPositiveLiteral prop = MkIsPositiveLiteral Defn
type role IsPositiveLiteral nominal
instance Injective IsPositiveLiteral
newtype IsNegativeLiteral prop = MkIsNegativeLiteral Defn
type role IsNegativeLiteral nominal
instance Injective IsNegativeLiteral
newtype IsConjunctOf parent name = MkIsConjunctOf Defn
type role IsConjunctOf nominal nominal

instance Argument (IsConjunctOf (parent) child) 0 where
    type GetArg (IsConjunctOf parent child) 0 = parent
    type SetArg (IsConjunctOf parent child) 0 p' = IsConjunctOf p' child
instance Argument (IsConjunctOf parent (child )) 1 where
    type GetArg (IsConjunctOf parent child) 1 = child
    type SetArg (IsConjunctOf parent child) 1 c' = IsConjunctOf parent c'

newtype IsDisjunctOf parent name = MkIsDisjunctOfOf Defn
type role IsDisjunctOf nominal nominal


instance Argument (IsDisjunctOf (parent ) child) 0 where
    type GetArg (IsDisjunctOf parent child) 0 = parent
    type SetArg (IsDisjunctOf parent child) 0 p' = IsDisjunctOf p' child
instance Argument (IsDisjunctOf parent (child )) 1 where
    type GetArg (IsDisjunctOf parent child) 1 = child
    type SetArg (IsDisjunctOf parent child) 1 c' = IsDisjunctOf parent c'

instance Antisymmetric IsConjunctOf
instance Antisymmetric IsDisjunctOf
instance Irreflexive IsConjunctOf
instance Irreflexive IsDisjunctOf
positiveLitIsLit :: Proof (IsPositiveLiteral prop) -> Proof(IsLiteral prop)
positiveLitIsLit _ = axiom
negativeLitIsLit :: Proof (IsNegativeLiteral prop) -> Proof (IsLiteral prop)
negativeLitIsLit _ = axiom
literalSenseLemma :: Proof (IsPositiveLiteral prop ∨ IsNegativeLiteral prop --> IsLiteral prop)
literalSenseLemma = axiom

disjunctOfPropIsOr :: Proof (IsDisjunctOf prop child --> IsOr prop)
disjunctOfPropIsOr = axiom

conjunctOfPropIsAnd :: Proof (IsConjunctOf prop child --> IsAnd prop)
conjunctOfPropIsAnd = axiom

newtype IsTruthValue prop = MkIsTruthValue Defn
type role IsTruthValue nominal

newtype IsDegenerateTruthValue prop = MkIsDegenerateTruthValue Defn
type role IsDegenerateTruthValue nominal



topIsTruthValue :: Proof (IsTop prop --> IsTruthValue prop)
topIsTruthValue = axiom

bottomIsTruthValue :: Proof (IsBottom prop --> IsTruthValue prop)
bottomIsTruthValue = axiom

---------------------
-- Injectivity

distinctOrAnd :: Proof (IsOr prop) -> Proof (IsAnd prop) -> Proof FALSE
distinctOrAnd _ _ = axiom



data ExprCase v prop where
    AnAtom :: (Fact (IsAtomic prop), Fact (IsLiteral prop), Fact (IsPositiveLiteral prop)) => ExprCase v prop
    ATop :: (Fact (IsTop prop), Fact (IsTruthValue prop)) => ExprCase v prop
    ABottom :: (Fact (IsBottom prop), Fact (IsTruthValue prop)) => ExprCase v prop
    AnAnd :: Fact (IsAnd prop) => ExprCase v prop
    AnOr :: Fact (IsOr prop) => ExprCase v prop
    AnImplication :: Fact (IsImplication prop) => ExprCase v prop
    AnEquivalence :: Fact (IsEquivalence prop) => ExprCase v prop
    ANot :: Fact (IsNot prop) => ExprCase v prop

deriving instance Eq (ExprCase v prop)
deriving instance Show (ExprCase v prop)


{-#INLINE classifyExpression' #-}
classifyExpression' :: forall v prop . ((Expr v) ~~ prop) -> ExprCase v prop
classifyExpression' prop = case the prop of
    Atomic _ -> note (axiom :: Proof (IsAtomic prop)) $ note (axiom :: Proof (IsLiteral prop)) $ note (axiom :: Proof (IsPositiveLiteral prop)) AnAtom
    Top -> note (axiom :: Proof (IsTop prop)) $ note (axiom :: Proof (IsTruthValue prop)) ATop
    Bottom -> note (axiom :: Proof (IsBottom prop)) $ note (axiom :: Proof (IsTruthValue prop)) ABottom
    And _ -> note (axiom :: Proof (IsAnd prop)) AnAnd
    Or _ -> note (axiom :: Proof (IsOr prop)) AnOr
    _ :=>: _ -> note (axiom :: Proof (IsImplication prop)) AnImplication
    _ :<=>: _ -> note (axiom :: Proof (IsEquivalence prop)) AnEquivalence
    Not _ -> note (axiom :: Proof (IsNot prop)) ANot

{-#INLINE conjuncts #-}
conjuncts :: Fact (IsAnd prop) => (Expr v) ~~ prop -> [(Expr v) ? IsConjunctOf  prop]
conjuncts (The (And as)) = fmap assert as

{-#INLINE disjuncts #-}
disjuncts :: Fact (IsOr prop) => (Expr v) ~~ prop -> [(Expr v) ? IsDisjunctOf  prop]
disjuncts (The (Or os)) = fmap assert os

{-# INLINE toAtomic #-}
toAtomic :: Literal m v -> (Expr v) ? IsLiteral
toAtomic (PositiveLiteral a) = assert $ Atomic a
toAtomic (NegativeLiteral a) = assert $ Not (Atomic a)

--pattern And x <- ((\(And' as) -> HS.fromList as) -> x)  where
--    And x = And' (HS.toList x)
{-instance Show v => Show (Expr v) where
    show Top = "⊤"
    show Bottom = "⊥"
    show (And as) = "⋀(" ++ (intercalate ", " (fmap show as)) ++ ")"
    show (Or os) = "⋁(" ++ (intercalate ", " (fmap show os)) ++ ")"
    show (a :=>: b) = (show a) ++ "→" ++ (show b)
    show (Not a) = '¬': show a
    show (a :<=>: b) = '(':(show a) ++ "↔" ++ (show b) ++ ")"
    show (Atomic a) = show a
-}
{- measure lsum :: [Nat] -> Nat}
lsum :: [Int] -> Int
lsum [] = 0
lsum (a:as) = a + (lsum as)

- measure exprSize :: a:(Expr v) -> Nat  }
exprSize :: Expr v -> Int
exprSize Top = 1
exprSize Bottom = 1
exprSize (And as) = 1 + lsum (fmap exprSize as)
exprSize (Or os) = 1 + lsum (fmap exprSize os)
exprSize (Not v) = 1 + exprSize v
exprSize (a :=>: b) = 1 + exprSize a + exprSize b
exprSize (a :<=>: b) = 1 + exprSize a + exprSize b
exprSize (Atomic _) = 1

-}
instance (Hashable v) => Hashable (Expr v)
instance (Data v) => Plated (Expr v)

pattern Var :: forall v. v -> Expr v
pattern Var v = Atomic (Variable v)

{-@toAtom :: n:Expr v -> {x : Atom v | isAtomic n} @-}
{-# INLINE toAtom #-}
toAtom :: Fact (IsLiteral prop) => (Expr v) ~~ prop  -> Atom v
toAtom (The (Atomic atom         )) = atom
toAtom (The (Not    (Atomic atom))) = atom


{-@ autosize [] @-}
{-# INLINE variables #-}
-- FIXME This should be called "atoms" or something
variables :: (Eq a, Hashable a) => Fold (Expr a) a
variables = folding (foldr HS.insert HS.empty)

{-#INLINE negateLiterals #-}
negateLiterals :: Expr v -> Expr v
negateLiterals = mapLiterals Not

{-#INLINABLE mapLiterals #-}
{-@ lazy mapLiterals @-}
{-@ mapLiterals :: f:(Expr v -> Expr v) -> a:(Expr v) -> Expr v / [autolen a] @-}
mapLiterals :: (Expr v -> Expr v) -> Expr v -> Expr v
mapLiterals f a@(Atomic _) = f a
mapLiterals f a@(Not(Atomic _)) = f a
mapLiterals f (And as) = And (fmap (mapLiterals f) as)
mapLiterals f (Or as) = Or (fmap (mapLiterals f) as)
mapLiterals f (Not p) = Not (mapLiterals f p)
mapLiterals f (a :=>: b) = (mapLiterals f a) :=>: (mapLiterals f b)
mapLiterals f (a :<=>: b) = (mapLiterals f a) :<=>: (mapLiterals f b)
mapLiterals _ Top = Top
mapLiterals _ Bottom = Bottom
type Prop = Expr Variable
type Valuation = Substitution Variable Prop

{-#INLINE varifyExpr#-}
varifyExpr :: Member (Fresh Variable) r => Expr String -> Sem r (Prop)
varifyExpr term = do
    let vars = term ^.. variables
    map <- stringVarsToVariables vars
    return (fmap (map HM.!) term)
{-@ measure isNegativeLiteral :: Expr v -> Bool @-}

{-# INLINE isNegativeLiteral #-}
isNegativeLiteral :: Expr v -> Bool
isNegativeLiteral (Not (Atomic ( _))) = True
isNegativeLiteral _                = False

{-@ measure isAtomic @-}

{-#INLINE isAtomic #-}
isAtomic :: Expr v -> Bool
isAtomic (Atomic _) = True
isAtomic _          = False

{-#INLINE isLiteral #-}
{-@ measure isLiteral :: Expr v -> Bool @-}
isLiteral :: Expr v -> Bool
isLiteral (Atomic _) = True
isLiteral (Not (Atomic _)) = True
isLiteral _ = False

{-@ measure isPositiveLiteral :: Expr v -> Bool @-}
{-# INLINE isPositiveLiteral #-}
isPositiveLiteral :: Expr v -> Bool
isPositiveLiteral (Atomic _) = True
isPositiveLiteral _ = False

{-# INLINE isPositiveLiteral' #-}
isPositiveLiteral' :: (Expr v) ~~ prop -> Bool
isPositiveLiteral' (The (Atomic _)) = True
isPositiveLiteral' _                     = False

{-#INLINE partitionLiterals#-}
partitionLiterals :: [(Expr v) ? IsLiteral] -> ([(Expr v) ? IsPositiveLiteral], [(Expr v) ? IsNegativeLiteral])
partitionLiterals p = bimap (fmap assert) (fmap assert) $ partition isPositiveLiteral (fmap the p)

posLitLemma :: Proof (IsAtomic prop --> IsPositiveLiteral prop)
posLitLemma = axiom

negLitLemma :: Proof (IsNot (IsAtomic prop) --> IsNegativeLiteral prop)
negLitLemma = axiom

{-@ measure isOr @-}
{-# INLINE isOr #-}
isOr :: Expr v -> Bool
isOr (Or _) = True
isOr _ = False

{-@ measure isAnd @-}
{-# INLINE isAnd #-}
isAnd :: Expr v -> Bool
isAnd (And _) = True
isAnd _ = False

{-#INLINE (/\) #-}
infixl 5 /\
(/\) :: Expr v -> Expr v -> Expr v
a       /\ (And b) = And (a : b)
(And a) /\ b       = And (b : a)
a       /\ b       = And [a, b]
{-# INLINE (\/) #-}
infixl 4 \/
(\/) :: Expr v -> Expr v -> Expr v
a      \/ (Or b) = Or (a : b)
(Or a) \/ b      = Or (b : a)
a      \/ b      = Or [a, b]

{-# INLINE lookupVar #-}
lookupVar
    :: Members '[Valuation, Error UnboundVariable] r => Variable -> Sem r Prop
lookupVar a = do
    result <- lookupVariable a
    case result of
        Just x  -> return x
        Nothing -> throw (UnboundVariable a)
{-# INLINE applySubstitution #-}
applySubstitution :: Members '[Valuation] r => Prop -> Sem r Prop
applySubstitution t = flip rewriteM t $ \case
    Var x -> lookupVariable x
    _     -> return Nothing


{-# INLINE positiveLiteral #-}
positiveLiteral :: Fact (IsPositiveLiteral prop) => (Expr v) ~~ prop -> Literal Positive v
positiveLiteral (The (Atomic v)) =  PositiveLiteral v

{-#INLINE negativeLiteral #-}
negativeLiteral :: Fact (IsNegativeLiteral prop) => (Expr v) ~~ prop -> Literal Negative v
negativeLiteral (The (Not (Atomic v))) = NegativeLiteral v
