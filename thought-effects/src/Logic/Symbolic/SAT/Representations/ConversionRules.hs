{-# LANGUAGE ParallelListComp #-}
module Logic.Symbolic.SAT.Representations.ConversionRules where
import           Polysemy
import           Control.Lens            hiding ( (...) )
import           Logic.Symbolic.SAT.Types
import           Logic.Symbolic.SAT.Representations.Types
import           Logic.Symbolic.SAT.Representations.Expression
import           Data.Foldable                 as DF
import           Data.Witherable               as DW
import qualified Data.HashSet                  as HS
import           Data.Hashable
import           Data.Either
import           Control.Effects.Commentary
import           Control.Effects.Substitution
import           Data.Maybe                     ( fromJust )
import           Control.Monad
import           Control.Applicative
import           Data.List                      ( partition )
import           Data.Data
import           GHC.Exts                      ( SPEC(..) )
import           Debug.Trace
import           GDP
import           Data.Discrimination.Grouping

newtype IsCNF prop = MkIsCNF Defn

type role IsCNF nominal

{-# INLINE trivialSimplifications #-}
trivialSimplifications
    :: (Grouping Variable, Members '[Valuation, Commentary] r)
    => Prop
    -> Sem r (Prop)
trivialSimplifications prop = do
    --prop' <- rewriteM (combineRewrites
     --   [equivalenceDefinitionRules, trivialSimplificationRules]) prop
    rewriteM (combineRewrites [trivialSimplificationRules, varLookupRule]) prop--'
{-# INLINE varLookupRule #-}
varLookupRule :: Member Valuation r => Prop -> Sem r (Maybe Prop)
varLookupRule prop = case prop of
    Var x -> lookupVariable x
    _     -> return Nothing

{-# INLINE combinations #-}
combinations :: [[a]] -> [[a]]
combinations = combinations' SPEC  where
    combinations' !sPEC [] = [[]]
    combinations' !sPEC (xs : xss) =
        [ x : xs' | x <- xs, xs' <- combinations' sPEC xss ]

data DistributiveLawsDirection = ToCNF | ToDNF deriving Eq

{-# INLINABLE distributiveLaws #-}
distributiveLaws
    :: DistributiveLawsDirection -> Expr v -> Sem r (Maybe (Expr v))
distributiveLaws direction prop = do
    -- TODO: Optimise
    case prop of
        And as -> do
            let (ors, rest) = partition isOr as
            if direction == ToCNF && DF.all isLiteral rest
                then return Nothing
                else case ors of
                    [] -> return Nothing
                    _  -> do
                        let cat = combinations (fmap (\(Or o) -> o) ors)

                        return $ Just $ Or [ And (c ++ rest) | c <- cat ]
        Or os -> do
            let (ands, rest) = partition isAnd os
            if direction == ToDNF && DF.all isLiteral rest
                then return Nothing
                else case ands of
                    [] -> return Nothing
                    _  -> do
                        let cat = combinations (fmap (\(And o) -> o) ands)
                        return $ Just $ And [ Or (c ++ rest) | c <- cat ]
        _ -> return Nothing
{-# INLINE cnfRules #-}
cnfRules
    :: (Eq v, Grouping v, Hashable v, Member Commentary r)
    => (Expr v)
    -> Sem r (Maybe ((Expr v)))
cnfRules = combineRewrites
    [ distributiveLaws ToCNF
    , implicationRemovalRules
    , doubleNegationRule
    , dualiseConnectiveRules
    , equivalenceRemovalRules
    , trivialSimplificationRules
    ]
{-# INLINE trivialSimplificationRules #-}
trivialSimplificationRules
    :: (Eq v, Grouping v, Hashable v, Member Commentary r)
    => Expr v
    -> Sem r (Maybe (Expr v))
trivialSimplificationRules = combineRewrites
    [{-trivialRedundantRules,-}
      basicIdentities
    , flattenRules
    , trivialRedundantRules
    ]
{-# INLINABLE basicIdentities #-}
basicIdentities
    :: (Eq v, Member Commentary r) => Expr v -> Sem r (Maybe (Expr v))
basicIdentities prop = do
    explanation "This function applies trivial simplifications to the input."
    -- TODO: Rewrite these as optics, particularly the folds
    case prop of
        And []                    -> return (Just Top)
        And [a]                   -> return (Just a)
        And as | Bottom `elem` as -> return (Just Bottom)
        And as | Top `elem` as    -> return (Just (And (DW.filter (/= Top) as)))
        Or []                     -> return (Just Bottom)
        Or [a]                    -> return (Just a)
        Or as | Top `elem` as     -> return (Just Top)
        Or as | Bottom `elem` as ->
            return (Just (Or (DW.filter (/= Bottom) as)))
        Not Top     -> return (Just Bottom)
        Not Bottom  -> return (Just Top)
        Not (Not p) -> do
            assumption "Classical logic"
            return (Just p)
        _      :=>: Top -> return (Just Top)
        Top    :=>: p   -> return (Just p)
        Bottom :=>: _   -> do
            assumption "Classical logic"
            return (Just Top)
        a :=>: Bottom       -> return (Just (Not a))
        a :=>: b | a == b   -> return (Just Top)
        a :<=>: b | a == b  -> return (Just Top)
        Top    :<=>: b      -> return (Just b)
        a      :<=>: Top    -> return (Just a)
        Bottom :<=>: b      -> return (Just (Not b))
        a      :<=>: Bottom -> return (Just (Not a))
        _                   -> return Nothing

{-# INLINE implicationRemovalRules #-}
implicationRemovalRules
    :: Member Commentary r => Expr v -> Sem r (Maybe (Expr v))
implicationRemovalRules prop = do
    case prop of
        a :=>: b -> do
            assumption "Classical logic"
            return (Just (Or [(Not a), b]))
        _ -> return Nothing



{-# INLINE equivalenceDefinitionRules #-}
equivalenceDefinitionRules
    :: (Eq v, Member (Substitution v (Expr v)) r)
    => Expr v
    -> Sem r (Maybe (Expr v))
equivalenceDefinitionRules prop = do
    case prop of
        (Var _     ) :<=>: (Var _)    -> return Nothing
        (a'@(Var _)) :<=>: b          -> checkDefs a' b
        b            :<=>: a'@(Var _) -> checkDefs a' b
        _                             -> return Nothing
{-# INLINE checkDefs #-}
checkDefs
    :: (Eq v, Member (Substitution v (Expr v)) r)
    => Expr v
    -> Expr v
    -> Sem r (Maybe (Expr v))
checkDefs a'@(Var a) b = do
    def <- lookupVariable a
    case def of
        Nothing -> do
            addSubstitutions [(a, b)]
            return $ Just a'
        Just definens -> do
            defined <- fmap fromJust $ matchesDefinition definens

            if (defined == a)
                then return Nothing--(Just Top)
                else do
                    return
                        $ Just
                              (And
                                  [ (Var a) :=>: Var defined
                                  , (Var defined) :=>: (Var a)
                                  ]
                              )
{-# INLINE equivalenceRemovalRules #-}
equivalenceRemovalRules :: Expr v -> Sem r (Maybe (Expr v))
equivalenceRemovalRules prop = do
    case prop of
        a :<=>: b -> return $ Just (And [a :=>: b, b :=>: a])
        _         -> return Nothing
{-# INLINE nnfRules #-}
nnfRules
    :: (Eq v, Grouping v, Hashable v, Member Commentary r)
    => (Expr v)
    -> Sem r (Maybe ((Expr v)))
nnfRules = combineRewrites
    [ trivialSimplificationRules
    , dualiseConnectiveRules
    , implicationRemovalRules
    , equivalenceRemovalRules
    ]

{-# INLINE combineRewrites #-}
combineRewrites :: [a -> Sem r (Maybe (a))] -> a -> Sem r (Maybe (a))
combineRewrites list prop = do
    results <- forM list (\rule -> rule prop)
    return (msum results)

{-# INLINE toNNF #-}
toNNF
    :: (Eq v, Grouping v, Hashable v, Data (v), Member Commentary r)
    => (Expr v)
    -> Sem r (Expr v)
toNNF = (rewriteM nnfRules) -- . negateLiterals

{-# INLINE nnf #-}
nnf :: (Eq v, Grouping v, Hashable v, Data v) => Expr v -> Expr v
nnf prop = run $ ignoreCommentary $ rewriteM nnfRules prop

{-# INLINE toCNF #-}
toCNF
    :: (Eq v, Grouping v, Hashable v, Data (v), Member Commentary r)
    => (Expr v)
    -> Sem r (Expr v ? IsCNF)
toCNF p = fmap assert (rewriteM cnfRules p) -- . negateLiterals

{-# INLINE doubleNegationRule #-}
doubleNegationRule :: Member Commentary r => Expr v -> Sem r (Maybe (Expr v))
doubleNegationRule (Not (Not p)) = do
    assumption "Classical logic"
    pure (Just p)
doubleNegationRule _ = pure Nothing

{-# INLINE dualiseConnectiveRules #-}
dualiseConnectiveRules :: Expr v -> Sem r (Maybe (Expr v))
dualiseConnectiveRules prop = do
    case prop of
        Not (And as) -> return $ Just (Or (fmap Not as))
        Not (Or  as) -> return $ Just (And (fmap Not as))
        _            -> return Nothing

{-# INLINE getAndConjuncts #-}
getAndConjuncts :: Expr v -> Either (Expr v) [Expr v]
getAndConjuncts (And a) = Right a
getAndConjuncts a       = Left a

{-# INLINE getOrDisjuncts #-}
getOrDisjuncts :: Expr v -> Either (Expr v) [Expr v]
getOrDisjuncts (Or a) = Right a
getOrDisjuncts a      = Left a

{-# INLINE assimilateAnds #-}
assimilateAnds :: Expr v -> Maybe (Expr v)
assimilateAnds (And conjuncts) = if DF.null concatted
    then Nothing
    -- TODO: Rewrite as optics
    else Just (And (normals <> concatted))  where
    (normals, nested) = partitionEithers $ fmap getAndConjuncts conjuncts
    concatted         = DF.concat nested
assimilateAnds _ = Nothing
{-# INLINE assimilateOrs #-}
assimilateOrs :: Expr v -> Maybe (Expr v)
assimilateOrs (Or disjuncts) = if DF.null concatted
    then Nothing
    -- TODO: Rewrite as optics
    else Just (Or (normals <> concatted))  where
    (normals, nested) = partitionEithers $ fmap getOrDisjuncts disjuncts
    concatted         = DF.concat nested
assimilateOrs _ = Nothing

{-# INLINE flattenRules #-}
flattenRules :: Applicative f => Expr v -> f (Maybe (Expr v))
flattenRules a = pure (assimilateAnds a <|> assimilateOrs a)


{-# INLINE trivialRedundantRules #-}
trivialRedundantRules
    :: (Eq v, Grouping v, Applicative f) => Expr v -> f (Maybe (Expr v))
trivialRedundantRules (And as) = if DF.length as == DF.length nubbed
    then pure Nothing
    else pure $ Just (And nubbed)
    where nubbed = nub as
trivialRedundantRules (Or as) = if DF.length as == DF.length nubbed
    then pure Nothing
    else pure $ Just (Or nubbed)
    where nubbed = nub as
trivialRedundantRules _ = pure Nothing
