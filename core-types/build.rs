extern crate string_cache_codegen;

use std::env;
use std::path::Path;

fn main() {
    string_cache_codegen::AtomType::new("cosmothought::SymbolAtom", "symbol_atom!")
        .atoms(&["type", "state", "superstate", "operator", "name"])
        .write_to_file(&Path::new(&env::var("OUT_DIR").unwrap()).join("cosmothought_atom.rs"))
        .unwrap()
}
