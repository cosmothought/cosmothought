
{
  description = "cosmothought";

  inputs.haskellNix.url = "github:input-output-hk/haskell.nix";
  inputs.nixpkgs.follows = "haskellNix/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  outputs = { self, nixpkgs, flake-utils, haskellNix }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
       compiler-nix-name = "ghc981";

        componentOpts = {
          doCoverage = true;
          doHaddock = true;
                            enableProfiling = true;
          # src = haskellNix.haskellLib.cleanSourceWith {
          #   src = haskellNix.haskellLib.cleanGit {
          #     name = "cosmothought";
          #     src = ./.;
          #     keepGitDir = true;
          #   };};

          #env = [{name="SOURCEREV";
          #        value="${self.rev or "dirty"}";}];

          # ["-DSOURCEREV=${self.rev or "dirty"}"
          #               "-DLASTMODIFIED=${self.sourceInfo.lastModifiedDate}"
          #              ];
        };
        pkgs = import nixpkgs { inherit system overlays; inherit (haskellNix) config; };

        overlays = [
          haskellNix.overlay
          (final: prev: {
            # This overlay adds our project to pkgs
            polysemy-plugin = prev.polysemy-plugin.override (_: { enableProfiling = true; enableLibraryProfiling = true; });
            cosmothoughtProject =
              final.haskell-nix.project'
                {
inherit compiler-nix-name;
                  src = ./.;
                  index-state = "2024-02-22T00:00:00Z";
                  # This is used by `nix develop .` to open a shell for use with
                  # `cabal`, `hlint` and `haskell-language-server`
                  shell.tools = {
                    cabal = "latest";
                    hlint = "latest";
                    alex = "latest";
                    haskell-language-server = "latest";
                    cabal-install = "latest";
                    profiteur = "latest";
                  };
                  modules = [
{
      reinstallableLibGhc = builtins.compareVersions final.haskell-nix.compiler.${compiler-nix-name}.version "9.8.1" < 0;
}
                    {

#                      ghcOptions = prev.cosmothoughtProject.ghcOptions ++ nixpkgs.lib.optional (pkgs.stdenv.hostPlatform.isLinux && !pkgs.haskell-nix.haskellLib.isCrossHost && !pkgs.stdenv.hostPlatform.isMusl) "-g3";

                                                                enableProfiling = true;
                                                                enableLibraryProfiling = true;
                    }
                    {
                      packages.prettyprinter-ansi-terminal-modified = { doCheck = false; };
                      packages.cosmic-fabric =
                        {
                          doCoverage = true;
                          doHaddock = true;
                          allComponent = componentOpts;
                        };
                      packages.cosmic-modelling =
                        {
                          doCoverage = true;
                          doHaddock = true;
                          allComponent = componentOpts  // ({setupDepends = [ final.cosmothoughtProject.hsPkgs.polysemy-plugin];});
                          
                                                    enableProfiling = true;
                                                    enableLibraryProfiling = true;
                        };
                      packages.cosmic-gc =
                        {
                          doCoverage = true;
                          doHaddock = true;
                          allComponent = componentOpts;
                                                    enableProfiling = true;
                                                    enableLibraryProfiling = true;
                        };
                      packages.cosmothought-core =
                        {
                          doCoverage = true;
                          doHaddock = true;
                          allComponent = componentOpts;
                                                    enableProfiling = true;
                                                    enableLibraryProfiling = true;

                          components =
                            {
                              library = componentOpts // {
                                pkgconfig = [ [ pkgs.graphviz ] ];
                              };
                              sublibs.cosmothought-core-cosmolude = componentOpts;
                              exes.cosmothought-core-oneoff = componentOpts // {
                                doCoverage = false;
                              };
                              tests.cosmothought-core-test = componentOpts;
                            };
                        };
                    }
                  ];

                  # Non-Haskell shell tools go here
                  shell.buildInputs = with pkgs; [
                    pkg-config
                    zlib
                    graphviz
                    pcre
                    git
                    scc #Succinct Code Counter
                  ];
                  # This adds `js-unknown-ghcjs-cabal` to the shell.
                  # shell.crossPlatforms = p: [p.ghcjs];
                };
          })
        ];
        coreShell = pkgs.mkShell {
          name = "basic-rust";
          packages = with pkgs; [
            stgit
            nixpkgs-fmt
            rust-analyzer
            gnum4
            #                    rust
            cargo
            cargo-fuzz
            cargo-tarpaulin
            cargo-audit
            cargo-graph
            #                    cargo-vendor
            #                    cargo-count
            aflplusplus
            linuxKernel.packages.linux_libre.perf
            haskellPackages.graphmod
            xdot
          ];
        };
        flake = pkgs.cosmothoughtProject.flake {
          # This adds support for `nix build .#js-unknown-ghcjs:hello:exe:hello`
          # crossPlatforms = p: [p.ghcjs];
          doCoverage = true;


        };
        coverageReport = pkgs.cosmothoughtProject.projectCoverageReport;
        copyReport' = pkgs.writeShellScriptBin "copy-coverage-report"
          ''
            mkdir -p $1
            cp -r ${coverageReport}/share/hpc/vanilla/html/* $1
            cp -r ${coverageReport}/share/hpc/vanilla/tix/* $1
            cp -r ${coverageReport}/share/hpc/vanilla/mix/* $1
          '';
        copyCrashData' = pkgs.writeShellScriptBin "copy-crash-data"
          ''
            mkdir -p $1
            tar  -cf cosmothought-cache-data.tar $2/cosmothought
            ${pkgs.zstd}/bin/zstd cosmothought-cache-data.tar
            cp -r cosmothought-cache-data.tar.zst $1
          '';

        fuzz = app: appName: pkgs.writeShellScriptBin "fuzz"
          "
mkdir -p fuzzing
cd fuzzing
${pkgs.aflplusplus}/bin/afl-fuzz -i $1/corpus -o $1/out ${app}/bin/${appName}
  ";

        shellArgs = {
          #          packages = pkgs: [ pkgs.cosmic-fabric pkgs.cosmic-modelling pkgs.cosmic-gc pkgs.cosmothought-core ];
          tools = {
            cabal = "latest";
            hlint = "latest";
            haskell-language-server = "latest";
            cabal-install = "latest";  alex = "latest";
          };
          #          enableProfiling = true;
          inputsFrom = [ coreShell ];
          #          enableLibraryProfiling = true;
          exactDeps = true;
          enableDWARF = true;
          shellHook = ''
            showdeps(){
            find $@ -name '[A-Z]*.hs' | xargs graphmod -q -p | xdot -
            }
          '';

        };
        myShell = pkgs.cosmothoughtProject.shellFor shellArgs;

        fuzzComponents = {
          configureFlags = [ "fuzzing" "llvm" ];
          pkgconfig = [ pkgs.aflplusplus llvmpkg ];
        };
        llvmpkg = pkgs.llvmPackages_9.libllvm;
        fuzzPackage = {
          allComponent = fuzzComponents;

          ghcOptions = [
            ''-DAFL_LLVM_INSTRUMENT=CFG''
            "-DAFL_USE_ASAN=1"
            "-DAFL_DEBUG=1"
            "-DAFL_LLVM_CTX=1"
            "-pgmlo ${llvmpkg}/bin/opt"
            "-pgmlc ${llvmpkg}/bin/llc"
            ''-optlo  \"-load=${pkgs.aflplusplus}/lib/afl/libLLVMInsTrim.so\"''
            ''-optlo  \"-load=${pkgs.aflplusplus}/lib/afl/afl-llvm-pass.so\"''
            ''-optlo  \"-load=${pkgs.aflplusplus}/lib/afl/cmplog-instructions-pass.so\"''
            ''-optlo  \"-load=${pkgs.aflplusplus}/lib/afl/cmplog-routines-pass.so\"''
            ''-optlo  \"-load=${pkgs.aflplusplus}/lib/afl/split-compares-pass.so\"''
            ''-optlo  \"-load=${pkgs.aflplusplus}/lib/afl/split-switches-pass.so\"''

            #-load=${pkgs.aflplusplus}/lib/afl/afl-llvm-pass.so -load=${pkgs.aflplusplus}/lib/afl/cmplog-instructions-pass.so -load=${pkgs.aflplusplus}/lib/afl/cmplog-routines-pass.so -load=${pkgs.aflplusplus}/lib/afl/split-compares-pass.so -load=${pkgs.aflplusplus}/lib/afl/split-switches-pass.so\"''
            "-fllvm"
            #           "-pgmlo ${pkgs.aflplusplus.inputs.llvm}/bin/opt"
          ];
        };
        dbg = (pkgs.cosmothoughtProject.appendModule [
          {
            modules = [{
              packages.cosmothought-core =
                {
                  flags = {
                    debug = true;
                    asserts = true;
                  };
                  allComponent.configureFlags = [ "+debug" "+asserts" ];
                  configureFlags = [ "+debug" "+asserts" ];
                  components =
                    {
                      library.configureFlags = [ "+debug" "+asserts" ];

                      sublibs.cosmolude.configureFlags = [ "+debug" "+asserts" ];
                      exes.cosmothought-core-oneoff.configureFlags = [ "+debug" "+asserts" ];
                      tests.cosmothought-core-test.configureFlags = [ "+debug" "+asserts" ];
                    };
                };
            }];
          }
        ]);
        dbgShell = dbg.shellFor (shellArgs // {
          cabalProjectLocal = ''
            packages: vendor/prettyprinter-ansi-terminal/prettyprinter-ansi-terminal-modified.cabal vendor/polysemy-keyed-state/polysemy-keyed-state.cabal vendor/calamity-commands/calamity-commands.cabal

            ignore-project: False

            package cosmothought-core
                   flags: +asserts +debug
                   tests: true
            package cosmothought-core-test
                   flags: +asserts +debug
          '';
        });

        withFuzzing = (pkgs.cosmothoughtProject.appendModule [
          {
            modules =
              [{
                packages.cosmothought-core = fuzzPackage;
                packages.cosmic-gc = fuzzPackage;
                packages.cosmic-fabric = fuzzPackage;
                packages.cosmic-modelling = fuzzPackage;
              }];
          }
        ]).flake
          { };
        copyHaddock' = pkgs.writeShellScriptBin "copy-haddock"
          ''
                                    mkdir -p $1
                                    cp -r ${pkgs.cosmothoughtProject.hsPkgs.cosmic-fabric.components.library.doc}/share/doc/* $1
                                    cp -r ${pkgs.cosmothoughtProject.hsPkgs.cosmic-modelling.components.library.doc}/share/doc/* $1
                                    cp -r ${pkgs.cosmothoughtProject.hsPkgs.cosmic-gc.components.library.doc}/share/doc/* $1
                                    cp -r ${pkgs.cosmothoughtProject.hsPkgs.cosmothought-core.components.library.doc}/share/doc/* $1
            #                        cp -r {pkgs.cosmothoughtProject.hsPkgs.cosmothought-core.components.sublibs.cosmothought-core-cosmolude.doc} $1
          '';

      in
      {
        inherit (flake); #flake // {
        myflake = flake;
        devShells.default = myShell;
        devShells.debug = dbgShell;
        hydraJobs = nixpkgs.lib.mapAttrs (_: nixpkgs.lib.hydraJob) self.packages."x86_64-linux";
        # Built by `nix build .`
        packages =  flake.packages // {
          

          copyCrashData = copyCrashData';
          copyReport = copyReport';
          copyHaddock = copyHaddock';
          default = flake.packages."cosmothought-core:test:cosmothought-core-test";
          fuzzOneOff = fuzz (withFuzzing.packages."cosmothought-core:exe:cosmothought-core-oneoff") "cosmothought-core-oneoff";
#          container = pkgs.dockerTools.buildLayeredImage {
#            name = "cosmothought/devenv";
#            maxLayers = 200;
#            fromImage = pkgs.dockerTools.pullImage {
#              finalImageName = "nix";
#              imageName = "nixos/nix";
#              finalImageTag = "2.12";
#              imageDigest = "sha256:840bec8bcedf4858ddc525e1a9f41240c2c2a1380fd5315e756119ef66a0aed6";
#              sha256 = "sha256-kYqcl4HMgpNsvrkD16SB4/2d0Q5fuWN1zoKGzgz5wJA=";
#            };
#            config = {
#              Memory = 8589934592;
#              Cmd = [
#                "mkdir -p /etc/containers"
#                ''echo '{"default":[{"type":"insecureAcceptAnything"}]}' > /etc/containers/policy.json''
#                "${pkgs.nix}/bin/nix develop .#shell"
#              ];
#            };
#          };
        };
      });
}
